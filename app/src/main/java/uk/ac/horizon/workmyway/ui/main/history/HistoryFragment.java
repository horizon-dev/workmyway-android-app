package uk.ac.horizon.workmyway.ui.main.history;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.WorkMyWay;
import uk.ac.horizon.workmyway.db.ASyncDatabase;
import uk.ac.horizon.workmyway.model.Model;
import uk.ac.horizon.workmyway.model.ModelStats;
import uk.ac.horizon.workmyway.ui.common.BarChartRenderer;
import uk.ac.horizon.workmyway.ui.common.ModelDataSet;
import uk.ac.horizon.workmyway.ui.common.HorizontalBarChart;
import uk.ac.horizon.workmyway.util.Log;

public class HistoryFragment extends Fragment {

    private static final String TAG = HistoryFragment.class.getSimpleName();

    private final List<SummaryFragment> fragments = new ArrayList<>();

    private FragmentActivity activity;

    private HorizontalBarChart chart;

    private Button dateBox;

    private ViewPager pager;

    private View trophyView;

    private DateTime current = DateTime.now().minusDays(1);

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment__ui_main_history__history, container, false);
        chart = v.findViewById(R.id.history_chart);
        configureChart(chart);
        dateBox = v.findViewById(R.id.date_selector);
        configureDatebox(dateBox);
        v.findViewById(R.id.button_decrease_date).setOnClickListener(view -> {
            current = current.minusDays(1);
            loadModel(current);
        });
        v.findViewById(R.id.button_increase_date).setOnClickListener(view -> {
            current = current.plusDays(1);
            loadModel(current);
        });
        pager = v.findViewById(R.id.history_pager);
        pager.setAdapter(new FragmentStatePagerAdapter(activity.getSupportFragmentManager()) {

            @Override
            public android.support.v4.app.Fragment getItem(int position) {
                return fragments.get(position);
            }

            @Override
            public int getCount() {
                return fragments.size();
            }

            @Override
            public int getItemPosition(@NonNull Object o) {
                return PagerAdapter.POSITION_NONE;
            }

        });
        trophyView = v.findViewById(R.id.trophy);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        /*final GestureDetector detector = new GestureDetector(
                activity,
                new GestureDetector.SimpleOnGestureListener() {
                    @Override
                    public boolean onFling(
                            MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                        final int threshold = 2000;
                        if (velocityX < threshold && velocityX > -threshold) {
                            return true;
                        }
                        float diffX = e2.getX() - e1.getX();
                        if (diffX < 0) {
                            current = current.plusDays(1);
                        } else {
                            current = current.minusDays(1);
                        }
                        loadModel(current);
                        return true;
                    }
                });
        chart.setOnTouchListener((v, event) -> {
            if (!detector.onTouchEvent(event)) {
                v.performClick();
            }
            return false;
        });*/
        loadModel(current);
        WorkMyWay.getAnalytics().setCurrentScreen(
                getActivity(), this.getClass().getSimpleName(), this.getClass().getName());
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (FragmentActivity) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.activity = null;
    }

    private void loadModel(final DateTime day) {
        DateTime dayStart = day.withMillisOfDay(0);
        DateTime dayEnd = dayStart.plusDays(1);
        Interval dayInterval = new Interval(dayStart, dayEnd);
        Model model = WorkMyWay.getModelManager().getModelFor(dayInterval);
        updateChart(model);
        updatePager(model);
    }

    private void updateChart(final Model model) {
        activity.runOnUiThread(() -> {
            dateBox.setText(current.toString("dd/MM/YYYY"), TextView.BufferType.NORMAL);
            long startMillis;
            try {
                startMillis = model.getPeriods().first().first().getInterval().getStartMillis();
            } catch (Exception e) {
                startMillis = 0;
            }
            long sMillis = startMillis;
            chart.getAxisRight().setValueFormatter((value, axis) -> {
                DateTime instant = new DateTime(((long) value) + sMillis);
                return instant.toString("HH:mm");
            });
            chart.setData(new BarData(new ModelDataSet(model, getActivity())));
            chart.notifyDataSetChanged();
            chart.invalidate();

        });
    }

    private void updatePager(final Model day) {
        activity.runOnUiThread(() -> {
            fragments.removeAll(Collections.unmodifiableList(fragments));
            ASyncDatabase db = WorkMyWay.getDatabase();
            final ModelStats stats = new ModelStats(day);
            JSONObject reward;
            try {
                reward = db.getDayReward(day.getPeriods().first().first().getInterval().getStart());
            } catch (Exception e) {
                reward = null;
            }
            if (reward != null) {
                trophyView.setVisibility(View.VISIBLE);
                TrophyFragment tf = new TrophyFragment();
                try {
                    tf.setBreaks(reward.getInt("breaks"));
                    tf.setCombined(reward.getInt("periods"));
                } catch (JSONException e) {
                    Log.e(TAG, e.getMessage(), e);
                }
                fragments.add(tf);
            } else {
                trophyView.setVisibility(View.INVISIBLE);
            }
            fragments.add(new ActiveSummaryFragment());
            fragments.add(new InactiveSummaryFragment());
            fragments.add(new BreakSummaryFragment());
            fragments.add(new DaySummaryFragment());
            for (SummaryFragment f : fragments) {
                f.setModelStats(stats);
            }
            PagerAdapter pagerAdapter = pager.getAdapter();
            if (pagerAdapter != null) {
                pagerAdapter.notifyDataSetChanged();
            }
        });
    }

    private void configureChart(HorizontalBarChart chart) {
        chart.setTouchEnabled(true);
        chart.setClickable(false);
        chart.setHighlightPerTapEnabled(false);
        chart.setHighlightFullBarEnabled(false);
        chart.setHighlightPerDragEnabled(false);
        chart.setDrawValueAboveBar(false);
        chart.setDragEnabled(true);
        chart.setScaleXEnabled(true);
        chart.setScaleYEnabled(false);

        chart.getXAxis().setEnabled(true);
        chart.getXAxis().setGranularity(1f);
        chart.getXAxis().setTextColor(Color.BLACK);
        chart.getXAxis().setLabelRotationAngle(-90f);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.getXAxis().setDrawGridLines(false);
        final String[] labels = new String[]{
                getResources().getString(R.string.inactive),
                getResources().getString(R.string.active)
        };
        chart.getXAxis().setValueFormatter((value, axis) -> labels[(int) value]);

        Description description = new Description();
        description.setText("");
        chart.setDescription(description);
        chart.setDrawBorders(false);
        chart.setDrawMarkers(true);
        chart.setDrawGridBackground(false);
        chart.getLegend().setEnabled(false);

        chart.getAxisLeft().setSpaceTop(0f);
        chart.getAxisLeft().setSpaceBottom(0f);
        chart.getAxisRight().setSpaceTop(0f);
        chart.getAxisRight().setSpaceBottom(0f);
        chart.getAxisLeft().setValueFormatter((value, axis) -> "");
        chart.getAxisRight().setTextColor(Color.BLACK);

        chart.setRenderer(new BarChartRenderer(
                chart, chart.getAnimator(), chart.getViewPortHandler()));
    }

    private void configureDatebox(final Button dateBox) {
        dateBox.setInputType(InputType.TYPE_NULL);
        dateBox.setOnClickListener(v -> {
            DatePickerDialog d = new DatePickerDialog(
                    getActivity(),
                    (view, year, month, dayOfMonth) -> {
                        current = DateTime.now()
                                .withYear(year)
                                .withMonthOfYear(month + 1)
                                .withDayOfMonth(dayOfMonth);
                        loadModel(current);
                    },
                    current.getYear(),
                    current.getMonthOfYear() - 1,
                    current.getDayOfMonth());
            DatePicker dp = d.getDatePicker();
            dp.setMaxDate(DateTime.now().getMillis());
            d.show();
        });
        dateBox.setText(current.toString("dd/MM/YYYY"), TextView.BufferType.NORMAL);
    }

}
