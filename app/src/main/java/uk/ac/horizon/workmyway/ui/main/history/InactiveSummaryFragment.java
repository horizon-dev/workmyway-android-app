package uk.ac.horizon.workmyway.ui.main.history;

import android.text.Html;
import android.view.View;
import android.widget.TextView;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.WorkMyWay;
import uk.ac.horizon.workmyway.model.ModelStats;

public class InactiveSummaryFragment extends SummaryFragment {

    @Override
    protected int getLayoutId() {
        return R.layout.fragment__ui_main_history__inactive_summary;
    }

    @Override
    protected void setup(View view, ModelStats modelStats) {
        TextView summary = (TextView) view.findViewById(R.id.text_view_summary);
        String format = getResources().getString(R.string.inactivity_summary);
        long minutes = modelStats.getCombinedProlongedSedentary().getStandardMinutes();
        summary.setText(Html.fromHtml(parseColors(String.format(format, minutes))));
    }

    @Override
    public void onResume() {
        super.onResume();
        WorkMyWay.getAnalytics().setCurrentScreen(
                getActivity(), this.getClass().getSimpleName(), this.getClass().getName());
    }

}
