package uk.ac.horizon.workmyway.db;

import android.provider.BaseColumns;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import uk.ac.horizon.workmyway.model.DeviceType;

public final class Contract {

    private Contract() {
    }

    private interface DeviceColumn extends BaseColumns {
        String COLUMN_NAME_DEVICE = "device";
    }

    private interface TimestampColumn extends BaseColumns {
        String COLUMN_NAME_TIMESTAMP = "timestamp";
    }

    static class DeviceTypeValue {

        static int toStorage(DeviceType deviceType) {
            switch (deviceType) {
                case Wrist:
                    return 0;
                case Cup:
                    return 1;
                default:
                    throw new RuntimeException("Unknown DeviceType <" + deviceType + ">");
            }
        }

    }

    public static class BooleanValue {

        static int toStorage(boolean value) {
            return value ? 1 : 0;
        }

        public static boolean fromStorage(int value) {
            return value != 0;
        }

    }

    public static class DayValue {

        static int toStorage(DateTime dt) {
            return (dt.getYear() * 10000) + (dt.getMonthOfYear() * 100) + dt.getDayOfMonth();
        }

        public static DateTime fromStorage(int v) {
            int year = v / 10000;
            int month = (v / 100) - (year * 100);
            int day = v - ((year * 10000) + (month * 100));
            return new DateTime(0)
                    .withZone(DateTimeZone.UTC)
                    .withYear(year)
                    .withMonthOfYear(month)
                    .withDayOfMonth(day);
        }

    }

    public static class MotionReadings implements DeviceColumn, TimestampColumn {
        static final String TABLE_NAME = "motion_readings";
        static final String SQL_CREATE_TABLE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY, " +
                        COLUMN_NAME_DEVICE + " INTEGER, " +
                        COLUMN_NAME_TIMESTAMP + " INTEGER)";
    }

    public static class StepReadings implements DeviceColumn, TimestampColumn {
        static final String TABLE_NAME = "step_readings";
        static final String SQL_CREATE_TABLE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY, " +
                        COLUMN_NAME_DEVICE + " INTEGER, " +
                        COLUMN_NAME_TIMESTAMP + " INTEGER)";
    }

    public static class TapReadings implements DeviceColumn, TimestampColumn {
        static final String TABLE_NAME = "tap_readings";
        public static final String COLUMN_NAME_SIGN = "sign";
        public static final String COLUMN_NAME_TAP_TYPE = "tap_type";
        static final String SQL_CREATE_TABLE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY, " +
                        COLUMN_NAME_DEVICE + " INTEGER, " +
                        COLUMN_NAME_TIMESTAMP + " INTEGER, " +
                        COLUMN_NAME_SIGN + " TEXT, " +
                        COLUMN_NAME_TAP_TYPE + " TEXT)";
    }

    public static class ConnectionStatuses implements DeviceColumn, TimestampColumn {
        static final String TABLE_NAME = "connection_statuses";
        public static final String COLUMN_NAME_CONNECTED = "connected";
        public static final String COLUMN_NAME_EXPECTED = "expected";
        static final String SQL_CREATE_TABLE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY, " +
                        COLUMN_NAME_DEVICE + " INTEGER, " +
                        COLUMN_NAME_TIMESTAMP + " INTEGER, " +
                        COLUMN_NAME_CONNECTED + " INTEGER, " +
                        COLUMN_NAME_EXPECTED + " INTEGER)";
    }

    public static class TrackingStatuses implements TimestampColumn {
        static final String TABLE_NAME = "tracking_statuses";
        public static final String COLUMN_NAME_STATUS = "status";
        static final String SQL_CREATE_TABLE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY, " +
                        COLUMN_NAME_TIMESTAMP + " INTEGER, " +
                        COLUMN_NAME_STATUS + " INTEGER)";
    }

    public static class Logs implements TimestampColumn {
        static final String TABLE_NAME = "logs";
        public static final String COLUMN_NAME_MESSAGE = "message";
        static final String SQL_CREATE_TABLE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY, " +
                        COLUMN_NAME_TIMESTAMP + " INTEGER, " +
                        COLUMN_NAME_MESSAGE + " TEXT)";
    }

    public static class SelfReports implements TimestampColumn {
        static final String TABLE_NAME = "self_reports";
        public static final String COLUMN_NAME_START = "start";
        public static final String COLUMN_NAME_DETAILS = "details";
        public static final String COLUMN_NAME_DURATION = "duration";
        public static final String COLUMN_NAME_ACTIVE = "active";
        static final String SQL_CREATE_TABLE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY, " +
                        COLUMN_NAME_TIMESTAMP + " INTEGER, " +
                        COLUMN_NAME_START + " INTEGER, " +
                        COLUMN_NAME_DETAILS + " TEXT, " +
                        COLUMN_NAME_DURATION + " INTEGER, " +
                        COLUMN_NAME_ACTIVE + " INTEGER)";
    }

    public static class Alerts implements TimestampColumn {
        static final String TABLE_NAME = "alerts";
        public static final String COLUMN_NAME_ACTION = "action";
        static final String SQL_CREATE_TABLE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY, " +
                        COLUMN_NAME_TIMESTAMP + " INTEGER, " +
                        COLUMN_NAME_ACTION + " TEXT)";
    }

    public static class ConfigurationChanges implements TimestampColumn {
        static final String TABLE_NAME = "config_changes";
        public static final String COLUMN_NAME_KEY = "key";
        public static final String COLUMN_NAME_VALUE = "value";
        static final String SQL_CREATE_TABLE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY, " +
                        COLUMN_NAME_TIMESTAMP + " INTEGER, " +
                        COLUMN_NAME_KEY + " TEXT, " +
                        COLUMN_NAME_VALUE + " TEXT)";
    }

    public static class Heartbeats implements DeviceColumn, TimestampColumn {
        static final String TABLE_NAME = "heartbeats";
        static final String SQL_CREATE_TABLE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY, " +
                        COLUMN_NAME_DEVICE + " INTEGER, " +
                        COLUMN_NAME_TIMESTAMP + " INTEGER)";
        static final String[] SQL_UPGRADE_1_TO_2 = new String[]{
                SQL_CREATE_TABLE
        };
    }

    public static class DayComparisonReward implements BaseColumns {
        static final String TABLE_NAME = "day_rewards";
        public static final String COLUMN_NAME_DAY = "day";
        public static final String COLUMN_NAME_DATA = "data";
        static final String SQL_CREATE_TABLE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY, " +
                        COLUMN_NAME_DAY + " INTEGER, " +
                        COLUMN_NAME_DATA + " TEXT)";
        static final String[] SQL_UPGRADE_2_TO_3 = new String[]{
                SQL_CREATE_TABLE
        };
    }

    public static class GoalReward implements BaseColumns {
        static final String TABLE_NAME = "goal_rewards";
        public static final String COLUMN_NAME_DAY = "day";
        public static final String COLUMN_NAME_DATA = "data";
        public static final String COLUMN_NAME_REWARDED = "rewarded";
        static final String SQL_CREATE_TABLE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY, " +
                        COLUMN_NAME_DAY + " INTEGER, " +
                        COLUMN_NAME_DATA + " TEXT, " +
                        COLUMN_NAME_REWARDED + " INTEGER)";
        static final String[] SQL_UPGRADE_3_TO_4 = new String[]{
                SQL_CREATE_TABLE
        };
        static final String[] SQL_UPGRADE_4_TO_5 = new String[]{
                "ALTER TABLE goal_rewards RENAME TO goal_rewards_old",
                SQL_CREATE_TABLE,
                "INSERT INTO goal_rewards (_id, day, data, rewarded) "
                        + "SELECT _id, day, data, REWARDED FROM goal_rewards_old"
        };
    }

}
