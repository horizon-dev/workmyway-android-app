package uk.ac.horizon.workmyway.ui;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.ui.about.AboutActivity;
import uk.ac.horizon.workmyway.ui.common.ActivityResultActivity;
import uk.ac.horizon.workmyway.ui.common.DeviceServiceExecutorActivity;
import uk.ac.horizon.workmyway.ui.connection.ConnectionFragment;
import uk.ac.horizon.workmyway.ui.connection.ConnectionUtils;
import uk.ac.horizon.workmyway.ui.developer.DeveloperSettingsActivity;
import uk.ac.horizon.workmyway.ui.main.MainFragment;
import uk.ac.horizon.workmyway.ui.registration.RegistrationUtils;
import uk.ac.horizon.workmyway.ui.registration.RegisterFragment;
import uk.ac.horizon.workmyway.ui.settings.SettingsActivity;

public class HomeActivity
        extends DeviceServiceExecutorActivity
        implements
        RegisterFragment.OnFragmentInteractionListener,
        ConnectionFragment.FragmentInteractionListener,
        ActivityResultActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__ui__home);
        displayContentFragment();
    }

    @Override
    public void signedIn() {
        displayContentFragment();
    }

    @Override
    public void connected() {
        displayContentFragment();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.menu_main_action_settings:
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        case R.id.menu_main_action_about:
            startActivity(new Intent(this, AboutActivity.class));
            return true;
        case R.id.menu_main_action_developer_mode:
            @SuppressLint("InflateParams") final View v = getLayoutInflater().inflate(
                    R.layout.activity__smartcup_ui_support__mainmenu_password_request, null);
            new AlertDialog.Builder(this)
                    .setView(v)
                    .setPositiveButton(R.string.button_positive_default,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    EditText pw = (EditText) v.findViewById(R.id.password);
                                    if (pw.getText().toString().equals("smartcup")) {
                                        startActivity(new Intent(
                                                HomeActivity.this,
                                                DeveloperSettingsActivity.class));
                                    } else {
                                        Toast.makeText(
                                                HomeActivity.this,
                                                R.string.invalid_password,
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }
                            })
                    .setNegativeButton(R.string.button_negative_default,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) { }
                            })
                    .create().show();
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

    private void displayContentFragment() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        if (!ConnectionUtils.isConnected(this)) {
            ft.replace(R.id.content, new ConnectionFragment());
        } else if (!RegistrationUtils.isRegistered(this)) {
            ft.replace(R.id.content, new RegisterFragment());
        } else {
            ft.replace(R.id.content, new MainFragment());
        }
        ft.commit();
    }

    private final List<ActivityResultActivity.ActivityResultListener> activityResultListeners =
            new ArrayList<>();

    @Override
    public void addActivityResultListener(ActivityResultListener listener) {
        activityResultListeners.add(listener);
    }

    @Override
    public void removeActivityResultListener(ActivityResultListener listener) {
        activityResultListeners.remove(listener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        for (ActivityResultActivity.ActivityResultListener listener : activityResultListeners) {
            listener.onActivityResult(requestCode, resultCode, data);
        }
    }


}
