package uk.ac.horizon.workmyway.ui.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import uk.ac.horizon.workmyway.R;

public class AssetWebView extends WebView {

    private final Context context;

    private String assetName;

    @SuppressWarnings("unused")
    public AssetWebView(Context context) {
        super(context);
        this.context = context;
    }

    @SuppressWarnings("unused")
    public AssetWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init(attrs);
    }

    @SuppressWarnings("unused")
    public AssetWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init(attrs);
    }

    @SuppressWarnings("unused")
    public AssetWebView(
            Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context = context;
        init(attrs);
    }

    @Override
    public void setWebViewClient(WebViewClient client) {
        // Do nothing
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (assetName == null) {
            return;
        }
        loadUrl("file:///android_asset/" + assetName);
        requestFocus();
    }

    private void init(AttributeSet attrs) {
        TypedArray res = context.obtainStyledAttributes(
                attrs, R.styleable.AssetWebView);
        try {
            assetName = res.getString(
                    R.styleable.AssetWebView_asset);
        } finally {
            res.recycle();
        }
    }

}
