package uk.ac.horizon.workmyway.ui.main.track.home;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.util.ArrayList;
import java.util.List;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.WorkMyWay;
import uk.ac.horizon.workmyway.model.Model;
import uk.ac.horizon.workmyway.model.ModelManager;
import uk.ac.horizon.workmyway.model.ModelStats;
import uk.ac.horizon.workmyway.ui.HomeActivity;
import uk.ac.horizon.workmyway.ui.main.history.ActiveSummaryFragment;
import uk.ac.horizon.workmyway.ui.main.history.BreakSummaryFragment;
import uk.ac.horizon.workmyway.ui.main.history.DaySummaryFragment;
import uk.ac.horizon.workmyway.ui.main.history.InactiveSummaryFragment;
import uk.ac.horizon.workmyway.ui.main.history.SummaryFragment;
import uk.ac.horizon.workmyway.ui.main.track.TrackFragment;
import uk.ac.horizon.workmyway.util.Utils;

public class HomeFragment extends Fragment {

    private FragmentActivity activity;

    private TrackFragment trackFragment;

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment__ui_main_track_home__home, container, false);
        final Button trackButton = v.findViewById(R.id.button_start_tracking);
        trackButton.setOnClickListener(v1 -> {
            trackButton.setEnabled(false);
            DateTime now = DateTime.now();
            WorkMyWay.getDatabase().insertTrackingStatus(now, true);
            WorkMyWay.getModelManager().getModelForToday().addIntervalStart(now);
            if (trackFragment != null) {
                trackFragment.trackingStarted();
            }
        });
        loadHistory();
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        WorkMyWay.getAnalytics().setCurrentScreen(
                getActivity(), this.getClass().getSimpleName(), this.getClass().getName());
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (FragmentActivity) activity;
    }

    public void setTrackFragment(TrackFragment trackFragment) {
        this.trackFragment = trackFragment;
    }

    private void loadHistory() {
        ModelManager modelManager = WorkMyWay.getModelManager();
        if (modelManager == null) {
            return;
        }
        Interval yesterday =
                Utils.Time.getDay(DateTime.now().minusDays(1));
        Model model = modelManager.getModelFor(yesterday);
        final List<SummaryFragment> fragments = new ArrayList<>();
        fragments.add(new ActiveSummaryFragment());
        fragments.add(new InactiveSummaryFragment());
        fragments.add(new BreakSummaryFragment());
        fragments.add(new DaySummaryFragment());
        ModelStats stats = new ModelStats(model);
        for (SummaryFragment f : fragments) {
            f.setModelStats(stats);
        }
        if (activity != null && activity instanceof HomeActivity) {
            HomeActivity ha = (HomeActivity) activity;
            android.support.v4.app.FragmentManager fm = ha.getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.history_grid_1, fragments.get(0));
            ft.replace(R.id.history_grid_2, fragments.get(1));
            ft.replace(R.id.history_grid_3, fragments.get(2));
            ft.replace(R.id.history_grid_4, fragments.get(3));
            ft.commit();
        }
    }

}
