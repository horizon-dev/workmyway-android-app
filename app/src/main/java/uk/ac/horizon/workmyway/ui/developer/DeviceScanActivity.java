package uk.ac.horizon.workmyway.ui.developer;

import android.app.ActionBar;
import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.mbientlab.bletoolbox.scanner.BleScannerFragment;
import com.mbientlab.metawear.MetaWearBoard;

import java.util.UUID;

import uk.ac.horizon.workmyway.R;

public class DeviceScanActivity
        extends Activity
        implements BleScannerFragment.ScannerCommunicationBus {

    public static final String RESULT_FIELD_NAME = "e0e8d504-b8e6-4715-bc1a-e7e95431c6b3";

    private static final long SCAN_DURATION_MS = 30000L;

    private static final UUID[] SERVICE_FILTER_UUIDS = new UUID[]{
            MetaWearBoard.METAWEAR_GATT_SERVICE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__ui_developer__device_scan);
        ActionBar bar = getActionBar();
        if (bar != null) {
            bar.setSubtitle(R.string.activity__ui_developer__device_scan__subtitle);
            bar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public UUID[] getFilterServiceUuids() {
        return SERVICE_FILTER_UUIDS;
    }

    @Override
    public long getScanDuration() {
        return SCAN_DURATION_MS;
    }

    @Override
    public void onDeviceSelected(BluetoothDevice device) {
        Intent i = new Intent();
        i.putExtra(RESULT_FIELD_NAME, device.getAddress());
        setResult(RESULT_OK, i);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
