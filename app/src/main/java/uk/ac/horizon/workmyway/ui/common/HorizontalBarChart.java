package uk.ac.horizon.workmyway.ui.common;

import android.content.Context;
import android.util.AttributeSet;

public class HorizontalBarChart extends com.github.mikephil.charting.charts.HorizontalBarChart {

    public HorizontalBarChart(Context context) {
        super(context);
    }

    public HorizontalBarChart(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HorizontalBarChart(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean performClick() {
        return super.performClick();
    }

}
