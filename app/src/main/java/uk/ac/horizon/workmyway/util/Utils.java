package uk.ac.horizon.workmyway.util;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.util.Base64;

import com.mbientlab.metawear.MetaWearBoard;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import uk.ac.horizon.workmyway.model.Epoch;
import uk.ac.horizon.workmyway.model.Model;
import uk.ac.horizon.workmyway.model.MutableModel;
import uk.ac.horizon.workmyway.model.Period;

public class Utils {

    private static final String TAG = Utils.class.getSimpleName();

    public static final class Metawear {

        public static void deserializeState(MetaWearBoard board, String state)
                throws
                IOException,
                ClassNotFoundException {
            InputStream in = new ByteArrayInputStream(Base64.decode(state, Base64.DEFAULT));
            board.deserialize(in);
            in.close();
        }

        public static String serializeState(MetaWearBoard board)
                throws IOException {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            board.serialize(out);
            return Base64.encodeToString(out.toByteArray(), Base64.DEFAULT);
        }

    }

    public static final class Time {

        public static class IntervalComparator implements Comparator<Interval> {
            @Override
            public int compare(Interval i1, Interval i2) {
                return i1.getStart().compareTo(i2.getStart());
            }
        }

        public static Interval getDay(DateTime dateTime) {
            DateTime start = dateTime.withMillisOfDay(0);
            DateTime end = start.plusDays(1);
            return new Interval(start, end);
        }

        public static List<DateTime> getDaysIn(Interval interval) {
            List<DateTime> days = new ArrayList<>();
            DateTime day = interval.getStart();
            while (interval.contains(day)) {
                days.add(day);
                day = day.plusDays(1);
            }
            return days;
        }

    }

    public static final class Model {

        public final static class UntrackedPeriod
                implements
                Period,
                Comparable<uk.ac.horizon.workmyway.model.Period> {

            private final Interval interval;

            UntrackedPeriod(Interval interval) {
                this.interval = interval;
            }

            @NonNull
            @Override
            public Interval getInterval() {
                return interval;
            }

            @NonNull
            @Override
            public SortedSet<? extends Epoch> getEpochs() {
                throw new RuntimeException("Not implemented");
            }

            @NonNull
            @Override
            public Type getType() {
                throw new RuntimeException("Not implemented");
            }

            @Override
            public int compareTo(@NonNull Period another) {
                return interval.getStart().compareTo(another.getInterval().getStart());
            }

        }

        /*@NonNull
        public static SortedSet<Period> flattenPeriods(
                SortedSet<SortedSet<? extends Period>> periods) {
            TreeSet<Period> flattened = new TreeSet<>();
            Period last = null;
            for (SortedSet<? extends Period> ps : periods) {
                if (ps.size() == 0) {
                    continue;
                }
                if (last != null) {
                    if ((last.getInterval().getEnd().isBefore(
                            ps.first().getInterval().getStart())) ||
                            ( last.getInterval().getEnd().equals(ps.first().getInterval().getStart()))) {
                        Interval interval = new Interval(last.getInterval().getEnd(),
                                ps.first().getInterval().getStart());
                        flattened.add(new UntrackedPeriod(interval));
                    } else {
                        Log.w(TAG, "No gap in periods, interval [" + last.getInterval().toString()
                                + "] is not before interval [" + ps.first().getInterval().toString() + "]");
                    }
                }
                flattened.addAll(ps);
                last = ps.last();
            }
            return flattened;
        }*/

        @NonNull
        public static SortedSet<Period> flattenPeriods(uk.ac.horizon.workmyway.model.Model model) {
            TreeSet<Period> flattened = new TreeSet<>();
            Period last = null;
            for (SortedSet<? extends Period> ps : model.getPeriods()) {
                if (ps.size() == 0) {
                    continue;
                }
                if (last != null) {
                    if ((last.getInterval().getEnd().isBefore(
                            ps.first().getInterval().getStart())) ||
                            (last.getInterval().getEnd().equals(
                                    ps.first().getInterval().getStart()))) {
                        Interval interval = new Interval(
                                last.getInterval().getEnd(),
                                ps.first().getInterval().getStart());
                        flattened.add(new UntrackedPeriod(interval));
                    } else {
                        Log.w(TAG, "No gap in periods, interval [" + last.getInterval().toString()
                                + "] is not before interval [" + ps.first().getInterval().toString() + "]");
                    }
                }
                flattened.addAll(ps);
                last = ps.last();
            }
            if (last != null && model instanceof MutableModel) {
                DateTime now = DateTime.now();
                if (last.getInterval().getEnd().isBefore(now)) {
                    Interval i = new Interval(last.getInterval().getEnd(), DateTime.now());
                    flattened.add(new UntrackedPeriod(i));
                }
            }
            return flattened;
        }

    }

    public static boolean isNullOrEmpty(Object o) {
        return o == null || o.equals("");
    }

    public static DateTime getInstallDate(Context context) {
        try {
            return new DateTime(context
                    .getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0)
                    .firstInstallTime);
        } catch (PackageManager.NameNotFoundException e) {
            return DateTime.now();
        }
    }

}
