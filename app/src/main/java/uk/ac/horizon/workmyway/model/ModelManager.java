package uk.ac.horizon.workmyway.model;

import android.support.annotation.NonNull;

import org.joda.time.Interval;

public interface ModelManager {

    @NonNull
    MutableModel getModelForToday();

    @NonNull
    Model getModelFor(Interval day);

}
