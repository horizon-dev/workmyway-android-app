package uk.ac.horizon.workmyway.ui.developer;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.util.AttributeSet;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.ui.common.ActivityResultPreference;

class DeviceScanPreference
        extends ActivityResultPreference
        implements SharedPreferences.OnSharedPreferenceChangeListener {

    private final Context context;

    @SuppressWarnings("unused")
    public DeviceScanPreference(Context context) {
        super(context);
        this.context = context;
        init();
    }

    @SuppressWarnings("unused")
    public DeviceScanPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    @SuppressWarnings("unused")
    public DeviceScanPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init();
    }

    @SuppressWarnings("unused")
    public DeviceScanPreference(
            Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context = context;
        init();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(context);
        p.unregisterOnSharedPreferenceChangeListener(this);
    }

    private void init() {
        SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(context);
        p.registerOnSharedPreferenceChangeListener(this);
        reset();
    }

    private void reset() {
        SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(context);
        Resources r = context.getResources();
        String mac = p.getString(getKey(), r.getString(R.string.label_not_connected));
        setSummary(String.format("%s %s",
                r.getString(R.string.label_currently_connected_to), mac));
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        reset();
    }
}
