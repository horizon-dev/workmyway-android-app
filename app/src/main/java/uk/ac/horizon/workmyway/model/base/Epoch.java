package uk.ac.horizon.workmyway.model.base;

import android.support.annotation.NonNull;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.util.ArrayList;
import java.util.List;

class Epoch implements uk.ac.horizon.workmyway.model.Epoch, Comparable<Epoch> {

    enum ManualStatus {
        None,
        Active,
        Inactive
    }

    private final List<DateTime> counts = new ArrayList<>();

    private final Interval interval;

    private ManualStatus manualStatus = ManualStatus.None;

    Epoch(Interval interval) {
        this.interval = interval;
    }

    @Override
    @NonNull
    public Interval getInterval() {
        return interval;
    }

    @Override
    public long getCount() {
        return counts.size();
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj instanceof Epoch && interval.equals(((Epoch) obj).interval);
    }

    @Override
    public int compareTo(@NonNull Epoch o) {
        return interval.getStart().compareTo(o.interval.getStart());
    }

    void setManualStatus(ManualStatus manualStatus) {
        this.manualStatus = manualStatus;
    }

    ManualStatus getManualStatus() {
        return manualStatus;
    }

    void addCount(DateTime count) {
        if (!interval.contains(count)) {
            throw new RuntimeException(
                    "Instant <" + count.toString() + "> does not belong to this Epoch");
        }
        counts.add(count);
    }

}
