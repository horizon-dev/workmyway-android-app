package uk.ac.horizon.workmyway.ui.main.track.chart;

import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.util.SortedSet;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.WorkMyWay;
import uk.ac.horizon.workmyway.alert.AlertLevel;
import uk.ac.horizon.workmyway.alert.AlertListener;
import uk.ac.horizon.workmyway.alert.AlertService;
import uk.ac.horizon.workmyway.model.Collections;
import uk.ac.horizon.workmyway.model.Model;
import uk.ac.horizon.workmyway.model.ModelManager;
import uk.ac.horizon.workmyway.model.MutableModel;
import uk.ac.horizon.workmyway.model.Period;
import uk.ac.horizon.workmyway.ui.common.BarChartRenderer;
import uk.ac.horizon.workmyway.ui.common.ModelDataSet;
import uk.ac.horizon.workmyway.ui.main.track.TrackFragment;
import uk.ac.horizon.workmyway.util.Log;

public class ChartFragment
        extends Fragment
        implements ServiceConnection, AlertListener {

    private static final String TAG = ChartFragment.class.getSimpleName();

    private final AtomicReference<AlertLevel> currentAlertLevel
            = new AtomicReference<>(AlertLevel.None);

    private ScheduledExecutorService executorService;

    private AlertService.LocalBinder alertService;

    private TrackFragment trackFragment;

    private HorizontalBarChart chart;

    private ImageView alertImage;

    private TextView alertText;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment__ui_main_track_chart__chart, container, false);

        chart = v.findViewById(R.id.activity_chart);
        Description description = new Description();
        description.setText("");
        chart.setDescription(description);
        chart.setTouchEnabled(true);
        chart.setClickable(false);
        chart.setHighlightPerTapEnabled(false);
        chart.setHighlightFullBarEnabled(false);
        chart.setHighlightPerDragEnabled(false);
        chart.setDrawValueAboveBar(false);
        chart.setDragEnabled(true);
        chart.setScaleXEnabled(true);
        chart.setScaleYEnabled(false);
        chart.setDrawBorders(false);
        chart.setDrawMarkers(true);
        chart.setDrawGridBackground(false);
        chart.getLegend().setEnabled(false);
        chart.setRenderer(new BarChartRenderer(
                chart, chart.getAnimator(), chart.getViewPortHandler()));

        XAxis xAxis = chart.getXAxis();
        xAxis.setEnabled(true);
        xAxis.setGranularity(1f);
        xAxis.setTextColor(Color.BLACK);
        xAxis.setLabelRotationAngle(-90f);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        final String[] labels = new String[]{
                getResources().getString(R.string.inactive),
                getResources().getString(R.string.active)
        };
        xAxis.setValueFormatter((value, axis) -> labels[(int) value]);

        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setSpaceTop(0f);
        leftAxis.setSpaceBottom(0f);
        leftAxis.setValueFormatter((value, axis) -> "");

        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setSpaceTop(0f);
        rightAxis.setSpaceBottom(0f);
        ModelManager modelManager = WorkMyWay.getModelManager();
        MutableModel model = modelManager.getModelForToday();
        long startMillis;
        try {
            startMillis = model.getPeriods().first().first().getInterval().getStartMillis();
        } catch (Exception e) {
            startMillis = DateTime.now().getMillis();
        }
        final long sMillis = startMillis;
        rightAxis.setValueFormatter((value, axis) -> {
            DateTime instant = new DateTime(((long) value) + sMillis);
            return instant.toString("HH:mm");
        });
        rightAxis.setTextColor(Color.BLACK);


        final Button finishButton = v.findViewById(R.id.finish_button);
        finishButton.setOnClickListener(v12 -> {
            finishButton.setEnabled(false);
            DateTime now = DateTime.now();
            WorkMyWay.getDatabase().insertTrackingStatus(now, false);
            WorkMyWay.getModelManager().getModelForToday().addIntervalEnd(now);
            if (trackFragment != null) {
                trackFragment.trackingStopped();
            }
        });

        final Button diaryButton = v.findViewById(R.id.diary_entry_button);
        diaryButton.setOnClickListener(v1 -> new DiaryEntryFragment().show(
                getFragmentManager(),
                getResources().getString(R.string.new_diary_entry)));

        alertImage = v.findViewById(R.id.lightbulb);
        alertText = v.findViewById(R.id.alert_text);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        Context context = getActivity();
        context.bindService(new Intent(
                context, AlertService.class), this, Context.BIND_AUTO_CREATE);
        SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(getActivity());
        executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(this::update, 0, Long.parseLong(p.getString(
                getString(R.string.pk_epoch_time),
                getString(R.string.epoch_time_default))), TimeUnit.SECONDS);
        WorkMyWay.getAnalytics().setCurrentScreen(
                getActivity(), this.getClass().getSimpleName(), this.getClass().getName());
    }

    @Override
    public void onPause() {
        super.onPause();
        executorService.shutdownNow();
        if (alertService != null) {
            alertService.removeAlertListener(this);
        }
        getActivity().unbindService(this);
    }

    private void update() {
        final ModelManager modelManager = WorkMyWay.getModelManager();
        final Model model = modelManager.getModelForToday();
        final AlertLevel alertLevel = currentAlertLevel.get();
        String text = null;
        try {
            if (alertLevel.equals(AlertLevel.BreakDue)
                    || alertLevel.equals(AlertLevel.BreakOverdue)) {
                long minutes = 0;
                Collections.ModelPeriods periods = model.getPeriods();
                if (periods.size() > 0) {
                    SortedSet<? extends Period> currentPeriods = periods.last();
                    if (currentPeriods.size() > 0) {
                        Period current = currentPeriods.last();
                        minutes = current.getInterval().toDuration().getStandardMinutes();
                    }
                }
                text = String.format(getResources().getQuantityString(
                        R.plurals.alert_text_sat_for, (int) minutes), minutes);
            }
            chart.setData(new BarData(new ModelDataSet(model, getActivity())));

        } catch (Exception e) {
            Log.w(TAG, e.getMessage(), e);
        }
        final String alert_text = text;
        ChartFragment.this.getActivity().runOnUiThread(() -> {
            if (alert_text != null) {
                alertText.setText(alert_text);
            }
            chart.notifyDataSetChanged();
            chart.invalidate();
        });
        // Disable the 'first 3 hours' reward for now.
        /*new Thread(new Runnable() {
            @Override
            public void run() {
                SharedPreferences prefs;
                try {
                    prefs = PreferenceManager.getDefaultSharedPreferences(ChartFragment.this.getActivity());
                } catch (Exception e) {
                    Log.w(TAG, e.getMessage(), e);
                    return;
                }
                boolean ran = prefs.getBoolean(getString(R.string.pk_three_hours_use), false);
                if (ran) {
                    return;
                }
                ModelManager modelManager = WorkMyWay.getModelManager();
                Model model = modelManager.getModelForToday();
                SortedSet<SortedSet<? extends Period>> periods = model.getPeriods();
                if (periods.size() > 0) {
                    SortedSet<? extends Period> currentPeriods = periods.last();
                    if (currentPeriods.size() > 0) {
                        Period current = currentPeriods.last();
                        if (current.getInterval().toDuration().getStandardHours() > 3) {
                            prefs.edit().putBoolean(getString(R.string.pk_three_hours_use), true).apply();
                            try {
                                JSONObject obj = new JSONObject();
                                obj.put("type", "firstuse");
                                ASyncDatabase db = WorkMyWay.getDatabase();
                                db.insertDayReward(DateTime.now(), obj);
                            } catch (JSONException e) {
                                Log.e(TAG, e.getMessage(), e);
                            }
                        }
                    }
                }
            }
        }).start();*/
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        if (service instanceof AlertService.LocalBinder) {
            (alertService = ((AlertService.LocalBinder) service)).addAlertListener(this);
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        alertService = null;
    }

    @Override
    public void onLevelChanged(AlertLevel level) {
        currentAlertLevel.set(level);
        Context context = getActivity();
        Resources res = context.getResources();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        int color_int;
        String text;
        long minutes = 0;
        ModelManager modelManager = WorkMyWay.getModelManager();
        Model model = modelManager.getModelForToday();
        Collections.ModelPeriods periods = model.getPeriods();
        if (periods.size() > 0) {
            SortedSet<? extends Period> currentPeriods = periods.last();
            if (currentPeriods.size() > 0) {
                Period current = currentPeriods.last();
                minutes = current.getInterval().toDuration().getStandardMinutes();
            }
        }

        switch (level) {
            case BreakApproaching:
                color_int = prefs.getInt(
                        res.getString(R.string.pk_first_alert_led_color),
                        res.getColor(R.color.alert_break_approaching));
                text = res.getString(R.string.alert_text_consider_break);
                break;
            case BreakDue:
                color_int = prefs.getInt(
                        res.getString(R.string.pk_second_alert_led_color),
                        res.getColor(R.color.alert_break_due));
                text = String.format(res.getQuantityString(
                        R.plurals.alert_text_sat_for, (int) minutes), minutes);
                break;
            case BreakOverdue:
                color_int = prefs.getInt(
                        res.getString(R.string.pk_third_alert_led_color),
                        res.getColor(R.color.alert_break_overdue));
                text = String.format(res.getQuantityString(
                        R.plurals.alert_text_sat_for, (int) minutes), minutes);
                break;
            default:
                color_int = Color.GREEN;
                text = res.getString(R.string.alert_text_keep_going);
        }
        final int c = color_int;
        final String t = text;
        getActivity().runOnUiThread(() -> {
            alertImage.getDrawable().setTintList(null);
            alertImage.getDrawable().setTint(c);
            alertImage.getDrawable().invalidateSelf();
            alertImage.invalidate();
            alertText.setText(t);
        });
    }

    @Override
    public void onBeginPause(Interval interval) { /* Not used */ }

    @Override
    public void onEndPause(Interval interval) { /* Not used */ }

    public void setTrackFragment(TrackFragment trackFragment) {
        this.trackFragment = trackFragment;
    }

}
