package uk.ac.horizon.workmyway.ui.main.connection;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mbientlab.metawear.module.Settings;

import org.joda.time.DateTime;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.metawear.DeviceConnectionService;
import uk.ac.horizon.workmyway.metawear.DeviceListener;
import uk.ac.horizon.workmyway.metawear.DeviceListenerAdapter;
import uk.ac.horizon.workmyway.model.DeviceType;
import uk.ac.horizon.workmyway.ui.common.DeviceServiceExecutor;
import uk.ac.horizon.workmyway.ui.common.DeviceServiceExecutorActivity;

public class ConnectionFragment extends Fragment {

    private static int getBatteryDrawableId(int level) {
        if (level > 95) {
            return R.drawable.ic_battery_full;
        }
        if (level > 64) {
            return R.drawable.ic_battery_3_4;
        }
        if (level > 34) {
            return R.drawable.ic_battery_1_2;
        }
        if (level > 5) {
            return R.drawable.ic_battery_1_4;
        }
        return R.drawable.ic_battery_empty;
    }

    private final DeviceListener deviceListener = new DeviceListenerAdapter() {

        @Override
        public void batteryState(
                final DeviceType deviceType, final Settings.BatteryState batteryState) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (view == null) {
                        return;
                    }
                    ImageView image = null;
                    TextView text = null;
                    switch (deviceType) {
                    case Wrist:
                        image = (ImageView)
                                view.findViewById(R.id.wrist_device_battery_status_icon);
                        text = (TextView)
                                view.findViewById(R.id.wrist_device_battery_status_text);
                        break;
                    case Cup:
                        image = (ImageView)
                                view.findViewById(R.id.cup_device_battery_status_icon);
                        text = (TextView)
                                view.findViewById(R.id.cup_device_battery_status_text);
                        break;
                    }
                    image.setImageResource(getBatteryDrawableId(batteryState.charge));
                    image.invalidate();
                    text.setText(String.format(
                            getActivity().getResources().getConfiguration().locale,
                            "%d%%", batteryState.charge));
                    text.invalidate();

                }
            });
        }

        @Override
        public void connected(final DeviceType deviceType, DateTime timestamp) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (view == null) {
                        return;
                    }
                    int id = -1;
                    switch (deviceType) {
                    case Wrist:
                        id = R.id.wrist_device_connection_status;
                        break;
                    case Cup:
                        id = R.id.cup_device_connection_status;
                        break;
                    }
                    TextView v = (TextView) view.findViewById(id);
                    v.setText(R.string.connection_status_connected);
                    v.setTextColor(getResources().getColor(R.color.device_connected_text));
                    v.invalidate();
                }
            });
        }

        @Override
        public void disconnected(
                final DeviceType deviceType, DateTime timestamp, boolean expected) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (view == null) {
                        return;
                    }
                    int id = -1;
                    switch (deviceType) {
                    case Wrist:
                        id = R.id.wrist_device_connection_status;
                        break;
                    case Cup:
                        id = R.id.cup_device_connection_status;
                        break;
                    }
                    TextView v = (TextView) view.findViewById(id);
                    v.setText(R.string.connection_status_disconnected);
                    v.setTextColor(getResources().getColor(R.color.device_disconnected_text));
                    v.invalidate();
                }
            });
        }
    };

    private View view;

    private DeviceServiceExecutorActivity activity;


    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(
                R.layout.fragment__ui_main_connection__connection, container, false);
        return view;
    }

    @Override
    public synchronized void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof DeviceServiceExecutorActivity) {
            this.activity = (DeviceServiceExecutorActivity) activity;
            this.activity.execute(new DeviceServiceExecutor.Task() {
                @Override
                public void doTask(DeviceConnectionService.LocalBinder binder) {
                    binder.addDeviceListener(deviceListener);
                }
            });
        }
    }

    @Override
    public synchronized void onDetach() {
        super.onDetach();
        activity.execute(new DeviceServiceExecutor.Task() {
            @Override
            public void doTask(DeviceConnectionService.LocalBinder binder) {
                binder.removeDeviceListener(deviceListener);
            }
        });
        activity = null;
    }

    private synchronized void runOnUiThread(Runnable runnable) {
        if (activity != null) {
            activity.runOnUiThread(runnable);
        }
    }

}
