package uk.ac.horizon.workmyway.metawear;

import com.mbientlab.metawear.module.AccelerometerBosch;
import com.mbientlab.metawear.module.Settings;

import org.joda.time.DateTime;

import uk.ac.horizon.workmyway.model.DeviceType;

@SuppressWarnings("EmptyMethod")
public abstract class DeviceListenerAdapter implements DeviceListener {

    @Override
    public void batteryState(DeviceType deviceType, Settings.BatteryState batteryState) {
    }

    @Override
    public void tapLive(DeviceType deviceType, DateTime timestamp, AccelerometerBosch.Tap tap) {
    }

    @Override
    public void stepLive(DeviceType deviceType, DateTime timestamp) {
    }

    @Override
    public void stepLogged(DeviceType deviceType, DateTime timestamp) {
    }

    @Override
    public void motionLive(DeviceType deviceType, DateTime timestamp) {
    }

    @Override
    public void motionLogged(DeviceType deviceType, DateTime timestamp) {
    }

    @Override
    public void heartbeat(DeviceType deviceType, DateTime timestamp) {
    }

    @Override
    public void connected(DeviceType deviceType, DateTime timestamp) {
    }

    @Override
    public void disconnected(DeviceType deviceType, DateTime timestamp, boolean expected) {
    }

}
