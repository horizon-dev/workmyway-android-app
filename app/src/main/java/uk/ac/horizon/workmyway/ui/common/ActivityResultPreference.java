package uk.ac.horizon.workmyway.ui.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.preference.Preference;
import android.util.AttributeSet;

import uk.ac.horizon.workmyway.R;

public class ActivityResultPreference extends Preference {

    private Activity activity;

    private Intent childActivity;

    private int requestCode;

    @SuppressWarnings("unused")
    public ActivityResultPreference(Context context) {
        super(context);
    }

    @SuppressWarnings("unused")
    public ActivityResultPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    @SuppressWarnings("unused")
    public ActivityResultPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @SuppressWarnings("unused")
    public ActivityResultPreference(
            Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    @Override
    protected void onClick() {
        activity.startActivityForResult(childActivity, requestCode);
    }

    private void init(Context context, AttributeSet attrs) {
        activity = (Activity) context;
        TypedArray res = context.obtainStyledAttributes(
                attrs, R.styleable.uk_ac_horizon_smartcup_ui_common_ActivityResultPreference);
        try {
            String className = res.getString(R.styleable.
                    uk_ac_horizon_smartcup_ui_common_ActivityResultPreference_className);
            try {
                childActivity = new Intent(context, Class.forName(className));
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }

            requestCode = res.getInteger(R.styleable.
                            uk_ac_horizon_smartcup_ui_common_ActivityResultPreference_requestCode,
                    -1);
        } finally {
            res.recycle();
        }
    }

}
