package uk.ac.horizon.workmyway.model.base;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import uk.ac.horizon.workmyway.model.Collections;
import uk.ac.horizon.workmyway.model.Period;
import uk.ac.horizon.workmyway.util.Utils;

class CollectionsImpl {

    interface Timeline extends SortedMap<DateTime, Epoch> {
    }

    static class TimelineImpl
            extends TreeMap<DateTime, Epoch>
            implements Timeline {
    }

    interface Intervals extends SortedSet<Interval> {
    }

    static class IntervalsImpl
            extends TreeSet<Interval>
            implements Intervals {
        IntervalsImpl() {
            super(new Utils.Time.IntervalComparator());
        }
    }

    static class PeriodsImpl
            extends TreeSet<Period>
            implements Collections.Periods {
        PeriodsImpl() {
            super((a, b) -> a.getInterval().getStart().compareTo(b.getInterval().getStart()));
        }
    }

    static class ModelPeriods
            extends TreeSet<Collections.Periods>
            implements Collections.ModelPeriods {
        ModelPeriods() {
            super((a, b) -> {
                try {
                    return a.first().getInterval().getStart().compareTo(
                            b.first().getInterval().getStart());
                } catch (Exception e) {
                    return 0;
                }
            });
        }
    }

}
