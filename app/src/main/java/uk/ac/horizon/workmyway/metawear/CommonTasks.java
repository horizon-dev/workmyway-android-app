package uk.ac.horizon.workmyway.metawear;

import android.graphics.Color;
import android.support.annotation.ColorInt;

import com.mbientlab.metawear.MetaWearBoard;
import com.mbientlab.metawear.module.Led;
import com.mbientlab.metawear.module.Logging;
import com.mbientlab.metawear.module.Settings;

class CommonTasks {

    static DeviceTask readBatteryState() {
        return board -> board.getModule(Settings.class).battery().read();
    }

    static DeviceTask tearDown() {
        return MetaWearBoard::tearDown;
    }

    static DeviceTask pulseLed(@ColorInt final int color, final int count) {
        return board -> actuateLed(
                board.getModule(Led.class), color, count, Led.PatternPreset.PULSE);
    }

    static DeviceTask blinkLed(@ColorInt final int color, final int count) {
        return board ->
                actuateLed(board.getModule(Led.class), color, count, Led.PatternPreset.BLINK);
    }

    static DeviceTask solidLed(@ColorInt final int color, final int count) {
        return board ->
                actuateLed(board.getModule(Led.class), color, count, Led.PatternPreset.SOLID);
    }

    static DeviceTask ledOff() {
        return board -> board.getModule(Led.class).stop(true);
    }

    static DeviceTask disconnectAction(@ColorInt final int ledColor) {
        return board -> {
            Settings settings = board.getModule(Settings.class);
            final Logging log = board.getModule(Logging.class);
            final Led led = board.getModule(Led.class);
            settings.onDisconnectAsync(() -> {
                log.start(true);
                led.stop(true);
                actuateLed(led, ledColor, 3, Led.PatternPreset.PULSE);
            });
        };
    }

    private static byte intToIntensity(int i) {
        return (byte) Math.max(1f, (((float) i) * (31f / 256f)));
    }

    private static void actuateLed(
            Led led, @ColorInt int color, int count, Led.PatternPreset pattern) {
        led.stop(true);
        for (Led.Color c : new Led.Color[]{Led.Color.RED, Led.Color.GREEN, Led.Color.BLUE}) {
            byte intensity = (byte) 0;
            switch (c) {
                case RED:
                    intensity = intToIntensity(Color.red(color));
                    break;
                case GREEN:
                    intensity = intToIntensity(Color.green(color));
                    break;
                case BLUE:
                    intensity = intToIntensity(Color.blue(color));
                    break;
            }
            Led.PatternEditor editor = led.editPattern(c, pattern)
                    .repeatCount(count == -1 ? Led.PATTERN_REPEAT_INDEFINITELY : (byte) count)
                    .highIntensity(intensity);
            if (pattern.equals(Led.PatternPreset.SOLID)) {
                editor.lowIntensity(intensity);
            }
            editor.commit();
        }
        led.play();
    }

}
