package uk.ac.horizon.workmyway.alert.alertlisteners;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.joda.time.Interval;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.alert.AlertLevel;
import uk.ac.horizon.workmyway.alert.AlertListener;
import uk.ac.horizon.workmyway.db.ASyncDatabase;

public class Database implements AlertListener {

    private final Context context;

    private final ASyncDatabase database;

    public Database(Context context, ASyncDatabase database) {
        this.context = context;
        this.database = database;
    }

    @Override
    public void onLevelChanged(AlertLevel level) {
        SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(context);
        String action;
        String time;
        switch (level) {
            case None:
                action = "none";
                time = "";
                break;
            case BreakApproaching:
                action = "breakApproaching";
                try {
                    time = p.getString(
                            context.getString(R.string.pk_first_alert_time),
                            context.getString(R.string.first_alert_time_default));
                } catch (ClassCastException e) {
                    time = String.valueOf(p.getInt(
                            context.getString(R.string.pk_first_alert_time),
                            Integer.parseInt(
                                    context.getString(R.string.first_alert_time_default))));
                }
                break;
            case BreakDue:
                action = "breakDue";
                try {
                    time = p.getString(
                            context.getString(R.string.pk_second_alert_led_color),
                            context.getString(R.string.second_alert_time_default));
                } catch (ClassCastException e) {
                    time = String.valueOf(p.getInt(
                            context.getString(R.string.pk_second_alert_led_color),
                            Integer.parseInt(
                                    context.getString(R.string.second_alert_time_default))));
                }
                break;
            case BreakOverdue:
                action = "breakOverdue";
                try {
                    time = p.getString(
                            context.getString(R.string.pk_third_alert_time),
                            context.getString(R.string.third_alert_time_default));
                } catch (ClassCastException e) {
                    time = String.valueOf(p.getInt(
                            context.getString(R.string.pk_third_alert_time),
                            Integer.parseInt(
                                    context.getString(R.string.third_alert_time_default))));
                }
                break;
            default:
                action = "unknown";
                time = "";
        }
        database.insertAlert(action + "(" + time + ")");
    }

    @Override
    public void onBeginPause(Interval interval) {
        SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(context);
        String time = p.getString(
                context.getString(R.string.pk_alert_snooze_time),
                context.getString(R.string.alert_snooze_time_default));
        database.insertAlert("pause(\"" + interval.toString() + "\", " + time + ")");
    }

    @Override
    public void onEndPause(Interval interval) {
        SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(context);
        String time = p.getString(
                context.getString(R.string.pk_alert_snooze_time),
                context.getString(R.string.alert_snooze_time_default));
        database.insertAlert("unpause(\"" + interval.toString() + "\", " + time + ")");
    }
}
