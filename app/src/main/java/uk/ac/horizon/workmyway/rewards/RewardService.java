package uk.ac.horizon.workmyway.rewards;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Pair;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Duration;
import org.joda.time.Interval;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.WorkMyWay;
import uk.ac.horizon.workmyway.db.ASyncDatabase;
import uk.ac.horizon.workmyway.model.Model;
import uk.ac.horizon.workmyway.model.ModelManager;
import uk.ac.horizon.workmyway.model.ModelStats;
import uk.ac.horizon.workmyway.util.Log;
import uk.ac.horizon.workmyway.util.Utils;

public class RewardService extends Service {

    private static final String TAG = RewardService.class.getSimpleName();

    public class LocalBinder extends Binder {

        public void recalculate() {
            WorkMyWay.getDatabase().deleteDayRewards();
            handler.removeCallbacks(dayComparisonMonitor);
            handler.post(dayComparisonMonitor);
        }

    }

    private final LocalBinder localBinder = new LocalBinder();

    private final Runnable personalGoalMonitor = new Runnable() {
        @Override
        public void run() {
            DateTime start =
                    Utils.getInstallDate(RewardService.this).withMillisOfDay(0).plusDays(1);
            DateTime end = DateTime.now().withMillisOfDay(0);
            android.util.Log.d(TAG, "Goal Reward Monitor Running");
            if (end.isBefore(start)) {
                android.util.Log.d(TAG, "Still on day 1");
                requeue();
                return;
            }
            Interval interval = new Interval(start.minusDays(1), end);
            List<DateTime> days = Utils.Time.getDaysIn(interval);
            ASyncDatabase db = WorkMyWay.getDatabase();
            ModelManager mm = WorkMyWay.getModelManager();

            SharedPreferences prefs =
                    PreferenceManager.getDefaultSharedPreferences(RewardService.this);
            Resources res = getResources();
            int sittingTime = prefs.getInt(
                    getString(R.string.pk_personal_goal_time),
                    res.getInteger(R.integer.personal_goal_time_default));
            int breakCount = prefs.getInt(
                    getString(R.string.pk_personal_goal_breaks),
                    res.getInteger(R.integer.personal_goal_breaks_default));

            for (DateTime day : days) {
                android.util.Log.d(TAG, "Processing day " + day.toString("dd/MM/yyyy"));
                Pair<JSONObject, Boolean> obj = db.getGoalReward(day);
                if (obj != null) {
                    continue;
                }
                // Process and save result
                Model model = mm.getModelFor(Utils.Time.getDay(day));
                ModelStats stats = new ModelStats(model);
                boolean rewarded = false;
                if (stats.getBreakCount() >= breakCount) {
                    JSONObject o = new JSONObject();
                    try {
                        o.put("type", "break");
                        o.put("breakCountPref", breakCount);
                        o.put("breakCount", stats.getBreakCount());
                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage(), e);
                    }
                    db.insertGoalReward(day, o, true);
                    rewarded = true;
                }
                if (stats.getCombinedProlongedSedentary()
                        .isShorterThan(Duration.standardMinutes(sittingTime))) {
                    JSONObject o = new JSONObject();
                    try {
                        o.put("type", "sitting");
                        o.put("sittingTimePref", sittingTime);
                        o.put(
                                "sittingTime",
                                stats.getCombinedProlongedSedentary().getStandardMinutes());
                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage(), e);
                    }
                    rewarded = true;
                }
                if (!rewarded) {
                    JSONObject o = new JSONObject();
                    db.insertGoalReward(day, o, false);
                }

            }
        }

        private void requeue() {
            DateTime end =
                    DateTime.now(DateTimeZone.UTC).plusDays(1).withMillisOfDay(10 * 60 * 1000);
            Duration d = new Duration(DateTime.now(DateTimeZone.UTC), end);
            handler.postDelayed(this, d.getMillis());
        }

    };

    private final Runnable dayComparisonMonitor = new Runnable() {

        @Override
        public void run() {
            DateTime start =
                    Utils.getInstallDate(RewardService.this).withMillisOfDay(0).plusDays(1);
            DateTime end = DateTime.now().withMillisOfDay(0);
            android.util.Log.d(TAG, "DayComparison Running");
            if (end.isBefore(start)) {
                android.util.Log.d(TAG, "Still on day 1");
                requeue();
                return;
            }
            Interval interval = new Interval(start.minusDays(1), end);
            List<DateTime> days = Utils.Time.getDaysIn(interval);
            ASyncDatabase db = WorkMyWay.getDatabase();
            ModelManager mm = WorkMyWay.getModelManager();
            for (DateTime day : days) {
                android.util.Log.d(TAG, "Processing day " + day.toString("dd/MM/yyyy"));
                JSONObject obj = db.getDayReward(day);
                if (obj != null) {
                    continue;
                }
                // Process and save result
                Model dayModel = mm.getModelFor(Utils.Time.getDay(day));
                Model previousModel = findPreviousDay(mm, day, start);
                if (previousModel == null) {
                    continue;
                }

                final ModelStats dayStats = new ModelStats(dayModel);
                final ModelStats previousStats = new ModelStats(previousModel);
                if (dayStats.getBreakCount() > previousStats.getBreakCount()
                        || dayStats.getCombinedProlongedSedentary().getMillis() <
                        previousStats.getCombinedProlongedSedentary().getMillis()) {
                    try {
                        obj = new JSONObject();
                        android.util.Log.d(TAG, "Day sedentary: " + dayStats.getCombinedProlongedSedentary().getMillis());
                        android.util.Log.d(TAG, "Previous day sedentary: " + previousStats.getCombinedProlongedSedentary().getMillis());
                        obj.put("breaks", dayStats.getBreakCount() - previousStats.getBreakCount());
                        obj.put("periods", dayStats.getCombinedProlongedSedentary().getMillis() -
                                previousStats.getCombinedProlongedSedentary().getMillis());
                        db.insertDayReward(day, obj);
                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage(), e);
                    }
                }
            }

            requeue();
        }

        private void requeue() {
            DateTime end =
                    DateTime.now(DateTimeZone.UTC).plusDays(1).withMillisOfDay(10 * 60 * 1000);
            Duration d = new Duration(DateTime.now(DateTimeZone.UTC), end);
            handler.postDelayed(this, d.getMillis());
        }

    };

    private Handler handler;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new Thread(() -> {
            Looper.prepare();
            handler = new Handler(Looper.myLooper());
            handler.post(dayComparisonMonitor);
            handler.post(personalGoalMonitor);
            Looper.loop();
        }).start();
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return localBinder;
    }

    @Nullable
    private static Model findPreviousDay(ModelManager mm, DateTime current, DateTime first) {
        final Duration d = Duration.standardHours(3);
        DateTime previous = current.minusDays(1);
        while (previous.isAfter(first)) {
            Model m = mm.getModelFor(Utils.Time.getDay(previous));
            ModelStats s = new ModelStats(m);
            if (s.getTrackingDuration().isLongerThan(d)) {
                return m;
            }
            previous = previous.minusDays(1);
        }
        return null;
    }

}
