package uk.ac.horizon.workmyway.model;

import android.support.annotation.NonNull;

import org.joda.time.DateTime;
import org.joda.time.Interval;

public interface MutableModel extends Model {

    void addCount(@NonNull DateTime count);

    void addIntervalStart(@NonNull DateTime start);

    void addIntervalEnd(@NonNull DateTime end);

    void addDisconnected(@NonNull DateTime dateTime);

    void addConnected(@NonNull DateTime dateTime);

    void addHeartbeat(@NonNull DateTime dateTime);

    boolean isCurrentlyTracking();

}
