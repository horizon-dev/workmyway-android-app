package uk.ac.horizon.workmyway.db;

import android.util.Log;
import android.util.Pair;

import java.util.Queue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.LockSupport;

class PriorityMutex2 {

    private static final String TAG = PriorityMutex2.class.getSimpleName();

    enum Priority {

        WRITE(1000),
        READ(-1000);

        final int value;

        Priority(int value) {
            this.value = value;
        }

    }

    private static final int INITIAL_QUEUE_CAPACITY = 2;

    private final AtomicBoolean locked = new AtomicBoolean(false);

    private final AtomicReference<Thread> next = new AtomicReference<>(null);

    private final Queue<Pair<Priority, Thread>> waiters = new PriorityBlockingQueue<>(
            INITIAL_QUEUE_CAPACITY,
            (o1, o2) -> Integer.compare(o2.first.value, o1.first.value));

    void lock(Priority priority) {
        boolean wasInterrupted = false;
        final Thread current = Thread.currentThread();
        final Pair<Priority, Thread> pair = new Pair<>(priority, current);
        waiters.add(pair);
        while ((next.get() == null ? -1L : next.get().getId()) != current.getId() ||
                !locked.compareAndSet(false, true)) {
            LockSupport.park(this);
            if (Thread.interrupted()) {
                wasInterrupted = true;
            }
        }
        waiters.remove(pair);
        if (wasInterrupted) {
            Log.w(TAG, "Thread " + current.getId() + " was interrupted");
            current.interrupt();
        }
    }

    void unlock() {
        locked.set(false);
        Pair<Priority, Thread> next = waiters.peek();
        if (next == null) {
            this.next.set(null);
            LockSupport.unpark(null);
        } else {
            this.next.set(next.second);
            LockSupport.unpark(next.second);
        }
    }

}
