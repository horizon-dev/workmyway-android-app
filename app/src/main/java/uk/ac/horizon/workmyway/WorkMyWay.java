package uk.ac.horizon.workmyway;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.Arrays;
import java.util.List;

import uk.ac.horizon.workmyway.alert.AlertService;
import uk.ac.horizon.workmyway.db.ASyncDatabase;
import uk.ac.horizon.workmyway.db.Helper;
import uk.ac.horizon.workmyway.metawear.DeviceConnectionService;
import uk.ac.horizon.workmyway.model.ModelManager;
import uk.ac.horizon.workmyway.rewards.RewardService;
import uk.ac.horizon.workmyway.upload.UploadService;
import uk.ac.horizon.workmyway.util.Log;

public class WorkMyWay
        extends Application
        implements SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = WorkMyWay.class.getSimpleName();

    private static final Object LOCK = new Object();

    private static final Log.Level DATABASE_LOG_LEVEL = Log.Level.WARN;

    private static final Class<?>[] SERVICES = new Class[]{
            DeviceConnectionService.class,
            UploadService.class,
            AlertService.class,
            RewardService.class};

    private static FirebaseAnalytics analytics;

    private static ASyncDatabase database;

    private static ModelManager modelManager;

    public static FirebaseAnalytics getAnalytics() {
        synchronized (LOCK) {
            while (analytics == null) {
                try {
                    LOCK.wait();
                } catch (InterruptedException e) {
                    android.util.Log.w(TAG, e);
                }
            }
        }
        return analytics;
    }

    public synchronized static ASyncDatabase getDatabase() {
        synchronized (LOCK) {
            while (database == null) {
                try {
                    LOCK.wait();
                } catch (InterruptedException e) {
                    android.util.Log.w(TAG, e);
                }
            }
        }
        return database;
    }

    public synchronized static ModelManager getModelManager() {
        synchronized (LOCK) {
            while (modelManager == null) {
                try {
                    LOCK.wait();
                } catch (InterruptedException e) {
                    android.util.Log.w(TAG, e);
                }
            }
        }
        return modelManager;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        synchronized (LOCK) {
            PreferenceManager.getDefaultSharedPreferences(this)
                    .registerOnSharedPreferenceChangeListener(this);
            analytics = FirebaseAnalytics.getInstance(this);
            Bundle b = new Bundle();
            b.putString(FirebaseAnalytics.Param.VALUE, BuildConfig.VERSION_NAME);
            analytics.logEvent(FirebaseAnalytics.Event.APP_OPEN, b);
            database = new ASyncDatabase(new Helper(this).getWritableDatabase());
            modelManager = new uk.ac.horizon.workmyway.model.base.ModelManager(this, database);
            //modelManager = new uk.ac.horizon.smartcup.model.base.RandomModelManager(this);
            Log.setLevel(DATABASE_LOG_LEVEL);
            Log.setASyncDatabase(database);
            LOCK.notifyAll();
        }
        Context context = getApplicationContext();
        for (Class<?> clazz : SERVICES) {
            context.startService(new Intent(context, clazz));
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
        List<String> monitoredKeys = Arrays.asList(
                getString(R.string.pk_wrist_device_mac),
                getString(R.string.pk_cup_device_mac),
                getString(R.string.pk_epoch_time),
                getString(R.string.pk_count_mode),
                getString(R.string.pk_count_threshold),
                getString(R.string.pk_detection_x),
                getString(R.string.pk_detection_y),
                getString(R.string.pk_detection_c0),
                getString(R.string.pk_detection_c1),
                getString(R.string.pk_detection_z1),
                getString(R.string.pk_detection_p),
                getString(R.string.pk_detection_q),
                getString(R.string.pk_prolonged_sitting_duration),
                getString(R.string.pk_accelerometer_range),
                getString(R.string.pk_accelerometer_odr),
                getString(R.string.pk_step_detector_mode),
                getString(R.string.pk_significant_motion_proof_time),
                getString(R.string.pk_significant_motion_skip_time),
                getString(R.string.pk_tap_detector_threshold),
                getString(R.string.pk_tap_detector_quiet_time),
                getString(R.string.pk_tap_detector_shock_time),
                getString(R.string.pk_tap_detector_double_tap_window),
                getString(R.string.pk_first_alert_led_color),
                getString(R.string.pk_first_alert_led_pattern),
                getString(R.string.pk_first_alert_time),
                getString(R.string.pk_second_alert_led_color),
                getString(R.string.pk_second_alert_led_pattern),
                getString(R.string.pk_second_alert_time),
                getString(R.string.pk_third_alert_led_color),
                getString(R.string.pk_third_alert_led_pattern),
                getString(R.string.pk_third_alert_time),
                getString(R.string.pk_alert_tap_to_snooze),
                getString(R.string.pk_alert_snooze_time),
                getString(R.string.pk_current_version),
                getString(R.string.pk_personal_goal_time),
                getString(R.string.pk_personal_goal_breaks),
                getString(R.string.pk_three_hours_use),
                getString(R.string.pk_detection_inactive_epochs));

        if (monitoredKeys.contains(key)) {
            try {
                getDatabase().insertConfigurationChange(key, prefs.getString(key, ""));
            } catch (ClassCastException e) {
                try {
                    getDatabase().insertConfigurationChange(key,
                            String.valueOf(prefs.getInt(key, -1)));
                } catch (ClassCastException e1) {
                    try {
                        getDatabase().insertConfigurationChange(key,
                                String.valueOf(prefs.getBoolean(key, false)));
                    } catch (ClassCastException e2) {
                        try {
                            getDatabase().insertConfigurationChange(key,
                                    String.valueOf(prefs.getFloat(key, -1.0f)));
                        } catch (ClassCastException e3) {
                            try {
                                getDatabase().insertConfigurationChange(key,
                                        String.valueOf(prefs.getLong(key, -1)));
                            } catch (ClassCastException e4) {
                                Log.e(TAG, "Unknown preference value type: " + e4.getMessage(), e4);
                            }
                        }
                    }
                }
            }
        }
    }

}
