package uk.ac.horizon.workmyway.model;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.util.List;
import java.util.Map;

public interface ModelDataStore {

    Map<DateTime, Boolean> getTrackingStatuses(DateTime start, DateTime end);

    List<DateTime> getMotionReadings(
            @SuppressWarnings("SameParameterValue") DeviceType deviceType,
            DateTime start,
            DateTime end);

    List<DateTime> getStepReadings(
            @SuppressWarnings("SameParameterValue") DeviceType deviceType,
            DateTime start,
            DateTime end);

    List<DateTime> getHeartbeats(
            @SuppressWarnings("SameParameterValue") DeviceType deviceType,
            DateTime start,
            DateTime end);

    Map<DateTime, Boolean> getConnectionStatuses(
            @SuppressWarnings("SameParameterValue") DeviceType deviceType,
            DateTime start,
            DateTime end);
    
}
