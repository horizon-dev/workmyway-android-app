package uk.ac.horizon.workmyway.model.base;

import android.support.annotation.NonNull;
import android.util.Pair;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

import uk.ac.horizon.workmyway.model.Collections;
import uk.ac.horizon.workmyway.model.Model;
import uk.ac.horizon.workmyway.util.Log;

abstract class AbstractModel implements Model {

    private static final String TAG = AbstractModel.class.getSimpleName();

    CollectionsImpl.Intervals convertTrackingStatuses(
            Config config, SortedMap<DateTime, Boolean> trackingStatuses, DateTime end) {
        CollectionsImpl.Intervals trackedIntervals = new CollectionsImpl.IntervalsImpl();
        List<Map.Entry<DateTime, Boolean>> statuses = new ArrayList<>(trackingStatuses.entrySet());

        if (trackingStatuses.size() == 0) {
            return new CollectionsImpl.IntervalsImpl();
        }

        if (!statuses.get(0).getValue()) {
            trackedIntervals.add(
                    new Interval(config.getDay().getStart(), statuses.get(0).getKey()));
            statuses.remove(0);
        }

        DateTime s = null;
        boolean findStart = true;

        for (Map.Entry<DateTime, Boolean> val : statuses) {
            if (findStart) {
                if (!val.getValue()) {
                    continue;
                }
                s = val.getKey();
                findStart = false;
            } else {
                if (val.getValue()) {
                    continue;
                }
                trackedIntervals.add(new Interval(s, val.getKey()));
                s = null;
                findStart = true;
            }
        }

        if (s != null) {
            trackedIntervals.add(new Interval(s, end));
        }

        return trackedIntervals;
    }

    CollectionsImpl.Intervals computeDisconnections(
            Collection<DateTime> heartbeats,
            Map<DateTime, Boolean> connectionStatuses,
            @NonNull Collection<DateTime> counts,
            DateTime end) {


        CollectionsImpl.Intervals disconnected = new CollectionsImpl.IntervalsImpl();

        if (connectionStatuses.size() == 0) {
            return new CollectionsImpl.IntervalsImpl();
        }

        List<Map.Entry<DateTime, Boolean>> statuses
                = new ArrayList<>(new TreeMap<>(connectionStatuses).entrySet());

        DateTime s = null;
        boolean findFalse = true;

        for (Map.Entry<DateTime, Boolean> val : statuses) {
            if (findFalse) {
                if (val.getValue()) {
                    continue;
                }
                s = val.getKey();
                findFalse = false;
            } else {
                if (!val.getValue()) {
                    continue;
                }
                disconnected.add(new Interval(s, val.getKey()));
                s = null;
                findFalse = true;
            }
        }

        if (!findFalse && s != null) {
            disconnected.add(new Interval(s, end));
        }

        CollectionsImpl.Intervals f = new CollectionsImpl.IntervalsImpl();

        outer:
        for (Interval i : disconnected) {
            for (DateTime heartbeat : heartbeats) {
                if (i.contains(heartbeat)) {
                    continue outer;
                }
            }
            f.add(i);
        }

        CollectionsImpl.Intervals f2 = new CollectionsImpl.IntervalsImpl();

        outer:
        for (Interval i : f) {
            for (DateTime count : counts) {
                if (i.contains(count)) {
                    continue outer;
                }
            }
            f2.add(i);
        }

        return f2;
    }

    Epoch getEpochForCount(Config config, SortedMap<DateTime, Epoch> epochs, DateTime i) {
        Interval day = config.getDay();
        if (!day.contains(i)) {
            throw new RuntimeException("Count is outside of configured day");
        }
        DateTime start = new DateTime(i.getMillis() -
                ((i.getMillis() - day.getStartMillis()) % config.getEpochDuration().getMillis()));
        if (epochs.containsKey(start)) {
            return epochs.get(start);
        }
        Epoch e = new Epoch(new Interval(new DateTime(start), config.getEpochDuration()));
        epochs.put(start, e);
        return e;
    }

    @NonNull
    CollectionsImpl.ModelPeriods build(
            @NonNull Config config,
            @NonNull CollectionsImpl.Timeline epochs,
            @NonNull CollectionsImpl.Intervals trackedIntervals,
            @NonNull CollectionsImpl.Intervals disconnectedIntervals) {

        CollectionsImpl.ModelPeriods periods = new CollectionsImpl.ModelPeriods();
        try {
            // Create an empty timeline of the entire day
            final CollectionsImpl.Timeline timeline = makeEmptyTimeline(config);

            // Input counts
            for (Map.Entry<DateTime, Epoch> e : epochs.entrySet()) {
                timeline.put(e.getKey(), e.getValue());
            }

            // Removed intervals of disconnection as calculated using the heartbeat
            CollectionsImpl.Intervals intervals = new CollectionsImpl.IntervalsImpl();
            /*tracked:
            for (Interval tracked : trackedIntervals) {
                for (Interval disconnected : disconnectedIntervals) {
                    if (disconnected.contains(tracked)) {
                        continue tracked;
                    }
                    if (tracked.contains(disconnected)) {
                        intervals.add(new Interval(tracked.getStart(), disconnected.getStart()));
                        intervals.add(new Interval(disconnected.getEnd(), tracked.getEnd()));
                        continue tracked;
                    }
                    if (disconnected.overlaps(tracked)) {
                        Interval overlap = disconnected.overlap(tracked);
                        if (overlap.getStart().isAfter(tracked.getStart())) {
                            intervals.add(new Interval(tracked.getStart(), overlap.getStart()));
                        } else {
                            intervals.add(new Interval(overlap.getEnd(), tracked.getEnd()));
                        }
                        continue tracked;
                    }
                }
                intervals.add(tracked);
            }*/
            intervals.addAll(trackedIntervals);

            // Select only tracked intervals
            List<CollectionsImpl.Timeline> timelines = new ArrayList<>();
            for (Interval interval : intervals) {
                CollectionsImpl.Timeline t = new CollectionsImpl.TimelineImpl();
                timelines.add(t);
                for (Map.Entry<DateTime, Epoch> entry : timeline.entrySet()) {
                    if (interval.contains(entry.getKey())) {
                        t.put(entry.getKey(), entry.getValue());
                    }
                }
            }

            // Removed intervals of disconnection as calculated using zero counts
            List<CollectionsImpl.Timeline> timelines2 = new ArrayList<>();
            final int C = config.getEpochsForInactive();
            for (CollectionsImpl.Timeline from : timelines) {
                CollectionsImpl.Timeline to = new CollectionsImpl.TimelineImpl();
                boolean empty = false;
                CollectionsImpl.Timeline buffer = null;
                for (final Map.Entry<DateTime, Epoch> entry : from.entrySet()) {
                    if (!empty) {
                        if (entry.getValue().getCount() > 0) {
                            to.put(entry.getKey(), entry.getValue());
                        } else {
                            empty = true;
                            buffer = new CollectionsImpl.TimelineImpl();
                            buffer.put(entry.getKey(), entry.getValue());
                        }
                    } else {
                        if (entry.getValue().getCount() > 0) {
                            if (buffer.size() < C) {
                                to.putAll(buffer);
                            } else {
                                timelines2.add(to);
                                to = new CollectionsImpl.TimelineImpl();
                            }
                            empty = false;
                            buffer = null;
                            to.put(entry.getKey(), entry.getValue());
                        } else {
                            buffer.put(entry.getKey(), entry.getValue());
                        }
                    }
                }
                if (buffer != null && buffer.size() < C) {
                    to.putAll(buffer);
                }
                if (to.size() > 0) {
                    timelines2.add(to);
                }
            }
            timelines = timelines2;

            // Apply the algorithm to each tracked interval
            for (CollectionsImpl.Timeline tl : timelines) {
                Collections.Periods ps = applyAlgorithm(config, tl);
                if (ps.size() > 0) {
                    periods.add(ps);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return periods;
    }

    private CollectionsImpl.Timeline makeEmptyTimeline(Config config) {
        CollectionsImpl.TimelineImpl timeline = new CollectionsImpl.TimelineImpl();
        Duration duration = config.getEpochDuration();
        DateTime start = config.getDay().getStart();
        DateTime end = config.getDay().getEnd();
        Epoch current = new Epoch(new Interval(start, duration));
        while (current.getInterval().getEnd().isBefore(end)) {
            timeline.put(current.getInterval().getStart(), current);
            current = new Epoch(new Interval(current.getInterval().getEnd(), duration));
        }
        return timeline;
    }

    private Collections.Periods applyAlgorithm(Config config, CollectionsImpl.Timeline timeline) {
        Collections.Periods periods = new CollectionsImpl.PeriodsImpl();
        final int X = config.getX();
        final int Y = config.getY();
        final int C0 = config.getC0();
        final int C1 = config.getC1();
        final int Z1 = config.getZ1();
        final int P = config.getP();
        final int Q = config.getQ();

        final List<Pair<Period.Type, List<Epoch>>> firstPass = new ArrayList<>();
        final Iterator<Map.Entry<DateTime, Epoch>> i = timeline.entrySet().iterator();
        List<Epoch> currentEpochs = new ArrayList<>();

        int xCount = 0;
        int yCount = 0;
        int totalCount = 0;
        int epochCount = 0;
        boolean breakRegister = true;

        while (i.hasNext()) {
            Epoch epoch = i.next().getValue();
            currentEpochs.add(epoch);
            if (breakRegister) {
                yCount = ((epoch.getCount() > C1) ? 0 : (yCount + 1));
                totalCount += epoch.getCount();
                epochCount++;
                if ((yCount >= Y && epoch.getManualStatus().equals(Epoch.ManualStatus.None))
                        || epoch.getManualStatus().equals(Epoch.ManualStatus.Inactive)) {
                    breakRegister = false;
                    List<Epoch> segment = new ArrayList<>(currentEpochs.subList(
                            Math.max(currentEpochs.size() - yCount, 0),
                            currentEpochs.size()));
                    yCount = 0;
                    xCount = 0;
                    totalCount = 0;
                    epochCount = 0;
                    currentEpochs.removeAll(segment);
                    firstPass.add(new Pair<>(Period.Type.Active, currentEpochs));
                    currentEpochs = new ArrayList<>(segment);
                }
            } else {
                xCount = ((epoch.getCount() > C0) ? (xCount + 1) : 0);
                totalCount += epoch.getCount();
                epochCount++;
                if ((xCount >= X && epoch.getManualStatus().equals(Epoch.ManualStatus.None))
                        || (totalCount >= P && epochCount >= Q && epoch.getManualStatus().equals(
                        Epoch.ManualStatus.None))
                        || epoch.getManualStatus().equals(Epoch.ManualStatus.Active)) {
                    breakRegister = true;
                    List<Epoch> segment = new ArrayList<>(currentEpochs.subList(
                            Math.max(currentEpochs.size() - xCount, 0),
                            currentEpochs.size()));
                    xCount = 0;
                    yCount = 0;
                    totalCount = 0;
                    epochCount = 0;
                    currentEpochs.removeAll(segment);
                    firstPass.add(new Pair<>(Period.Type.Healthy, currentEpochs));
                    currentEpochs = new ArrayList<>(segment);
                }
            }

        }
        firstPass.add(new Pair<>(breakRegister
                ? Period.Type.Active : Period.Type.Healthy, currentEpochs));

        int j = 0;
        while (j < firstPass.size()) {
            Pair<Period.Type, List<Epoch>> p = firstPass.get(j);
            if (p.first == Period.Type.Healthy) {
                if (j + 2 < firstPass.size()) {
                    Pair<Period.Type, List<Epoch>> p2 = firstPass.get(j + 1);
                    Pair<Period.Type, List<Epoch>> p3 = firstPass.get(j + 2);
                    if (p2.second.size() <= Z1) {
                        firstPass.remove(j);
                        firstPass.remove(j);
                        firstPass.remove(j);
                        List<Epoch> es = new ArrayList<>();
                        es.addAll(p.second);
                        es.addAll(p2.second);
                        es.addAll(p3.second);
                        firstPass.add(j, new Pair<>(Period.Type.Healthy, es));
                        j = 0;
                    } else {
                        j++;
                    }
                } else {
                    j++;
                }
            } else {
                j++;
            }
        }

        for (Pair<Period.Type, List<Epoch>> p : firstPass) {
            if (p.second.size() == 0) {
                continue;
            }
            if (p.first == Period.Type.Healthy) {
                Epoch f = p.second.get(0);
                Epoch l = p.second.get(p.second.size() - 1);
                Duration d = new Duration(
                        f.getInterval().getStart(),
                        l.getInterval().getEnd());
                if (d.isShorterThan(config.getProlongedPeriodDuration())) {
                    periods.add(new Period(Period.Type.Healthy, new TreeSet<>(p.second)));
                } else {
                    periods.add(new Period(Period.Type.Prolonged, new TreeSet<>(p.second)));
                }
            } else {
                periods.add(new Period(p.first, new TreeSet<>(p.second)));
            }
        }
        return periods;
    }

}
