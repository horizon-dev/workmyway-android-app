package uk.ac.horizon.workmyway.metawear;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;

import com.mbientlab.metawear.MetaWearBoard;
import com.mbientlab.metawear.Route;
import com.mbientlab.metawear.android.BtleService;
import com.mbientlab.metawear.module.AccelerometerBmi160;
import com.mbientlab.metawear.module.AccelerometerBosch;
import com.mbientlab.metawear.module.Logging;
import com.mbientlab.metawear.module.Settings;
import com.mbientlab.metawear.module.Temperature;
import com.mbientlab.metawear.module.Timer;

import java.io.IOException;
import java.util.Set;

import bolts.Task;
import uk.ac.horizon.workmyway.BuildConfig;
import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.model.DeviceType;
import uk.ac.horizon.workmyway.util.Log;
import uk.ac.horizon.workmyway.util.Utils;

class CupDeviceConnection
        extends DeviceConnection
        implements SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = CupDeviceConnection.class.getSimpleName();

    CupDeviceConnection(
            Context context,
            BtleService.LocalBinder service,
            String mac,
            Set<DeviceListener> deviceListeners) {
        super(context, service, mac, DeviceType.Cup, deviceListeners);
        PreferenceManager.getDefaultSharedPreferences(context)
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
        Resources r = context.getResources();
        if (key != null && (
                key.equals(r.getString(R.string.pk_tap_detector_threshold))
                        || key.equals(r.getString(R.string.pk_tap_detector_quiet_time))
                        || key.equals(r.getString(R.string.pk_tap_detector_shock_time))
                        || key.equals(r.getString(R.string.pk_tap_detector_double_tap_window))
                        || key.equals(r.getString(R.string.pk_accelerometer_range))
                        || key.equals(r.getString(R.string.pk_accelerometer_odr))
                        || key.equals(r.getString(R.string.pk_step_detector_mode))
                        || key.equals(r.getString(R.string.pk_significant_motion_proof_time))
                        || key.equals(r.getString(R.string.pk_significant_motion_skip_time)))) {
            AccelerometerBmi160 acc = board.getModule(AccelerometerBmi160.class);
            configureAccelerometer(acc);
        }
    }

    @Override
    void reset() {
        SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(context);
        p.edit()
                .remove(getDeviceKey(R.string.pk_cup_device_state))
                .remove(getDeviceKey(R.string.pk_cup_device_acc_stream_route_id))
                .remove(getDeviceKey(R.string.pk_cup_device_tap_stream_route_id))
                .remove(getDeviceKey(R.string.pk_cup_device_step_stream_route_id))
                .remove(getDeviceKey(R.string.pk_cup_device_step_log_route_id))
                .remove(getDeviceKey(R.string.pk_cup_device_motion_stream_route_id))
                .remove(getDeviceKey(R.string.pk_cup_device_motion_log_route_id))
                .remove(getDeviceKey(R.string.pk_cup_device_batt_route_id))
                .remove(getDeviceKey(R.string.pk_cup_device_heartbeat_log_route_id))
                .remove(getDeviceKey(R.string.pk_cup_device_heartbeat_timer_task_id))
                .apply();
    }

    @Override
    void onConnection(final MetaWearBoard board, DeviceReady deviceReady) {
        waitOnConnection(10000L);
        final SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(context);
        final String pkState = getDeviceKey(R.string.pk_cup_device_state);
        final int version = p.getInt(context.getString(R.string.pk_current_version), 0);
        String state = p.getString(pkState, null);
        if (BuildConfig.VERSION_CODE != version) {
            state = null;
            p.edit().putInt(
                    context.getString(R.string.pk_current_version),
                    BuildConfig.VERSION_CODE).apply();
            Log.d(TAG, "Version codes differ, reinitialising device");
        }
        if (state != null) {
            try {
                Utils.Metawear.deserializeState(board, state);
            } catch (Exception e) {
                Log.e(TAG, "Error deserialising device state, reinitialising device", e);
                state = null;
            }
        }
        if (state != null) {
            onConnectExisting(board, deviceReady);
        } else {
            onConnectNew(board, deviceReady);
        }
    }

    private void onConnectExisting(MetaWearBoard board, DeviceReady deviceReady) {
        Log.d(TAG, "Restoring device state");
        while (!board.isConnected()) {
            android.util.Log.d(TAG, "NOT CONNECTED");
        }
        final SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(context);
        final String[] keys = {
                getDeviceKey(R.string.pk_cup_device_tap_stream_route_id),
                getDeviceKey(R.string.pk_cup_device_motion_stream_route_id),
                getDeviceKey(R.string.pk_cup_device_motion_log_route_id),
                getDeviceKey(R.string.pk_cup_device_step_stream_route_id),
                getDeviceKey(R.string.pk_cup_device_step_log_route_id),
                getDeviceKey(R.string.pk_cup_device_batt_route_id),
                getDeviceKey(R.string.pk_cup_device_heartbeat_log_route_id),
                getDeviceKey(R.string.pk_cup_device_heartbeat_stream_route_id)
        };
        for (String key : keys) {
            int id = p.getInt(key, -1);
            if (id != -1) {
                Route route = board.lookupRoute(p.getInt(key, -1));
                if (route != null) {
                    route.setEnvironment(0, deviceType, deviceListeners);
                } else {
                    android.util.Log.w(TAG, "Couldn't get route: " + key);
                }
            }
        }
        Logging log = board.getModule(Logging.class);
        log.stop();
        log.downloadAsync();

        /*final String pkHeartbeatTask = getDeviceKey(
                R.string.pk_cup_device_heartbeat_timer_task_id);
        byte taskID = Integer.valueOf(p.getInt(pkHeartbeatTask, -1)).byteValue();
        Timer timer = board.getModule(Timer.class);
        Timer.ScheduledTask t = timer.lookupScheduledTask(taskID);
        if (t != null) {
            Log.e(TAG, "Heartbeat task is not found");
        }*/

        CommonTasks
                .disconnectAction(
                        context.getResources().getColor(R.color.device_disconnected_led))
                .doTask(board);
        CommonTasks
                .pulseLed(context.getResources().getColor(R.color.device_connected_led), 3)
                .doTask(board);
        board.getModule(Settings.class).battery().read();
        deviceReady.onReady();
        Log.d(TAG, "Device state restored");
    }

    private void onConnectNew(final MetaWearBoard board, final DeviceReady deviceReady) {
        Log.d(TAG, "Setting up new device");
        board.tearDown();
        final SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(context);
        final String pkState = getDeviceKey(R.string.pk_cup_device_state);
        final String pkMotionStream = getDeviceKey(
                R.string.pk_cup_device_motion_stream_route_id);
        final String pkMotionLog = getDeviceKey(R.string.pk_cup_device_motion_log_route_id);
        final String pkStepStream = getDeviceKey(R.string.pk_cup_device_step_stream_route_id);
        final String pkStepLog = getDeviceKey(R.string.pk_cup_device_step_log_route_id);
        final String pkBattRoute = getDeviceKey(R.string.pk_cup_device_batt_route_id);
        final String pkHeartbeatLog = getDeviceKey(
                R.string.pk_cup_device_heartbeat_log_route_id);
        final String pkHeartbeatStream = getDeviceKey(
                R.string.pk_cup_device_heartbeat_stream_route_id);
        final String pkHeartbeatTask = getDeviceKey(
                R.string.pk_cup_device_heartbeat_timer_task_id);
        final String pkTapStream = getDeviceKey(R.string.pk_cup_device_tap_stream_route_id);

        final Settings settings = board.getModule(Settings.class);
        final AccelerometerBmi160 acc = board.getModule(AccelerometerBmi160.class);
        final AccelerometerBmi160.SignificantMotionDataProducer motion =
                acc.motion(AccelerometerBmi160.SignificantMotionDataProducer.class);
        final AccelerometerBmi160.StepDetectorDataProducer step = acc.stepDetector();
        final Timer timer = board.getModule(Timer.class);
        final Temperature temperature = board.getModule(Temperature.class);
        final Temperature.Sensor tempSensor = temperature.findSensors(
                Temperature.SensorType.PRESET_THERMISTOR)[0];
        settings.editBleConnParams()
                .maxConnectionInterval(7.5f)
                .commit();
        final Logging log = board.getModule(Logging.class);
        log.stop();
        board.tearDown();
        acc.tap().stop();
        acc.acceleration().stop();
        acc.stop();
        motion.stop();
        step.stop();

        configureAccelerometer(acc);

        acc.tap().addRouteAsync((source) -> source.stream(tapSubscriber)).continueWithTask(
                new RouteContinuation<>(
                        route -> {
                            Log.d(TAG, "Registered tapSubscriber");
                            p.edit().putInt(pkTapStream, route.id()).apply();
                        },
                        () -> motion.addRouteAsync(source -> source.stream(motionSubscriber))
                )).continueWithTask(
                new RouteContinuation<>(
                        route -> {
                            Log.d(TAG, "Registered motionSubscriber");
                            p.edit().putInt(pkMotionStream, route.id()).apply();
                        },
                        () -> motion.addRouteAsync(source -> source.log(motionLogSubscriber))
                )).continueWithTask(
                new RouteContinuation<>(
                        route -> {
                            Log.d(TAG, "Registered motionLogSubscriber");
                            p.edit().putInt(pkMotionLog, route.id()).apply();
                        },
                        () -> step.addRouteAsync(source -> source.stream(stepCountSubscriber))
                )).continueWithTask(
                new RouteContinuation<>(
                        route -> {
                            Log.d(TAG, "Registered stepCountSubscriber");
                            p.edit().putInt(pkStepStream, route.id()).apply();
                        },
                        () -> step.addRouteAsync(source -> source.log(stepCountLogSubscriber))
                )).continueWithTask(
                new RouteContinuation<>(
                        route -> {
                            Log.d(TAG, "Registered stepCountLogSubscriber");
                            p.edit().putInt(pkStepLog, route.id()).apply();
                        },
                        () -> board.getModule(Settings.class).battery().addRouteAsync(
                                source -> source.stream(batterySubscriber))
                )).continueWithTask(
                new RouteContinuation<>(
                        route -> {
                            Log.d(TAG, "Registered batterySubscriber");
                            p.edit().putInt(pkBattRoute, route.id()).apply();
                        },
                        () -> tempSensor.addRouteAsync(
                                source -> source.stream(heartbeatSubscriber))
                )).continueWithTask(
                new RouteContinuation<>(
                        route -> {
                            Log.d(TAG, "Registered heartbeatSubscriber (stream)");
                            p.edit().putInt(pkHeartbeatStream, route.id()).apply();
                        },
                        () -> tempSensor.addRouteAsync(
                                source -> source.log(heartbeatSubscriber))
                )).continueWithTask(
                new RouteContinuation<>(
                        route -> {
                            Log.d(TAG, "Registered heartbeatSubscriber (log)");
                            p.edit().putInt(pkHeartbeatLog, route.id()).apply();
                        },
                        () -> timer.scheduleAsync(15 * 1000, false, tempSensor::read)
                )).continueWithTask(
                new Continuation<>(
                        task -> {
                            Log.d(TAG, "Registered heartbeat task");
                            task.start();
                            p.edit().putInt(pkHeartbeatTask, task.id()).apply();
                        },
                        () -> Task.forResult(null)
                )).continueWith(
                new Continuation<Object, Void>(
                        v -> {
                            try {
                                p.edit().putString(
                                        pkState,
                                        Utils.Metawear.serializeState(board)).apply();
                            } catch (IOException e) {
                                Log.e(TAG, e.getMessage(), e);
                            }
                            acc.acceleration().start();
                            acc.tap().start();
                            acc.start();
                            motion.start();
                            step.start();

                            Resources r = context.getResources();
                            CommonTasks
                                    .disconnectAction(
                                            r.getColor(R.color.device_disconnected_led))
                                    .doTask(board);
                            CommonTasks
                                    .pulseLed(r.getColor(R.color.device_connected_led), 3)
                                    .doTask(board);
                            try {
                                Thread.sleep(3000);
                            } catch (InterruptedException e) {
                                Log.w(TAG, e.getMessage(), e);
                            }
                            settings.battery().read();
                            deviceReady.onReady();
                        },
                        () -> Task.forResult(null)
                ));
    }

    private void configureAccelerometer(AccelerometerBmi160 acc) {
        SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(context);

        acc.configure()

                .range(AccelerometerBosch.AccRange.valueOf(p.getString(
                        context.getString(R.string.pk_accelerometer_range),
                        context.getString(R.string.metawear_accelerometer_range_default))))

                .odr(AccelerometerBmi160.OutputDataRate.valueOf(p.getString(
                        context.getString(R.string.pk_accelerometer_odr),
                        context.getString(R.string.metawear_accelerometer_odr_default))))

                .commit();

        acc.stepDetector().configure()

                .mode(AccelerometerBmi160.StepDetectorMode.valueOf(p.getString(
                        context.getString(R.string.pk_step_detector_mode),
                        context.getString(R.string.metawear_step_detector_default))))

                .commit();

        acc.motion(AccelerometerBmi160.SignificantMotionDataProducer.class).configure()

                .proofTime(AccelerometerBmi160.ProofTime.valueOf(p.getString(
                        context.getString(R.string.pk_significant_motion_proof_time),
                        context.getString(
                                R.string.metawear_motion_detector_proof_time_default))))

                .skipTime(AccelerometerBmi160.SkipTime.valueOf(p.getString(
                        context.getString(R.string.pk_significant_motion_skip_time),
                        context.getString(
                                R.string.metawear_motion_detector_skip_time_default))))

                .commit();

        acc.tap().configure().enableDoubleTap()

                .threshold(Float.parseFloat(p.getString(
                        context.getString(R.string.pk_tap_detector_threshold),
                        context.getString(R.string.metawear_tap_detector_threshold_default))))

                .quietTime(AccelerometerBosch.TapQuietTime.valueOf(p.getString(
                        context.getString(R.string.pk_tap_detector_quiet_time),
                        context.getString(R.string.metawear_tap_detector_quiet_time_default))))

                .shockTime(AccelerometerBosch.TapShockTime.valueOf(p.getString(
                        context.getString(R.string.pk_tap_detector_shock_time),
                        context.getString(R.string.metawear_tap_detector_shock_time_default))))

                .doubleTapWindow(AccelerometerBosch.DoubleTapWindow.valueOf(p.getString(
                        context.getString(R.string.pk_tap_detector_double_tap_window),
                        context.getString(
                                R.string.metawear_tap_detector_double_tap_window_default))))

                .commit();
    }

}
