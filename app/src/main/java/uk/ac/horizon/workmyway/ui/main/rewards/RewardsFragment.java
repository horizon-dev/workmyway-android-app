package uk.ac.horizon.workmyway.ui.main.rewards;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.WorkMyWay;
import uk.ac.horizon.workmyway.db.ASyncDatabase;
import uk.ac.horizon.workmyway.util.Log;

public class RewardsFragment extends Fragment {

    private static final String TAG = RewardsFragment.class.getSimpleName();

    @Override
    public View onCreateView(
            final LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment__ui_main_rewards__rewards, container, false);
        View b = v.findViewById(R.id.update_goals_button);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new PersonalGoalsFragment().show(getFragmentManager(), "");
            }
        });

        final ListView lv = v.findViewById(R.id.listview);
        new Thread(new Runnable() {
            @Override
            public void run() {

                ASyncDatabase db = WorkMyWay.getDatabase();
                final List<JSONObject> grs = db.getGoalRewards();
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                boolean threehours = prefs.getBoolean(getString(R.string.pk_three_hours_use), false);
                int timeRewards = 0;
                int sessionRewards = 0;
                for (JSONObject o : db.getGoalRewards()) {
                    try {
                        String type = o.getString("type");
                        if ("sitting".equals(type)) {
                            timeRewards++;
                        } else if ("break".equals(type)) {
                            sessionRewards++;
                        }
                    } catch (JSONException e) {
                        Log.w(TAG, e.getMessage(), e);
                    }
                }

                final ArrayList<View> views = new ArrayList<>();

                if (threehours) {
                    View v = inflater.inflate(R.layout.fragment__ui_common__target, null);
                    TextView tv = v.findViewById(R.id.description);
                    tv.setText("You used the app for 3 hours");
                    views.add(v);
                }

                if (timeRewards > 0) {
                    View v = inflater.inflate(R.layout.fragment__ui_common__target, null);
                    TextView tv = v.findViewById(R.id.description);
                    tv.setText("Time spent sitting reduced");
                    TextView cv = v.findViewById(R.id.count);
                    cv.setText("Total badges: " + timeRewards);
                    views.add(v);
                }

                if (sessionRewards > 0) {
                    View v = inflater.inflate(R.layout.fragment__ui_common__target, null);
                    TextView tv = v.findViewById(R.id.description);
                    tv.setText("Sessions spent sitting reduced");
                    TextView cv = v.findViewById(R.id.count);
                    cv.setText("Total badges: " + sessionRewards);
                    views.add(v);
                }

                List<JSONObject> objs = db.getDayRewards(); {
                    View v = inflater.inflate(R.layout.fragment__ui_common__trophy, null);
                    TextView tv = v.findViewById(R.id.description);
                    tv.setText("Days healthier than the previous");
                    TextView cv = v.findViewById(R.id.count);
                    cv.setText("Total badges: " + objs.size());
                    views.add(v);
                }


                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        lv.setAdapter(new BaseAdapter() {
                            @Override
                            public int getCount() {
                                return views.size();
                            }

                            @Override
                            public Object getItem(int i) {
                                return views.get(i);
                            }

                            @Override
                            public long getItemId(int i) {
                                return views.get(i).hashCode();
                            }

                            @Override
                            public View getView(int i, View view, ViewGroup viewGroup) {
                                return views.get(i);
                            }
                        });
                    }
                });
            }
        }).start();

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        WorkMyWay.getAnalytics().setCurrentScreen(
                getActivity(), this.getClass().getSimpleName(), this.getClass().getName());
    }

}
