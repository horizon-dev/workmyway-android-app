package uk.ac.horizon.workmyway.model.base;

import android.support.annotation.NonNull;

import org.joda.time.Interval;

import java.util.Collections;
import java.util.SortedSet;

class Period
        implements
        uk.ac.horizon.workmyway.model.Period,
        Comparable<uk.ac.horizon.workmyway.model.Period> {

    private final SortedSet<? extends uk.ac.horizon.workmyway.model.Epoch> epochs;

    private final Interval interval;

    private final Type type;


    Period(@NonNull Type type, @NonNull SortedSet<? extends uk.ac.horizon.workmyway.model.Epoch> epochs) {
        if (epochs.size() == 0) {
            throw new RuntimeException("A period must contain at least one Epoch");
        }
        this.type = type;
        this.epochs = Collections.unmodifiableSortedSet(epochs);
        interval = new Interval(
                epochs.first().getInterval().getStart(),
                epochs.last().getInterval().getEnd());
    }

    @Override
    @NonNull
    public SortedSet<? extends uk.ac.horizon.workmyway.model.Epoch> getEpochs() {
        return epochs;
    }

    @Override
    @NonNull
    public Interval getInterval() {
        return interval;
    }

    @Override
    @NonNull
    public Type getType() {
        return type;
    }

    @Override
    public int compareTo(@NonNull uk.ac.horizon.workmyway.model.Period another) {
        return interval.getStart().compareTo(another.getInterval().getStart());
    }

}
