package uk.ac.horizon.workmyway.ui.main.rewards;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import biz.kasual.materialnumberpicker.MaterialNumberPicker;
import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.WorkMyWay;

public class PersonalGoalsFragment extends DialogFragment {

    private int sittingTime;

    private int breakCount;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        @SuppressLint("InflateParams")
        View v = inflater.inflate(R.layout.fragment__ui_main_rewards__personal_goals, null);
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        final Resources res = getResources();
        final TextView sittingView = (TextView) v.findViewById(R.id.personal_goal_sitting_text);
        sittingTime = prefs.getInt(
                getString(R.string.pk_personal_goal_time),
                res.getInteger(R.integer.personal_goal_time_default));
        String sittingText = String.format(res.getQuantityString(R.plurals.sitting_text, sittingTime), sittingTime);
        sittingView.setText(Html.fromHtml(sittingText));
        sittingView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final MaterialNumberPicker numberPicker = new MaterialNumberPicker.Builder(getActivity())
                        .minValue(1)
                        .maxValue(180)
                        .defaultValue(sittingTime)
                        .backgroundColor(Color.WHITE)
                        .separatorColor(Color.BLACK)
                        .textColor(Color.BLACK)
                        .textSize(20)
                        .enableFocusability(false)
                        .wrapSelectorWheel(true)
                        .build();
                new AlertDialog.Builder(getActivity())
                        .setView(numberPicker)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                sittingTime = numberPicker.getValue();
                                String sittingText = String.format(res.getQuantityString(R.plurals.sitting_text, sittingTime), sittingTime);
                                sittingView.setText(Html.fromHtml(sittingText));
                            }
                        })
                        .show();

            }
        });
        final TextView breakView = (TextView) v.findViewById(R.id.personal_goal_break_text);
        breakCount = prefs.getInt(
                getString(R.string.pk_personal_goal_breaks),
                res.getInteger(R.integer.personal_goal_breaks_default));
        String breakText = String.format(res.getQuantityString(R.plurals.break_text, breakCount), breakCount);
        breakView.setText(Html.fromHtml(breakText));
        breakView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final MaterialNumberPicker numberPicker = new MaterialNumberPicker.Builder(getActivity())
                        .minValue(0)
                        .maxValue(10)
                        .defaultValue(breakCount)
                        .backgroundColor(Color.WHITE)
                        .separatorColor(Color.BLACK)
                        .textColor(Color.BLACK)
                        .textSize(20)
                        .enableFocusability(false)
                        .wrapSelectorWheel(true)
                        .build();
                new AlertDialog.Builder(getActivity())
                        .setView(numberPicker)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                breakCount = numberPicker.getValue();
                                String breakText = String.format(res.getQuantityString(R.plurals.break_text, breakCount), breakCount);
                                breakView.setText(Html.fromHtml(breakText));
                            }
                        })
                        .show();
            }
        });
        builder.setView(v)
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        prefs.edit().putInt(getString(R.string.pk_personal_goal_time), sittingTime).apply();
                        prefs.edit().putInt(getString(R.string.pk_personal_goal_breaks), breakCount).apply();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        PersonalGoalsFragment.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }

    @Override
    public void onResume() {
        super.onResume();
        WorkMyWay.getAnalytics().setCurrentScreen(
                getActivity(), this.getClass().getSimpleName(), this.getClass().getName());
    }

}
