package uk.ac.horizon.workmyway.ui.common;

import android.content.Context;
import android.preference.Preference;
import android.util.AttributeSet;

import uk.ac.horizon.workmyway.util.VoidCallback;

public class ActionPreference extends Preference {

    private VoidCallback<Void> action;

    @SuppressWarnings("unused")
    public ActionPreference(Context context) {
        super(context);
    }

    @SuppressWarnings("unused")
    public ActionPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @SuppressWarnings("unused")
    public ActionPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @SuppressWarnings("unused")
    public ActionPreference(
            Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public synchronized void setAction(VoidCallback<Void> action) {
        this.action = action;
    }

    @Override
    protected synchronized void onClick() {
        if (action != null) {
            action.doCallback(null);
        }
    }

}
