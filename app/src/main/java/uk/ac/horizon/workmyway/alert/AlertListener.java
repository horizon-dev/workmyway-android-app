package uk.ac.horizon.workmyway.alert;

import org.joda.time.Interval;

public interface AlertListener {

    void onLevelChanged(AlertLevel level);

    void onBeginPause(Interval interval);

    void onEndPause(Interval interval);

}
