package uk.ac.horizon.workmyway.alert;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Binder;
import android.os.IBinder;
import android.preference.PreferenceManager;

import org.joda.time.Duration;
import org.joda.time.Instant;
import org.joda.time.Interval;

import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.WorkMyWay;
import uk.ac.horizon.workmyway.alert.alertlisteners.Database;
import uk.ac.horizon.workmyway.model.Collections;
import uk.ac.horizon.workmyway.model.Model;
import uk.ac.horizon.workmyway.model.ModelManager;
import uk.ac.horizon.workmyway.model.Period;
import uk.ac.horizon.workmyway.util.Log;

public class AlertService extends Service {

    private static final String TAG = AlertService.class.getSimpleName();

    public class LocalBinder extends Binder {

        public void addAlertListener(AlertListener listener) {
            synchronized (lock) {
                listeners.add(listener);
                if (currentPauseInterval != null) {
                    listener.onBeginPause(currentPauseInterval);
                }
                listener.onLevelChanged(currentLevel);
            }
        }

        public void removeAlertListener(AlertListener listener) {
            synchronized (lock) {
                listeners.remove(listener);
            }
        }

        public void pause() {
            AlertService.this.pause();
        }

    }

    private final Object lock = new Object();

    private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

    private final LocalBinder localBinder = new LocalBinder();

    private final Set<AlertListener> listeners = new HashSet<>();

    private AlertLevel currentLevel = AlertLevel.None;

    private Interval currentPauseInterval;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final Resources res = getResources();
        executor.submit(new Runnable() {
            @Override
            public void run() {
                synchronized (lock) {
                    AlertLevel level = getAlertLevel();
                    if (!level.equals(currentLevel)) {
                        currentLevel = level;
                        for (AlertListener l : listeners) {
                            try {
                                l.onLevelChanged(level);
                            } catch (Exception e) {
                                Log.w(TAG, "Error with AlertListener", e);
                            }
                        }
                    }
                }
                if (!executor.isShutdown()) {
                    int sleep = Integer.parseInt(prefs.getString(
                            res.getString(R.string.pk_epoch_time),
                            res.getString(R.string.epoch_time_default)));
                    executor.schedule(this, sleep, TimeUnit.SECONDS);
                }
            }

        });
        return Service.START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        localBinder.addAlertListener(new Database(this, WorkMyWay.getDatabase()));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        executor.shutdown();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return localBinder;
    }

    private AlertLevel getAlertLevel() {
        try {
            final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            final Resources res = getResources();

            Period current = null;
            ModelManager modelManager = WorkMyWay.getModelManager();
            Model model = modelManager.getModelForToday();
            Collections.ModelPeriods periods = model.getPeriods();
            if (periods.size() > 0) {
                SortedSet<? extends Period> currentPeriods = periods.last();
                if (currentPeriods.size() > 0) {
                    current = currentPeriods.last();
                }
            }

            Duration breakApproaching = Duration.standardMinutes(Integer.parseInt(prefs.getString(
                    res.getString(R.string.pk_first_alert_time),
                    res.getString(R.string.first_alert_time_default))));
            Duration breakDue = Duration.standardMinutes(Integer.parseInt(prefs.getString(
                    res.getString(R.string.pk_second_alert_time),
                    res.getString(R.string.second_alert_time_default))));
            Duration breakOverdue = Duration.standardMinutes(Integer.parseInt(prefs.getString(
                    res.getString(R.string.pk_third_alert_time),
                    res.getString(R.string.third_alert_time_default))));
            if (current != null) {
                switch (current.getType()) {
                    case Active:
                        return AlertLevel.None;
                    case Healthy:
                    case Prolonged:
                        Duration d = current.getInterval().toDuration();
                        if (d.isLongerThan(breakOverdue)) {
                            return AlertLevel.BreakOverdue;
                        } else if (d.isLongerThan(breakDue)) {
                            return AlertLevel.BreakDue;
                        } else if (d.isLongerThan(breakApproaching)) {
                            return AlertLevel.BreakApproaching;
                        } else {
                            return AlertLevel.None;
                        }
                    default:
                        return AlertLevel.None;
                }
            }
            return AlertLevel.None;
        } catch (Exception e) {
            return AlertLevel.None;
        }
    }

    private void pause() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Resources res = getResources();
        final int minutes = Integer.parseInt(prefs.getString(
                res.getString(R.string.pk_alert_snooze_time),
                res.getString(R.string.alert_snooze_time_default)));
        final Interval i = Duration.standardMinutes(minutes).toIntervalFrom(Instant.now());
        executor.schedule(() -> {
            synchronized (lock) {
                for (AlertListener l : listeners) {
                    l.onEndPause(i);
                }
            }
        }, minutes, TimeUnit.MINUTES);
        synchronized (lock) {
            if (currentPauseInterval == null
                    || i.getEnd().isAfter(currentPauseInterval.getEnd())) {
                currentPauseInterval = i;
            }
            for (AlertListener l : listeners) {
                l.onBeginPause(i);
            }
        }
    }

}
