package uk.ac.horizon.workmyway.util;


public class Enclosure<T> {

    private T val;

    public T get() {
        return val;
    }

    public void set(T val) {
        this.val = val;
    }

}
