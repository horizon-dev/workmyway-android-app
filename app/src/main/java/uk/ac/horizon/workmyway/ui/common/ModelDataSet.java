package uk.ac.horizon.workmyway.ui.common;

import android.content.Context;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Typeface;

import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.BaseDataSet;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.ViewPortHandler;

import org.joda.time.Duration;

import java.util.ArrayList;
import java.util.List;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.model.Model;
import uk.ac.horizon.workmyway.model.Period;
import uk.ac.horizon.workmyway.util.Utils;

public class ModelDataSet
        extends BaseDataSet<BarEntry>
        implements IBarDataSet {

    private final BarDataSet barDataSet;

    private final List<Integer> colors = new ArrayList<>();

    public ModelDataSet(Model model, Context context) {

        final int color_active = context.getResources().getColor(R.color.activity_active);
        final int color_healthy = context.getResources().getColor(R.color.activity_healthy);
        final int color_prolonged = context.getResources().getColor(R.color.activity_prolonged);
        final int color_transparent = context.getResources().getColor(R.color.transparent);

        List<Integer> aColors = new ArrayList<>();
        List<Integer> iColors = new ArrayList<>();
        final List<? extends Period> periods
                = new ArrayList<>(Utils.Model.flattenPeriods(model));
        float[] periodVals = new float[periods.size()];

        int i = 0;
        for (Period p : periods) {
            periodVals[i] = ((float) p.getInterval().toDuration().getMillis());
            if (p instanceof Utils.Model.UntrackedPeriod) {
                aColors.add(color_transparent);
                iColors.add(color_transparent);
            } else {
                switch (p.getType()) {
                case Active:
                    aColors.add(color_active);
                    iColors.add(color_transparent);
                    break;
                case Healthy:
                    aColors.add(color_transparent);
                    iColors.add(color_healthy);
                    break;
                case Prolonged:
                    aColors.add(color_transparent);
                    iColors.add(color_prolonged);
                    break;
                }
            }
            i++;
        }

        colors.addAll(aColors);
        colors.addAll(iColors);

        List<BarEntry> allEntries = new ArrayList<>();
        allEntries.add(new BarEntry(1f, periodVals));
        allEntries.add(new BarEntry(0f, periodVals));

        barDataSet = new BarDataSet(allEntries, "Entries");
        barDataSet.setValueFormatter(new IValueFormatter() {

            private int counter = 0;
            private boolean activeBar = true;

            @Override
            public String getFormattedValue(
                    float value,
                    Entry entry,
                    int dataSetIndex,
                    ViewPortHandler viewPortHandler) {
                if (periods.size() == 0) {
                    return "";
                }
                Period p = periods.get(counter);
                String fv = "";
                if (!(p instanceof Utils.Model.UntrackedPeriod)
                        && ((p.getType().equals(Period.Type.Active) && activeBar)
                        || (!p.getType().equals(Period.Type.Active) && !activeBar))) {
                    Duration d = p.getInterval().toDuration();
                    StringBuilder buf = new StringBuilder();
                    if (d.getStandardHours() > 0) {
                        buf.append(d.getStandardHours()).append("h ");
                    }
                    buf.append(d.toPeriod().getMinutes()).append("m");
                    fv = buf.toString();
                }
                counter++;
                if (counter == periods.size()) {
                    counter = 0;
                    activeBar = !activeBar;
                }
                return fv;
            }

        });
        barDataSet.setValueTextColor(Color.BLACK);
        barDataSet.setValueTextSize(15);

    }

    @Override
    public List<Integer> getColors() {
        return colors;
    }

    @Override
    public List<Integer> getValueColors() {
        return barDataSet.getValueColors();
    }

    @Override
    public int getColor() {
        return colors.get(0);
    }

    @Override
    public int getColor(int index) {
        if (colors.size() == 0) {
            return Color.BLACK;
        }
        return colors.get(index);
    }

    @Override
    public int getStackSize() {
        return barDataSet.getStackSize();
    }

    @Override
    public boolean isStacked() {
        return barDataSet.isStacked();
    }

    @Override
    public int getBarShadowColor() {
        return barDataSet.getBarShadowColor();
    }

    @Override
    public float getBarBorderWidth() {
        return barDataSet.getBarBorderWidth();
    }

    @Override
    public int getBarBorderColor() {
        return barDataSet.getBarBorderColor();
    }

    @Override
    public int getHighLightAlpha() {
        return barDataSet.getHighLightAlpha();
    }

    @Override
    public String[] getStackLabels() {
        return barDataSet.getStackLabels();
    }

    @Override
    public int getHighLightColor() {
        return barDataSet.getHighLightColor();
    }

    @Override
    public void calcMinMax() {
        barDataSet.calcMinMax();
    }

    @Override
    public void calcMinMaxY(float fromX, float toX) {
        barDataSet.calcMinMaxY(fromX, toX);
    }

    @Override
    public int getEntryCount() {
        return barDataSet.getEntryCount();
    }

    @Override
    public String toString() {
        return barDataSet.toString();
    }

    @Override
    public float getYMin() {
        return barDataSet.getYMin();
    }

    @Override
    public float getYMax() {
        return barDataSet.getYMax();
    }

    @Override
    public float getXMin() {
        return barDataSet.getXMin();
    }

    @Override
    public float getXMax() {
        return barDataSet.getXMax();
    }

    @Override
    public void addEntryOrdered(BarEntry e) {
        barDataSet.addEntryOrdered(e);
    }

    @Override
    public void clear() {
        barDataSet.clear();
    }

    @Override
    public boolean addEntry(BarEntry e) {
        return barDataSet.addEntry(e);
    }

    @Override
    public boolean removeEntry(BarEntry e) {
        return barDataSet.removeEntry(e);
    }

    @Override
    public BarEntry getEntryForXValue(float xValue, float closestToY, DataSet.Rounding rounding) {
        return barDataSet.getEntryForXValue(xValue, closestToY, rounding);
    }

    @Override
    public BarEntry getEntryForXValue(float xValue, float closestToY) {
        return barDataSet.getEntryForXValue(xValue, closestToY);
    }

    @Override
    public BarEntry getEntryForIndex(int index) {
        return barDataSet.getEntryForIndex(index);
    }

    @Override
    public int getEntryIndex(float xValue, float closestToY, DataSet.Rounding rounding) {
        return barDataSet.getEntryIndex(xValue, closestToY, rounding);
    }

    @Override
    public List<BarEntry> getEntriesForXValue(float xValue) {
        return barDataSet.getEntriesForXValue(xValue);
    }

    @Override
    public void notifyDataSetChanged() {
        barDataSet.notifyDataSetChanged();
    }

    @Override
    public void setColors(List<Integer> colors) {
        barDataSet.setColors(colors);
    }

    @Override
    public void setColors(int... colors) {
        barDataSet.setColors(colors);
    }

    @Override
    public void setColors(int[] colors, Context c) {
        barDataSet.setColors(colors, c);
    }

    @Override
    public void addColor(int color) {
        barDataSet.addColor(color);
    }

    @Override
    public void setColor(int color) {
        barDataSet.setColor(color);
    }

    @Override
    public void setColor(int color, int alpha) {
        barDataSet.setColor(color, alpha);
    }

    @Override
    public void setColors(int[] colors, int alpha) {
        barDataSet.setColors(colors, alpha);
    }

    @Override
    public void resetColors() {
        barDataSet.resetColors();
    }

    @Override
    public void setLabel(String label) {
        barDataSet.setLabel(label);
    }

    @Override
    public String getLabel() {
        return barDataSet.getLabel();
    }

    @Override
    public void setHighlightEnabled(boolean enabled) {
        barDataSet.setHighlightEnabled(enabled);
    }

    @Override
    public boolean isHighlightEnabled() {
        return barDataSet.isHighlightEnabled();
    }

    @Override
    public void setValueFormatter(IValueFormatter f) {
        barDataSet.setValueFormatter(f);
    }

    @Override
    public IValueFormatter getValueFormatter() {
        return barDataSet.getValueFormatter();
    }

    @Override
    public boolean needsFormatter() {
        return barDataSet.needsFormatter();
    }

    @Override
    public void setValueTextColor(int color) {
        barDataSet.setValueTextColor(color);
    }

    @Override
    public void setValueTextColors(List<Integer> colors) {
        barDataSet.setValueTextColors(colors);
    }

    @Override
    public void setValueTypeface(Typeface tf) {
        barDataSet.setValueTypeface(tf);
    }

    @Override
    public void setValueTextSize(float size) {
        barDataSet.setValueTextSize(size);
    }

    @Override
    public int getValueTextColor() {
        return barDataSet.getValueTextColor();
    }

    @Override
    public int getValueTextColor(int index) {
        return barDataSet.getValueTextColor(index);
    }

    @Override
    public Typeface getValueTypeface() {
        return barDataSet.getValueTypeface();
    }

    @Override
    public float getValueTextSize() {
        return barDataSet.getValueTextSize();
    }

    @Override
    public void setForm(Legend.LegendForm form) {
        barDataSet.setForm(form);
    }

    @Override
    public Legend.LegendForm getForm() {
        return barDataSet.getForm();
    }

    @Override
    public void setFormSize(float formSize) {
        barDataSet.setFormSize(formSize);
    }

    @Override
    public float getFormSize() {
        return barDataSet.getFormSize();
    }

    @Override
    public void setFormLineWidth(float formLineWidth) {
        barDataSet.setFormLineWidth(formLineWidth);
    }

    @Override
    public float getFormLineWidth() {
        return barDataSet.getFormLineWidth();
    }

    @Override
    public void setFormLineDashEffect(DashPathEffect dashPathEffect) {
        barDataSet.setFormLineDashEffect(dashPathEffect);
    }

    @Override
    public DashPathEffect getFormLineDashEffect() {
        return barDataSet.getFormLineDashEffect();
    }

    @Override
    public void setDrawValues(boolean enabled) {
        barDataSet.setDrawValues(enabled);
    }

    @Override
    public boolean isDrawValuesEnabled() {
        return barDataSet.isDrawValuesEnabled();
    }

    @Override
    public void setDrawIcons(boolean enabled) {
        barDataSet.setDrawIcons(enabled);
    }

    @Override
    public boolean isDrawIconsEnabled() {
        return barDataSet.isDrawIconsEnabled();
    }

    @Override
    public void setIconsOffset(MPPointF offsetDp) {
        barDataSet.setIconsOffset(offsetDp);
    }

    @Override
    public MPPointF getIconsOffset() {
        return barDataSet.getIconsOffset();
    }

    @Override
    public void setVisible(boolean visible) {
        barDataSet.setVisible(visible);
    }

    @Override
    public boolean isVisible() {
        return barDataSet.isVisible();
    }

    @Override
    public YAxis.AxisDependency getAxisDependency() {
        return barDataSet.getAxisDependency();
    }

    @Override
    public void setAxisDependency(YAxis.AxisDependency dependency) {
        barDataSet.setAxisDependency(dependency);
    }

    @Override
    public int getIndexInEntries(int xIndex) {
        return barDataSet.getIndexInEntries(xIndex);
    }

    @Override
    public boolean removeFirst() {
        return barDataSet.removeFirst();
    }

    @Override
    public boolean removeLast() {
        return barDataSet.removeLast();
    }

    @Override
    public boolean removeEntryByXValue(float xValue) {
        return barDataSet.removeEntryByXValue(xValue);
    }

    @Override
    public boolean removeEntry(int index) {
        return barDataSet.removeEntry(index);
    }

    @Override
    public boolean contains(BarEntry e) {
        return barDataSet.contains(e);
    }

    @Override
    public int getEntryIndex(BarEntry e) {
        return barDataSet.getEntryIndex(e);
    }

}
