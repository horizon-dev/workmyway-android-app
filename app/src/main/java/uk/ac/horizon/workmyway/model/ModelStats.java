package uk.ac.horizon.workmyway.model;

import org.joda.time.Duration;
import org.joda.time.Interval;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;

public class ModelStats {

    private final Collections.ModelPeriods periods;

    public ModelStats(Model model) {
        this.periods = model.getPeriods();
    }

    public Duration getCombinedActive() {
        return getCombined(periods, Period.Type.Active);
    }

    public Duration getCombinedSedentary() {
        return getCombined(periods, Period.Type.Healthy);
    }

    public Duration getCombinedProlongedSedentary() {
        return getCombined(periods, Period.Type.Prolonged);
    }

    public int getBreakCount() {
        final Duration min = Duration.standardMinutes(30);
        final Duration max = Duration.standardMinutes(60);
        int count = 0;
        Period previous = null;
        for (SortedSet<? extends Period> ps : periods) {
            for (Period p : ps) {
                switch (p.getType()) {
                case Active:
                    if (previous != null) {
                        Duration d = previous.getInterval().toDuration();
                        if (d.isLongerThan(min) && d.isShorterThan(max)) {
                            count++;
                        }
                    }
                    previous = null;
                    break;
                case Healthy:
                    previous = p;
                    break;
                default:
                    previous = null;
                    break;
                }
            }
        }
        return count;
    }

    public Map<Period.Type, Duration> getSummary() {
        HashMap<Period.Type, Duration> map = new HashMap<>();
        map.put(Period.Type.Active, getCombinedActive());
        map.put(Period.Type.Healthy, getCombinedSedentary());
        map.put(Period.Type.Prolonged, getCombinedProlongedSedentary());
        return map;
    }

    private static Duration getCombined(Collections.ModelPeriods periods, Period.Type type) {
        List<Interval> intervals = new ArrayList<>();
        for (Collections.Periods ps : periods) {
            for (Period p : ps) {
                if (p.getType().equals(type)) {
                    intervals.add(p.getInterval());
                }
            }
        }
        long millis = 0L;
        for (Interval i : intervals) {
            millis += i.toDurationMillis();
        }
        return new Duration(millis);
    }

    public Duration getTrackingDuration() {
        long millis = 0;
        for (Collections.Periods ps : periods) {
            for (Period p : ps) {
                millis += p.getInterval().toDurationMillis();
            }
        }
        return new Duration(millis);
    }

}
