package uk.ac.horizon.workmyway.model;

import android.support.annotation.NonNull;

import org.joda.time.Interval;

import java.util.SortedSet;

public interface Period {

    enum Type {
        Active,
        Healthy,
        Prolonged
    }

    @SuppressWarnings("unused")
    @NonNull
    SortedSet<? extends Epoch> getEpochs();

    @NonNull
    Interval getInterval();

    @NonNull
    Type getType();

}
