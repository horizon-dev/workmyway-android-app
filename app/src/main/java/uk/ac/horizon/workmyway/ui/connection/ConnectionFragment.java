package uk.ac.horizon.workmyway.ui.connection;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.ui.common.ActivityResultActivity;
import uk.ac.horizon.workmyway.ui.developer.DeviceScanActivity;

public class ConnectionFragment extends Fragment implements ActivityResultActivity.ActivityResultListener {

    public static final int REQUEST_CODE_WRIST = 1;

    public static final int REQUEST_CODE_CUP = 2;

    public interface FragmentInteractionListener {
        void connected();
    }

    private FragmentInteractionListener listener = null;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof ActivityResultActivity) {
            ((ActivityResultActivity) activity).addActivityResultListener(this);
        }
        if (activity instanceof FragmentInteractionListener) {
            listener = (FragmentInteractionListener) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        if (resultCode != Activity.RESULT_OK) {
            super.onActivityResult(requestCode, resultCode, data);
            return;
        }
        if (requestCode == REQUEST_CODE_WRIST) {
            final String mac = data.getStringExtra(DeviceScanActivity.RESULT_FIELD_NAME);
            preferences.edit().putString(
                    getString(R.string.pk_wrist_device_mac), mac).apply();
        } else if (requestCode == REQUEST_CODE_CUP) {
            final String mac = data.getStringExtra(DeviceScanActivity.RESULT_FIELD_NAME);
            preferences.edit().putString(
                    getString(R.string.pk_cup_device_mac), mac).apply();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
        if (listener != null && ConnectionUtils.isConnected(getActivity())) {
            listener.connected();
        }
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment__ui_connection__connection, container, false);
        Button wrist = v.findViewById(R.id.connect_wrist_device);
        wrist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), DeviceScanActivity.class);
                getActivity().startActivityForResult(i, REQUEST_CODE_WRIST);
            }
        });
        Button cup = v.findViewById(R.id.connect_cup_device);
        cup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), DeviceScanActivity.class);
                getActivity().startActivityForResult(i, REQUEST_CODE_CUP);
            }
        });
        CheckBox data = v.findViewById(R.id.use_mobile_data);
        data.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                prefs.edit().putBoolean(getString(R.string.pk_use_mobile_data), b).apply();
            }
        });
        return v;
    }

}
