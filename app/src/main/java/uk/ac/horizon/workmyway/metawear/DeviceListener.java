package uk.ac.horizon.workmyway.metawear;

import com.mbientlab.metawear.module.AccelerometerBosch;
import com.mbientlab.metawear.module.Settings;

import org.joda.time.DateTime;

import uk.ac.horizon.workmyway.model.DeviceType;

public interface DeviceListener {

    void batteryState(DeviceType deviceType, Settings.BatteryState batteryState);

    void tapLive(DeviceType deviceType, DateTime timestamp, AccelerometerBosch.Tap tap);

    void stepLive(DeviceType deviceType, DateTime timestamp);

    void stepLogged(DeviceType deviceType, DateTime timestamp);

    void motionLive(DeviceType deviceType, DateTime timestamp);

    void motionLogged(DeviceType deviceType, DateTime timestamp);

    void heartbeat(DeviceType deviceType, DateTime timestamp);

    void connected(DeviceType deviceType, DateTime timestamp);

    void disconnected(DeviceType deviceType, DateTime timestamp, boolean expected);

}
