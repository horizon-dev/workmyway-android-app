package uk.ac.horizon.workmyway.model.base;

import android.support.annotation.NonNull;

import org.joda.time.Duration;
import org.joda.time.Interval;

class ConfigBuilder {

    private Interval day;

    private Duration prolongedPeriodDuration;

    private Duration epochDuration;

    private int X;

    private int Y;

    private int C0;

    private int C1;

    private int Z1;

    private int P;

    private int Q;

    private int epochsForInactive;

    ConfigBuilder setDay(@NonNull Interval day) {
        this.day = day;
        return this;
    }

    ConfigBuilder setProlongedPeriodDuration(@NonNull Duration prolongedPeriodDuration) {
        this.prolongedPeriodDuration = prolongedPeriodDuration;
        return this;
    }

    ConfigBuilder setEpochDuration(@NonNull Duration epochDuration) {
        this.epochDuration = epochDuration;
        return this;
    }

    ConfigBuilder setX(int X) {
        this.X = X;
        return this;
    }

    ConfigBuilder setY(int Y) {
        this.Y = Y;
        return this;
    }

    ConfigBuilder setC0(int C0) {
        this.C0 = C0;
        return this;
    }

    ConfigBuilder setC1(int C1) {
        this.C1 = C1;
        return this;
    }

    ConfigBuilder setZ1(int Z1) {
        this.Z1 = Z1;
        return this;
    }

    ConfigBuilder setP(int P) {
        this.P = P;
        return this;
    }

    ConfigBuilder setQ(int Q) {
        this.Q = Q;
        return this;
    }

    ConfigBuilder setEpochsForInactive(int epochsForInactive) {
        this.epochsForInactive = epochsForInactive;
        return this;
    }

    Config build() {
        final Interval _day = day;
        final Duration _prolongedPeriodDuration = prolongedPeriodDuration;
        final Duration _epochDuration = epochDuration;
        final int _X = X;
        final int _Y = Y;
        final int _C0 = C0;
        final int _C1 = C1;
        final int _Z1 = Z1;
        final int _P = P;
        final int _Q = Q;
        final int _epochsForInactive = epochsForInactive;
        return new Config() {

            @Override
            @NonNull
            public Interval getDay() {
                return _day;
            }

            @Override
            @NonNull
            public Duration getProlongedPeriodDuration() {
                return _prolongedPeriodDuration;
            }

            @Override
            @NonNull
            public Duration getEpochDuration() {
                return _epochDuration;
            }

            @Override
            public int getX() {
                return _X;
            }

            @Override
            public int getY() {
                return _Y;
            }

            @Override
            public int getC0() {
                return _C0;
            }

            @Override
            public int getC1() {
                return _C1;
            }

            @Override
            public int getZ1() {
                return _Z1;
            }

            @Override
            public int getP() {
                return _P;
            }

            @Override
            public int getQ() {
                return _Q;
            }

            @Override
            public int getEpochsForInactive() {
                return _epochsForInactive;
            }

        };
    }

}
