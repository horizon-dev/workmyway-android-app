package uk.ac.horizon.workmyway.metawear.devicelisteners;

import android.util.Log;

import com.mbientlab.metawear.module.AccelerometerBosch;

import org.joda.time.DateTime;

import uk.ac.horizon.workmyway.db.ASyncDatabase;
import uk.ac.horizon.workmyway.metawear.DeviceListenerAdapter;
import uk.ac.horizon.workmyway.model.DeviceType;

public class Database extends DeviceListenerAdapter {

    private final ASyncDatabase database;

    public Database(ASyncDatabase database) {
        this.database = database;
    }

    @Override
    public void tapLive(DeviceType deviceType, DateTime timestamp, AccelerometerBosch.Tap tap) {
        database.insertTapReading(timestamp, tap, deviceType);
    }

    @Override
    public void stepLive(DeviceType deviceType, DateTime timestamp) {
        database.insertStepReading(timestamp, deviceType);
    }

    @Override
    public void stepLogged(DeviceType deviceType, DateTime timestamp) {
        database.insertStepReading(timestamp, deviceType);
    }

    @Override
    public void motionLive(DeviceType deviceType, DateTime timestamp) {
        database.insertMotionReading(timestamp, deviceType);
    }

    @Override
    public void motionLogged(DeviceType deviceType, DateTime timestamp) {
        database.insertMotionReading(timestamp, deviceType);
    }

    @Override
    public void heartbeat(DeviceType deviceType, DateTime timestamp) {
        database.insertHeartbeat(timestamp, deviceType);
    }

    @Override
    public void connected(DeviceType deviceType, DateTime timestamp) {
        database.insertConnectionStatus(timestamp, true, true, deviceType);
    }

    @Override
    public void disconnected(DeviceType deviceType, DateTime timestamp, boolean expected) {
        database.insertConnectionStatus(timestamp, false, expected, deviceType);
    }

}
