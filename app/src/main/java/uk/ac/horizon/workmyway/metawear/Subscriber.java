package uk.ac.horizon.workmyway.metawear;

import com.mbientlab.metawear.Data;

import org.joda.time.DateTime;

import java.util.Set;

import uk.ac.horizon.workmyway.model.DeviceType;
import uk.ac.horizon.workmyway.util.Log;

abstract class Subscriber<T> implements com.mbientlab.metawear.Subscriber {

    private static final String TAG = Subscriber.class.getSimpleName();

    @Override
    public final void apply(Data data, Object... env) {

        DateTime dt = new DateTime(data.timestamp());
        if(dt.isAfterNow()) {
            try {
                throw new Exception(dt.toString() + " is in the future");
            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
            }
            return;
        }
        if (env == null || env.length == 0) {
            Log.e(TAG, "Environment is empty <" + this.toString() + ">");
            return;
        }
        if (env.length < 2) {
            Log.e(TAG, "There are not enough arguments to the environment");
            return;
        }
        if (!(env[0] instanceof DeviceType)) {
            Log.e(TAG, "First environment variable is not an instance of DeviceType");
            return;
        }
        if (!(env[1] instanceof Set)) {
            Log.e(TAG, "Second environment variable is not an instance of java.util.Set");
            return;
        }
        DeviceType deviceType = (DeviceType) env[0];
        T value = getValue(data);
        android.util.Log.v(TAG, "data(" + String.valueOf(this) + ", "
                + deviceType.name() + ", " + String.valueOf(value) + ")");
        if (((Set) env[1]).size() < 1) {
            Log.e(TAG, "There are no DeviceListeners set");
        }
        for (Object o : (Set) env[1]) {
            if (o instanceof DeviceListener) {
                try {
                    onData((DeviceListener) o, deviceType, data, value);
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage(), e);
                }
            } else {
                Log.e(TAG, o + " is not an instance of DeviceListener");
            }
        }
    }

    abstract void onData(DeviceListener listener, DeviceType deviceType, Data data, T value);

    abstract T getValue(Data data);

}