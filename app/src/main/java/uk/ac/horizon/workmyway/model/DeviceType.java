package uk.ac.horizon.workmyway.model;

public enum DeviceType {

    Wrist,
    Cup

}
