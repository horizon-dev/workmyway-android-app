package uk.ac.horizon.workmyway.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Arrays;

public class Helper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 5;

    private static final String DATABASE_NAME = "smartcup.db";

    public Helper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Contract.MotionReadings.SQL_CREATE_TABLE);
        db.execSQL(Contract.StepReadings.SQL_CREATE_TABLE);
        db.execSQL(Contract.TapReadings.SQL_CREATE_TABLE);
        db.execSQL(Contract.ConnectionStatuses.SQL_CREATE_TABLE);
        db.execSQL(Contract.TrackingStatuses.SQL_CREATE_TABLE);
        db.execSQL(Contract.Logs.SQL_CREATE_TABLE);
        db.execSQL(Contract.SelfReports.SQL_CREATE_TABLE);
        db.execSQL(Contract.Alerts.SQL_CREATE_TABLE);
        db.execSQL(Contract.ConfigurationChanges.SQL_CREATE_TABLE);
        db.execSQL(Contract.Heartbeats.SQL_CREATE_TABLE);
        db.execSQL(Contract.DayComparisonReward.SQL_CREATE_TABLE);
        db.execSQL(Contract.GoalReward.SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        ArrayList<String> commands = new ArrayList<>();
        if (oldVersion <= 1 && newVersion >= 2) {
            commands.addAll(Arrays.asList(Contract.Heartbeats.SQL_UPGRADE_1_TO_2));
        }
        if (oldVersion <= 2 && newVersion >= 3) {
            commands.addAll(Arrays.asList(Contract.DayComparisonReward.SQL_UPGRADE_2_TO_3));
        }
        if (oldVersion <= 3 && newVersion >= 4) {
            commands.addAll(Arrays.asList(Contract.GoalReward.SQL_UPGRADE_3_TO_4));
        }
        if (oldVersion <= 4 && newVersion >= 5) {
            commands.addAll(Arrays.asList(Contract.GoalReward.SQL_UPGRADE_4_TO_5));
        }
        for (String command : commands) {
            db.execSQL(command);
        }
    }

}
