package uk.ac.horizon.workmyway.ui.common;

import uk.ac.horizon.workmyway.metawear.DeviceConnectionService;

public interface DeviceServiceExecutor {

    interface Task {
        void doTask(DeviceConnectionService.LocalBinder binder);
    }

    void execute(Task task);

}
