package uk.ac.horizon.workmyway.model;

import android.support.annotation.NonNull;

import org.joda.time.Interval;

public interface Epoch {

    @SuppressWarnings("unused")
    @NonNull
    Interval getInterval();

    @SuppressWarnings("unused")
    long getCount();

}
