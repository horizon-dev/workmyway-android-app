package uk.ac.horizon.workmyway.metawear;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.support.annotation.Nullable;

import com.mbientlab.metawear.Data;
import com.mbientlab.metawear.MetaWearBoard;
import com.mbientlab.metawear.Route;
import com.mbientlab.metawear.android.BtleService;
import com.mbientlab.metawear.module.AccelerometerBosch;
import com.mbientlab.metawear.module.Settings;

import org.joda.time.DateTime;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

import bolts.Task;
import uk.ac.horizon.workmyway.model.DeviceType;
import uk.ac.horizon.workmyway.util.Log;
import uk.ac.horizon.workmyway.util.RetryPeriod;

abstract class DeviceConnection {

    private static final String TAG = DeviceConnection.class.getSimpleName();

    private static final long CONNECTION_INITIAL_RETRY_TIME_MS = 1000;

    private static final long CONNECTION_MAXIMUM_RETRY_TIME_MS = 10000;

    private static final long TASK_REGISTRATION_DELAY = 750;

    @FunctionalInterface
    interface Success<T> {
        void onSuccess(T t) throws Exception;
    }

    @FunctionalInterface
    interface Return<T> {
        @Nullable
        Task<T> onReturn() throws Exception;
    }

    class Continuation<T, Y> implements bolts.Continuation<T, Task<Y>> {

        private final Success<T> success;

        private final Return<Y> _return;

        Continuation(Success<T> success, Return<Y> _return) {
            this.success = success;
            this._return = _return;
        }

        @Override
        public Task<Y> then(Task<T> task) throws Exception {
            if (task.isFaulted()) {
                try {
                    throw new RuntimeException(task.getError());
                } catch (Exception e) {
                    Log.e(TAG, task.getError().getMessage(), task.getError());
                }
            } else if (task.isCancelled()) {
                Log.i(TAG, "Task cancelled <" + String.valueOf(task) + ">");
            } else {
                success.onSuccess(task.getResult());
            }
            try {
                Thread.sleep(TASK_REGISTRATION_DELAY);
            } catch (InterruptedException e) {
                Log.e(TAG, e.getMessage(), e);
            }
            return _return.onReturn();
        }

    }

    class RouteContinuation<T> extends Continuation<Route, T> {
        RouteContinuation(final Success<Route> success, final Return<T> _return) {
            //noinspection Convert2Lambda
            super(
                    new Success<Route>() {
                        @Override
                        public void onSuccess(Route route) throws Exception {
                            route.setEnvironment(0, deviceType, deviceListeners);
                            success.onSuccess(route);
                        }
                    },
                    _return);
        }
    }

    static final Subscriber batterySubscriber = new Subscriber<Settings.BatteryState>() {

        @Override
        void onData(
                DeviceListener listener,
                DeviceType deviceType,
                Data data,
                Settings.BatteryState state) {
            listener.batteryState(deviceType, state);
        }

        @Override
        Settings.BatteryState getValue(Data data) {
            return data.value(Settings.BatteryState.class);
        }

        @Override
        public String toString() {
            return "batterySubscriber";
        }

    };

    static final Subscriber tapSubscriber = new Subscriber<AccelerometerBosch.Tap>() {

        @Override
        void onData(
                DeviceListener listener,
                DeviceType deviceType,
                Data data,
                AccelerometerBosch.Tap tap) {
            listener.tapLive(deviceType, new DateTime(data.timestamp()), tap);
        }

        @Override
        AccelerometerBosch.Tap getValue(Data data) {
            return data.value(AccelerometerBosch.Tap.class);
        }

        @Override
        public String toString() {
            return "tapSubscriber";
        }

    };

    static final Subscriber motionSubscriber = new Subscriber<Void>() {

        @Override
        public void onData(DeviceListener listener, DeviceType deviceType, Data data, Void v) {
            listener.motionLive(deviceType, new DateTime(data.timestamp()));
        }

        public Void getValue(Data data) {
            return null;
        }

        @Override
        public String toString() {
            return "motionSubscriber";
        }

    };

    static final Subscriber motionLogSubscriber = new Subscriber<Void>() {

        @Override
        public void onData(DeviceListener listener, DeviceType deviceType, Data data, Void v) {
            listener.motionLogged(deviceType, new DateTime(data.timestamp()));
        }

        public Void getValue(Data data) {
            return null;
        }

        @Override
        public String toString() {
            return "motionLogSubscriber";
        }

    };

    static final Subscriber stepCountSubscriber = new Subscriber<Void>() {

        @Override
        public void onData(DeviceListener listener, DeviceType deviceType, Data data, Void v) {
            listener.stepLive(deviceType, new DateTime(data.timestamp()));
        }

        @Override
        public Void getValue(Data data) {
            return null;
        }

        @Override
        public String toString() {
            return "stepCountSubscriber";
        }

    };

    static final Subscriber stepCountLogSubscriber = new Subscriber<Void>() {

        @Override
        public void onData(DeviceListener listener, DeviceType deviceType, Data data, Void v) {
            listener.stepLogged(deviceType, new DateTime(data.timestamp()));
        }

        @Override
        public Void getValue(Data data) {
            //android.util.Log.i(TAG, "STEP COUNT LOG RESULT!");
            return null;
        }

        @Override
        public String toString() {
            return "stepCountLogSubscriber";
        }

    };

    static final Subscriber heartbeatSubscriber = new Subscriber<Void>() {

        @Override
        void onData(DeviceListener listener, DeviceType deviceType, Data data, Void v) {
            listener.heartbeat(deviceType, new DateTime(data.timestamp()));
        }

        @Override
        public Void getValue(Data data) {
            return null;
        }

        @Override
        public String toString() {
            return "heartbeatSubscriber";
        }

    };

    interface DeviceReady {
        void onReady();
    }

    final Context context;

    MetaWearBoard board;

    final Set<DeviceListener> deviceListeners;

    final DeviceType deviceType;

    private final String mac;

    private final ScheduledExecutorService taskExecutor
            = Executors.newSingleThreadScheduledExecutor();

    private final RetryPeriod retryPeriod = new RetryPeriod(
            CONNECTION_INITIAL_RETRY_TIME_MS,
            CONNECTION_MAXIMUM_RETRY_TIME_MS);

    private final AtomicBoolean deviceReady = new AtomicBoolean(false);

    private final AtomicBoolean isConnecting = new AtomicBoolean(false);

    private final AtomicBoolean disconnectionRequested = new AtomicBoolean(false);

    @SuppressWarnings("FieldCanBeLocal")
    private final bolts.Continuation<Void, Void> onConnected =
            new bolts.Continuation<Void, Void>() {

                @Override
                public Void then(Task<Void> task) throws Exception {
                    isConnecting.set(false);
                    if (task.isCancelled()) {
                        Log.i(TAG, "Connection cancelled <" + deviceType
                                + "> <" + mac + "> < " + DeviceConnection.this + ">");
                    } else if (task.isFaulted()) {
                        Log.w(TAG, "Unable to connect <" + deviceType + "> <" + mac
                                + "> < " + DeviceConnection.this + ">", task.getError());
                        connect(retryPeriod.next());
                    } else {
                        Log.i(TAG, "Connected <" + deviceType + "> <"
                                + mac + "> < " + DeviceConnection.this + ">");
                        retryPeriod.reset();
                        onConnection(board, () -> {
                            synchronized (deviceReady) {
                                deviceReady.set(true);
                                deviceReady.notifyAll();
                            }
                        });
                        synchronized (deviceListeners) {
                            DateTime now = DateTime.now();
                            for (DeviceListener l : deviceListeners) {
                                l.connected(deviceType, now);
                            }
                        }
                    }
                    return null;
                }
            };

    DeviceConnection(
            final Context context,
            final BtleService.LocalBinder service,
            final String mac,
            final DeviceType deviceType,
            final Set<DeviceListener> listeners) {
        Log.i(TAG, "Creating new device connection <" + deviceType
                + "> <" + mac + "> < " + DeviceConnection.this + ">");
        this.context = context;
        this.deviceListeners = Collections.synchronizedSet(new HashSet<>(listeners));
        this.deviceType = deviceType;
        this.mac = mac;
        BluetoothManager btManager = (BluetoothManager)
                context.getSystemService(Context.BLUETOOTH_SERVICE);
        if (btManager == null) {
            throw new RuntimeException("No Bluetooth adapter available");
        } else {
            BluetoothDevice device = btManager.getAdapter().getRemoteDevice(mac);
            board = service.getMetaWearBoard(device);
            board.onUnexpectedDisconnect(status -> {
                Log.i(TAG, "Unexpected Disconnect (" + status + ") <"
                        + deviceType + "> <" + mac + "> < " + DeviceConnection.this + ">");
                synchronized (deviceListeners) {
                    DateTime now = DateTime.now();
                    for (DeviceListener l : deviceListeners) {
                        l.disconnected(deviceType, now, false);
                    }
                }
                connect(retryPeriod.next());
            });
            connect(0);
        }
    }

    private void connect(long delay) {
        if (disconnectionRequested.get()) {
            Log.i(TAG, "Not calling connect(), disconnection requested <"
                    + deviceType + "> <" + mac + "> < " + DeviceConnection.this + ">");
            return;
        }
        synchronized (isConnecting) {
            if (isConnecting.get()) {
                Log.w(TAG, "connect() already called <"
                        + deviceType + "> <" + mac + "> < " + DeviceConnection.this + ">");
                return;
            }
            isConnecting.set(true);
            Log.i(TAG, "connect() <" + deviceType + "> <"
                    + mac + "> < " + DeviceConnection.this + ">");
            /*scanForDevice().continueWithTask(
                    task -> board.connectAsync(delay)).continueWith(onConnected);*/
            board.connectAsync(delay).continueWith(onConnected);
        }
    }

    /*private Task<Void> scanForDevice() {
        final TaskCompletionSource<Void> tcs = new TaskCompletionSource<>();
        final BluetoothManager btManager = (BluetoothManager)
                context.getSystemService(Context.BLUETOOTH_SERVICE);
        if (btManager == null) {
            tcs.setResult(null);
            throw new RuntimeException("No Bluetooth adapter available");
        } else {
            ScanFilter filter = new ScanFilter.Builder().setDeviceAddress(mac).build();
            ArrayList<ScanFilter> filters = new ArrayList<>();
            filters.add(filter);
            ScanSettings settings = new ScanSettings.Builder().setScanMode(
                    ScanSettings.SCAN_MODE_LOW_LATENCY).build();
            BluetoothLeScanner scanner = btManager.getAdapter().getBluetoothLeScanner();
            Log.i(TAG, "---SCANNING--- <" + deviceType + "> <" + mac
                    + "> < " + DeviceConnection.this + ">");
            final Object scanLock = new Object();
            scanner.startScan(filters, settings, new ScanCallback() {
                @Override
                public void onScanResult(int callbackType, ScanResult result) {
                    synchronized (scanLock) {
                        Log.i(TAG, "---FOUND--- <" + deviceType + "> <" + mac + "> < "
                                + DeviceConnection.this + ">");
                        scanner.stopScan(this);
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            android.util.Log.w(TAG, e.getMessage(), e);
                        }
                        tcs.trySetResult(null);
                    }
                }
            });
        }

        return tcs.getTask();
    }*/

    void addDeviceListener(DeviceListener deviceListener) {
        synchronized (deviceListeners) {
            deviceListeners.add(deviceListener);
        }
        if (isConnected()) {
            deviceListener.connected(deviceType, DateTime.now());
        } else {
            deviceListener.disconnected(deviceType, DateTime.now(), false);
        }
    }

    void removeDeviceListener(DeviceListener deviceListener) {
        synchronized (deviceListeners) {
            deviceListeners.remove(deviceListener);
        }
    }

    final void disconnect(boolean reset) {
        if (reset) {
            reset();
        }
        if (!disconnectionRequested.get() && board.isConnected()) {
            disconnectionRequested.set(true);
            board.disconnectAsync().continueWith(v -> {
                synchronized (deviceListeners) {
                    DateTime now = DateTime.now();
                    for (DeviceListener l : deviceListeners) {
                        l.disconnected(deviceType, now, true);
                    }
                }
                return Task.forResult(null);
            });
        } else {
            disconnectionRequested.set(true);
        }
    }

    final void submitTask(final DeviceTask task) {
        taskExecutor.submit(() -> {
            if (!waitOnConnection(0L)) return;
            try {
                task.doTask(board);
            } catch (final Throwable e) {
                Log.e(TAG, "Error executing device task", e);
            }
        });
    }

    boolean waitOnConnection(long timeout) {
        synchronized (deviceReady) {
            while (board != null
                    && !board.isConnected()
                    && !deviceReady.get()
                    && !disconnectionRequested.get()) {
                try {
                    Log.d(TAG, "Waiting for connection");
                    deviceReady.wait(timeout);
                } catch (InterruptedException e) {
                    Log.w(TAG, e.getMessage(), e);
                }
            }
        }
        return !disconnectionRequested.get();
    }

    String getDeviceKey(int key) {
        return String.format("%s_%s", mac, context.getString(key));
    }

    abstract void onConnection(MetaWearBoard board, DeviceReady deviceReady);

    abstract void reset();

    private boolean isConnected() {
        try {
            return board != null && board.isConnected();
        } catch (Exception e) {
            Log.w(TAG, e.getMessage(), e);
            return false;
        }
    }

}
