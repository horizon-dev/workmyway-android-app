package uk.ac.horizon.workmyway.ui.main.history;

import android.text.Html;
import android.view.View;
import android.widget.TextView;

import org.joda.time.Period;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.model.ModelStats;

public class TrophyFragment extends SummaryFragment {

    private int breaks = 0;

    private int combined = 0;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment__ui_main_history__trophy;
    }

    @Override
    protected void setup(View view, ModelStats modelStats) {
        TextView sitting = view.findViewById(R.id.textbox_sitting);

        if (combined < 0) {
            String sittingFormat = getResources().getString(R.string.trophy_prolonged_sitting);
            Period p = new Period(Math.abs(combined));
            sitting.setText(Html.fromHtml(
                    parseColors(String.format(sittingFormat, "minus", p.getHours(),
                            p.getMinutes()))));
        } else {
            sitting.setText("");
        }

        TextView breaks = view.findViewById(R.id.textbox_breaks);
        if (this.breaks > 0) {
            String breaksFormat = getResources().getString(R.string.trophy_breaks);
            breaks.setText(Html.fromHtml(parseColors(String.format(breaksFormat, this.breaks))));
        } else {
            breaks.setText("");
        }

    }

    public void setBreaks(int breaks) {
        this.breaks = breaks;
    }

    public void setCombined(int combined) {
        this.combined = combined;
    }

}
