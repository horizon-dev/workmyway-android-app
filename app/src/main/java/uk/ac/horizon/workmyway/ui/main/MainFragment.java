package uk.ac.horizon.workmyway.ui.main;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.ui.main.history.HistoryFragment;
import uk.ac.horizon.workmyway.ui.main.rewards.RewardsFragment;
import uk.ac.horizon.workmyway.ui.main.track.TrackFragment;

public class MainFragment extends Fragment {

    private static final String TAB_HISTORY = "history";

    private static final String TAB_TRACK = "track";

    private static final String TAB_REWARDS = "rewards";

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment__ui_main__main, container, false);
        TabHost th = (TabHost) v.findViewById(android.R.id.tabhost);
        Resources res = getResources();
        th.setup();
        th.addTab(th.newTabSpec(TAB_HISTORY).setIndicator(res.getString(R.string.tab_title_history))
                .setContent(android.R.id.tabcontent));
        th.addTab(th.newTabSpec(TAB_TRACK).setIndicator(res.getString(R.string.tab_title_track))
                .setContent(android.R.id.tabcontent));
        th.addTab(th.newTabSpec(TAB_REWARDS).setIndicator(res.getString(R.string.tab_title_rewards))
                .setContent(android.R.id.tabcontent));
        th.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                if (TAB_HISTORY.equals(tabId)) {
                    ft.replace(R.id.tabcontent, new HistoryFragment());
                } else if (TAB_TRACK.equals(tabId)) {
                    ft.replace(R.id.tabcontent, new TrackFragment());
                } else if (TAB_REWARDS.equals(tabId)) {
                    ft.replace(R.id.tabcontent, new RewardsFragment());
                }
                ft.commit();
            }
        });
        th.setCurrentTabByTag(TAB_TRACK);
        return v;
    }

}
