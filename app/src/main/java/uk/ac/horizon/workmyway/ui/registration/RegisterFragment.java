package uk.ac.horizon.workmyway.ui.registration;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import uk.ac.horizon.workmyway.R;

public class RegisterFragment
        extends Fragment
        implements
        RegistrationFragment.OnFragmentInteractionListener,
        SignInFragment.OnFragmentInteractionListener {

    public interface OnFragmentInteractionListener {
        void signedIn();
    }

    private OnFragmentInteractionListener listener;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment__ui_registration__register, container, false);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        RegistrationFragment rf = new RegistrationFragment();
        rf.setOnFragmentInteractionListener(this);
        ft.replace(R.id.register_content, rf);
        ft.commit();
        return v;
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(
                    context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void switchToSignIn() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        SignInFragment sf = new SignInFragment();
        sf.setOnFragmentInteractionListener(this);
        ft.replace(R.id.register_content, sf);
        ft.commit();
    }

    @Override
    public void switchToRegistration() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        RegistrationFragment rf = new RegistrationFragment();
        rf.setOnFragmentInteractionListener(this);
        ft.replace(R.id.register_content, rf);
        ft.commit();
    }

    @Override
    public void register(final String username, final String password) {
        final ProgressDialog progress = new ProgressDialog(getActivity());
        progress.setMessage(getResources().getString(R.string.please_wait));
        progress.setCancelable(false);
        progress.show();
        (new AsyncTask<Void, Void, Exception>() {

            @Override
            protected Exception doInBackground(Void... params) {
                try {
                    RegistrationUtils.register(username, password, getActivity());
                } catch (Exception e) {
                    return e;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Exception e) {
                progress.dismiss();
                if (e == null) {
                    SharedPreferences p =
                            PreferenceManager.getDefaultSharedPreferences(getActivity());
                    p.edit()
                            .putString(getString(R.string.pk_registration_username), username)
                            .putString(getString(R.string.pk_registration_password), password)
                            .apply();
                    listener.signedIn();
                } else {
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    RegistrationFragment rf = new RegistrationFragment();
                    rf.setOnFragmentInteractionListener(RegisterFragment.this);
                    rf.setErrorText(getResources().getString(R.string.register_error));
                    ft.replace(R.id.register_content, rf);
                    ft.commit();
                }
            }

        }).execute();
    }

    @Override
    public void signIn(final String username, final String password) {
        final ProgressDialog progress = new ProgressDialog(getActivity());
        progress.setMessage("Please wait");
        progress.setCancelable(false);
        progress.show();
        (new AsyncTask<Void, Void, Exception>() {

            @Override
            protected Exception doInBackground(Void... params) {
                try {
                    RegistrationUtils.signIn(username, password, getActivity());
                } catch (Exception e) {
                    return e;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Exception e) {
                progress.dismiss();
                if (e == null) {
                    SharedPreferences p =
                            PreferenceManager.getDefaultSharedPreferences(getActivity());
                    p.edit()
                            .putString(getString(R.string.pk_registration_username), username)
                            .putString(getString(R.string.pk_registration_password), password)
                            .apply();
                    listener.signedIn();
                } else {
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    SignInFragment sf = new SignInFragment();
                    sf.setOnFragmentInteractionListener(RegisterFragment.this);
                    sf.setErrorText(getResources().getString(R.string.signin_error));
                    ft.replace(R.id.register_content, sf);
                    ft.commit();
                }
            }

        }).execute();
    }


}
