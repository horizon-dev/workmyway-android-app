package uk.ac.horizon.workmyway.ui.settings;

import android.app.ActionBar;
import android.os.Bundle;
import android.view.MenuItem;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.ui.common.DeviceServiceExecutorActivity;

public class SettingsActivity extends DeviceServiceExecutorActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__ui_settings__settings);
        ActionBar bar = getActionBar();
        if (bar != null) {
            bar.setSubtitle(R.string.activity__ui_settings__settings__subtitle);
            bar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
