package uk.ac.horizon.workmyway.ui.main.track;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.WorkMyWay;
import uk.ac.horizon.workmyway.model.MutableModel;
import uk.ac.horizon.workmyway.ui.main.track.chart.ChartFragment;
import uk.ac.horizon.workmyway.ui.main.track.home.HomeFragment;
import uk.ac.horizon.workmyway.util.Log;

public class TrackFragment extends Fragment {

    private static final String TAG = TrackFragment.class.getSimpleName();

    private static class MyTask extends AsyncTask<Void, Void, MutableModel>  {
        @Override
        protected MutableModel doInBackground(Void... voids) {
            return WorkMyWay.getModelManager().getModelForToday();
        }
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void onResume() {
        super.onResume();
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        final TrackFragment self = this;
        new MyTask() {
            @Override
            protected void onPostExecute(MutableModel mutableModel) {
                try {
                    super.onPostExecute(mutableModel);
                    if (mutableModel.isCurrentlyTracking()) {
                        ChartFragment c = new ChartFragment();
                        c.setTrackFragment(self);
                        ft.replace(R.id.track_content_frame, c);
                    } else {
                        HomeFragment h = new HomeFragment();
                        h.setTrackFragment(self);
                        ft.replace(R.id.track_content_frame, h);
                    }
                    ft.commit();
                } catch (Exception e) {
                    Log.w(TAG, e.getMessage(), e);
                }
            }
        }.execute((Void)null);
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment__ui_main_track__track, container, false);
    }

    public void trackingStarted() {
        getActivity().runOnUiThread(() -> {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ChartFragment c = new ChartFragment();
            c.setTrackFragment(TrackFragment.this);
            ft.replace(R.id.track_content_frame, c);
            ft.commit();
        });
    }

    public void trackingStopped() {
        getActivity().runOnUiThread(() -> {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            HomeFragment h = new HomeFragment();
            h.setTrackFragment(TrackFragment.this);
            ft.replace(R.id.track_content_frame, h);
            ft.commit();
        });
    }

}
