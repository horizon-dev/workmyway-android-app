package uk.ac.horizon.workmyway.ui.common;


import android.content.Intent;

public interface ActivityResultActivity {

    interface ActivityResultListener {
        void onActivityResult(int requestCode, int resultCode, Intent data);
    }

    void addActivityResultListener(ActivityResultListener listener);

    void removeActivityResultListener(ActivityResultListener listener);

}
