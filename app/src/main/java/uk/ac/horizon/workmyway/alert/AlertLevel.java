package uk.ac.horizon.workmyway.alert;

public enum AlertLevel {

    None,
    BreakApproaching,
    BreakDue,
    BreakOverdue
    
}
