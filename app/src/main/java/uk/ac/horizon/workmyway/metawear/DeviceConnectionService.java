package uk.ac.horizon.workmyway.metawear;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.preference.PreferenceManager;

import com.mbientlab.metawear.android.BtleService;
import com.mbientlab.metawear.module.AccelerometerBosch;
import com.mbientlab.metawear.module.Settings;

import org.joda.time.DateTime;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.WorkMyWay;
import uk.ac.horizon.workmyway.alert.AlertService;
import uk.ac.horizon.workmyway.metawear.devicelisteners.Database;
import uk.ac.horizon.workmyway.metawear.devicelisteners.Live;
import uk.ac.horizon.workmyway.metawear.devicelisteners.NoData;
import uk.ac.horizon.workmyway.model.DeviceType;
import uk.ac.horizon.workmyway.util.Log;

public class DeviceConnectionService
        extends Service
        implements ServiceConnection {

    private static final String TAG = DeviceConnectionService.class.getSimpleName();

    private class BatteryListener extends DeviceListenerAdapter {
        @Override
        public void batteryState(DeviceType deviceType, Settings.BatteryState batteryState) {
            if (deviceType.equals(DeviceType.Cup)) {
                executorService.schedule(() -> {
                    if (cupDevice != null) {
                        android.util.Log.v(TAG, "Requesting cup battery update");
                        cupDevice.submitTask(CommonTasks.readBatteryState());
                    }
                }, 1, TimeUnit.MINUTES);
            } else if (deviceType.equals(DeviceType.Wrist)) {
                executorService.schedule(() -> {
                    if (wristDevice != null) {
                        android.util.Log.v(TAG, "Requesting wrist battery update");
                        wristDevice.submitTask(CommonTasks.readBatteryState());
                    }
                }, 1, TimeUnit.MINUTES);
            }
        }
    }

    public class LocalBinder extends Binder {

        public void addDeviceListener(DeviceListener listener) {
            synchronized (deviceListeners) {
                deviceListeners.add(listener);
            }
            synchronized (DeviceConnectionService.this) {
                if (wristDevice != null) {
                    wristDevice.addDeviceListener(listener);
                }
                if (cupDevice != null) {
                    cupDevice.addDeviceListener(listener);
                }
            }
        }

        public void removeDeviceListener(DeviceListener listener) {
            synchronized (deviceListeners) {
                deviceListeners.remove(listener);
            }
            synchronized (DeviceConnectionService.this) {
                if (wristDevice != null) {
                    wristDevice.removeDeviceListener(listener);
                }
                if (cupDevice != null) {
                    cupDevice.removeDeviceListener(listener);
                }
            }
        }

        public void resetAndDisconnect() {
            synchronized (DeviceConnectionService.this) {
                if (wristDevice != null) {
                    //preferences.edit().remove(getString(R.string.pk_wrist_device_mac)).apply();
                    wristDevice.disconnect(true);
                    wristDevice = null;
                }
                if (cupDevice != null) {
                    //preferences.edit().remove(getString(R.string.pk_cup_device_mac)).apply();
                    cupDevice.disconnect(true);
                    cupDevice = null;
                }
                BluetoothManager bm = (BluetoothManager)
                        DeviceConnectionService.this.getSystemService(Context.BLUETOOTH_SERVICE);
                if (bm != null) {
                    BluetoothAdapter adapter = bm.getAdapter();
                    if (adapter.isEnabled()) {
                        adapter.disable();
                    }
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        android.util.Log.w(TAG, e.getMessage(), e);
                    }
                    if (!adapter.isEnabled()) {
                        adapter.enable();
                    }
                }
                setupCupDevice();
                setupWristDevice();
                //DeviceConnectionService.this.stopSelf();
            }
        }

    }

    private final LocalBinder localBinder = new LocalBinder();

    private final SharedPreferences.OnSharedPreferenceChangeListener preferenceChangeListener =
            (sharedPreferences, key) -> {
                if (getString(R.string.pk_wrist_device_mac).equals(key)) {
                    setupWristDevice();
                } else if (getString(R.string.pk_cup_device_mac).equals(key)) {
                    setupCupDevice();
                }
            };

    private final Set<DeviceListener> deviceListeners =
            Collections.synchronizedSet(new HashSet<DeviceListener>());

    private final ScheduledExecutorService executorService
            = Executors.newSingleThreadScheduledExecutor();

    private boolean isRunning = false;

    private SharedPreferences preferences;

    private ComponentName mwServiceComponentName;

    private BtleService.LocalBinder mwService;

    private ComponentName alertServiceComponentName;

    private AlertService.LocalBinder alertService;

    private WristDeviceConnection wristDevice;

    private CupDeviceConnection cupDevice;

    @Override
    public void onCreate() {
        super.onCreate();
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        deviceListeners.add(new Database(WorkMyWay.getDatabase()));
        deviceListeners.add(new Live(this, WorkMyWay.getModelManager()));
        deviceListeners.add(new NoData(this));
        deviceListeners.add(new BatteryListener());
        deviceListeners.add(new DeviceListenerAdapter() {
            @Override
            public void tapLive
                    (DeviceType deviceType, DateTime timestamp, AccelerometerBosch.Tap tap) {
                Log.i(TAG, "Tap");
                if (deviceType == DeviceType.Cup && alertService != null) {
                    if (preferences.getBoolean(
                            getString(R.string.pk_alert_tap_to_snooze),
                            getResources().getBoolean(R.bool.alert_tap_to_snooze_default))) {
                        alertService.pause();
                    }
                }
            }
        });
        // At this point, both devices are disconnected so we notify all device listeners
        synchronized (deviceListeners) {
            DateTime now = DateTime.now();
            for (DeviceListener dl : deviceListeners) {
                dl.disconnected(DeviceType.Cup, now, true);
                dl.disconnected(DeviceType.Wrist, now, true);
            }
        }
    }

    @Override
    public synchronized int onStartCommand(Intent intent, int flags, int startId) {
        if (isRunning) {
            return Service.START_STICKY;
        }
        isRunning = true;
        try {
            BluetoothManager bm = (BluetoothManager)
                    this.getSystemService(Context.BLUETOOTH_SERVICE);
            if (bm != null) {
                BluetoothAdapter adapter = bm.getAdapter();
                if (!adapter.isEnabled()) {
                    adapter.enable();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
        getApplicationContext().bindService(
                new Intent(this, BtleService.class), this, BIND_AUTO_CREATE);
        getApplicationContext().bindService(
                new Intent(this, AlertService.class), this, BIND_AUTO_CREATE);
        return Service.START_STICKY;
    }

    @Override
    public synchronized void onDestroy() {
        isRunning = false;
        executorService.shutdown();
        if (wristDevice != null) {
            wristDevice.disconnect(false);
        }
        if (cupDevice != null) {
            cupDevice.disconnect(false);
        }
        try {
            getApplicationContext().unbindService(this);
        } catch (Exception e) {
            // Do nothing
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return localBinder;
    }

    private void setupWristDevice() {
        executorService.execute(() -> {
            synchronized (DeviceConnectionService.this) {
                waitOnConnection();
                if (wristDevice != null) {
                    localBinder.resetAndDisconnect();
                    wristDevice = null;
                }
                String key = getString(R.string.pk_wrist_device_mac);
                if (!preferences.contains(key)) return;
                Log.d(TAG, "Establishing wrist device connection");
                String mac = preferences.getString(key, null);
                wristDevice = new WristDeviceConnection(
                        DeviceConnectionService.this, mwService, mac, deviceListeners);
                DeviceConnectionService.this.notifyAll();
            }
        });
    }

    private void setupCupDevice() {
        executorService.execute(() -> {
            synchronized (DeviceConnectionService.this) {
                waitOnConnection();
                if (cupDevice != null) {
                    localBinder.resetAndDisconnect();
                    cupDevice = null;
                }
                String key = getString(R.string.pk_cup_device_mac);
                if (!preferences.contains(key)) return;
                Log.d(TAG, "Establishing cup device connection");
                String mac = preferences.getString(key, null);
                cupDevice = new CupDeviceConnection(
                        DeviceConnectionService.this, mwService, mac, deviceListeners);
                DeviceConnectionService.this.notifyAll();
            }
        });
    }

    @Override
    public synchronized void onServiceConnected(ComponentName name, IBinder service) {
        if (service instanceof BtleService.LocalBinder) {
            preferences.registerOnSharedPreferenceChangeListener(preferenceChangeListener);
            mwService = (BtleService.LocalBinder) service;
            mwServiceComponentName = name;
            this.notifyAll();
            setupWristDevice();
            setupCupDevice();
        } else if (service instanceof AlertService.LocalBinder) {
            alertService = (AlertService.LocalBinder) service;
            alertService.addAlertListener(new AlertListener(this));
            alertServiceComponentName = name;
        }
    }

    @Override
    public synchronized void onServiceDisconnected(ComponentName name) {
        if (name.equals(mwServiceComponentName)) {
            preferences.unregisterOnSharedPreferenceChangeListener(preferenceChangeListener);
            mwService = null;
            if (isRunning) {
                getApplicationContext().startService(new Intent(this, BtleService.class));
            }
        } else if (name.equals(alertServiceComponentName)) {
            alertService = null;
            if (isRunning) {
                getApplicationContext().startService(new Intent(this, AlertService.class));
            }
        }
    }

    void submitCupTask(DeviceTask task) {
        synchronized (this) {
            if (cupDevice != null) {
                cupDevice.submitTask(task);
            }
        }
    }

    void queueCupTask(final DeviceTask task) {
        new Thread(() -> {
            synchronized (DeviceConnectionService.this) {
                while (cupDevice == null) {
                    try {
                        DeviceConnectionService.this.wait();
                    } catch (InterruptedException e) {
                        Log.w(TAG, e.getMessage(), e);
                    }
                }
                cupDevice.submitTask(task);
            }
        }).start();
    }

    private synchronized void waitOnConnection() {
        while (mwService == null) {
            try {
                Log.d(TAG, "Waiting for Metawear service connection");
                this.wait();
            } catch (InterruptedException e) {
                Log.w(TAG, e.getMessage(), e);
            }
        }
    }

}
