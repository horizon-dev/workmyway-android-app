package uk.ac.horizon.workmyway.util;

@SuppressWarnings("SameParameterValue")
public class RetryPeriod {

    private final long initial;

    private final long maximum;

    private long next;

    public RetryPeriod(
            long initial,
            long maximum) {
        this.initial = initial;
        this.maximum = maximum;
        next = initial;
        reset();
    }

    public void reset() {
        next = Math.min(initial, maximum);
    }

    public long next() {
        long v = next;
        next = Math.min(next * 2, maximum);
        return v;
    }

}
