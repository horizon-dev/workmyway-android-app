package uk.ac.horizon.workmyway.model.base;

import android.support.annotation.NonNull;

import org.joda.time.Duration;
import org.joda.time.Interval;

interface Config {

    @NonNull
    Interval getDay();

    @NonNull
    Duration getProlongedPeriodDuration();

    @NonNull
    Duration getEpochDuration();

    int getX();

    int getY();

    int getC0();

    int getC1();

    int getZ1();

    int getP();

    int getQ();

    int getEpochsForInactive();

}
