package uk.ac.horizon.workmyway.ui.common;

import android.graphics.Canvas;

import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.buffer.BarBuffer;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.interfaces.dataprovider.BarDataProvider;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.renderer.HorizontalBarChartRenderer;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.util.List;

public class BarChartRenderer extends HorizontalBarChartRenderer {

    public BarChartRenderer(
            BarDataProvider chart, ChartAnimator animator, ViewPortHandler viewPortHandler) {
        super(chart, animator, viewPortHandler);
    }

    @Override
    public void drawValues(Canvas c) {
        final List<IBarDataSet> dataSets = mChart.getBarData().getDataSets();
        final float halfTextHeight = Utils.calcTextHeight(mValuePaint, "10") / 2f;
        final float phaseY = mAnimator.getPhaseY();

        for (int i = 0; i < mChart.getBarData().getDataSetCount(); i++) {

            IBarDataSet dataSet = dataSets.get(i);
            applyValueTextStyle(dataSet);
            IValueFormatter formatter = dataSet.getValueFormatter();
            BarBuffer buffer = mBarBuffers[i];
            Transformer trans = mChart.getTransformer(dataSet.getAxisDependency());
            int bufferIndex = 0;
            int index = 0;

            while (index < dataSet.getEntryCount() * mAnimator.getPhaseX()) {

                BarEntry entry = dataSet.getEntryForIndex(index);
                int color = dataSet.getValueTextColor(index);
                float[] vals = entry.getYVals();
                float[] transformed = new float[vals.length * 2];
                float[] barValuesTransformed = new float[vals.length * 2];
                float posY = 0f;

                for (int j = 0, idx = 0; j < transformed.length; j += 2, idx++) {

                    float value = vals[idx];
                    float y;
                    float yStart;
                    if (value == 0.0f && posY == 0.0f) {
                        y = value;
                        yStart = 0;
                    } else {
                        y = posY + (value / 2);
                        yStart = posY;
                        posY += value;
                    }
                    transformed[j] = y * phaseY;
                    barValuesTransformed[j] = yStart * phaseY;
                }

                trans.pointValuesToPixel(transformed);
                trans.pointValuesToPixel(barValuesTransformed);

                for (int j = 0; j < transformed.length; j += 2) {
                    String formattedValue =
                            formatter.getFormattedValue(0, entry, i, mViewPortHandler);
                    if (uk.ac.horizon.workmyway.util.Utils.isNullOrEmpty(formattedValue)) {
                        continue;
                    }

                    float valueTextWidth = Utils.calcTextWidth(mValuePaint, formattedValue);
                    float x = transformed[j] - (valueTextWidth / 2);
                    float y = (buffer.buffer[bufferIndex + 1]
                            + buffer.buffer[bufferIndex + 3]) / 2f;

                    if (x < barValuesTransformed[j]
                            || !mViewPortHandler.isInBoundsTop(y)
                            || !mViewPortHandler.isInBoundsX(x)
                            || !mViewPortHandler.isInBoundsX(x + valueTextWidth)
                            || !mViewPortHandler.isInBoundsBottom(y)) {
                        continue;
                    }

                    drawValue(c, formattedValue, x, y + halfTextHeight, color);
                }

                bufferIndex = bufferIndex + 4 * vals.length;
                index++;
            }

        }

    }

}
