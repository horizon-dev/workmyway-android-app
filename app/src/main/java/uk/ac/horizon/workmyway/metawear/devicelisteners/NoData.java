package uk.ac.horizon.workmyway.metawear.devicelisteners;

import android.app.NotificationManager;
import android.content.Context;

import com.mbientlab.metawear.module.AccelerometerBosch;
import com.mbientlab.metawear.module.Settings;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.WorkMyWay;
import uk.ac.horizon.workmyway.metawear.DeviceListener;
import uk.ac.horizon.workmyway.model.DeviceType;
import uk.ac.horizon.workmyway.model.MutableModel;
import uk.ac.horizon.workmyway.util.Log;
import uk.ac.horizon.workmyway.util.NotificationID;

public class NoData implements DeviceListener {

    private static final int CUP_NOTIFICATION_ID = NotificationID.getNextID();

    private static final int WRIST_NOTIFICATION_ID = NotificationID.getNextID();

    private static final String TAG = NoData.class.getSimpleName();

    private static final int CUP_MINUTES = 60;

    private static final int WRIST_MINUTES = 10;

    private final AtomicReference<DateTime> lastCup = new AtomicReference<>(DateTime.now());

    private final AtomicReference<DateTime> lastWrist = new AtomicReference<>(DateTime.now());

    @SuppressWarnings("FieldCanBeLocal")
    private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

    private final NotificationManager notificationManager;

    private final android.app.Notification cupNotification;

    private final android.app.Notification wristNotification;

    public NoData(final Context context) {
        Object nm = context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (nm != null && nm instanceof NotificationManager) {
            notificationManager = (NotificationManager) nm;
        } else {
            notificationManager = null;
        }
        cupNotification = new android.app.Notification.Builder(context)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle("WorkMyWay Cup Problem")
                .setContentText(
                        "No data for more than " + CUP_MINUTES + " minutes")
                .build();
        wristNotification = new android.app.Notification.Builder(context)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle("WorkMyWay Wrist Problem")
                .setContentText("No data for more than " + WRIST_MINUTES + " minutes")
                .build();
        if (notificationManager != null) {
            executor.scheduleWithFixedDelay(() -> {
                try {
                    DateTime thenCup = lastCup.get();
                    if (DateTime.now().minusMinutes(CUP_MINUTES).isAfter(thenCup)) {
                        MutableModel model = WorkMyWay.getModelManager().getModelForToday();
                        if (model.isCurrentlyTracking()) {
                            Duration dc = new Duration(thenCup, DateTime.now());
                            Log.e(TAG, "NO DATA (cup) for " + dc.getStandardMinutes() + " minutes");
                            notificationManager.notify(CUP_NOTIFICATION_ID, cupNotification);
                        }
                    } else {
                        notificationManager.cancel(CUP_NOTIFICATION_ID);
                    }
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage(), e);
                }
            }, 0, 10, TimeUnit.SECONDS);
            executor.scheduleWithFixedDelay(() -> {
                try {
                    DateTime thenWrist = lastWrist.get();
                    if (DateTime.now().minusMinutes(WRIST_MINUTES).isAfter(thenWrist)) {
                        MutableModel model = WorkMyWay.getModelManager().getModelForToday();
                        if (model.isCurrentlyTracking()) {
                            Duration dw = new Duration(thenWrist, DateTime.now());
                            Log.e(
                                    TAG,
                                    "NO DATA (wrist) for " + dw.getStandardMinutes() + " minutes");
                            notificationManager.notify(WRIST_NOTIFICATION_ID, wristNotification);
                        }
                    } else {
                        notificationManager.cancel(WRIST_NOTIFICATION_ID);
                    }
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage(), e);
                }
            }, 0, 10, TimeUnit.SECONDS);
        }
    }

    @Override
    public void batteryState(DeviceType deviceType, Settings.BatteryState batteryState) {
        if (deviceType == DeviceType.Cup) {
            lastCup.set(DateTime.now());
        }
    }

    @Override
    public void tapLive(DeviceType deviceType, DateTime timestamp, AccelerometerBosch.Tap tap) {
        if (deviceType == DeviceType.Cup) {
            lastCup.set(DateTime.now());
        } else {
            lastWrist.set(DateTime.now());
        }
    }

    @Override
    public void stepLive(DeviceType deviceType, DateTime timestamp) {
        if (deviceType == DeviceType.Cup) {
            lastCup.set(DateTime.now());
        } else {
            lastWrist.set(DateTime.now());
        }
    }

    @Override
    public void stepLogged(DeviceType deviceType, DateTime timestamp) {
        if (deviceType == DeviceType.Cup) {
            lastCup.set(DateTime.now());
        } else {
            lastWrist.set(DateTime.now());
        }
    }

    @Override
    public void motionLive(DeviceType deviceType, DateTime timestamp) {
        if (deviceType == DeviceType.Cup) {
            lastCup.set(DateTime.now());
        } else {
            lastWrist.set(DateTime.now());
        }
    }

    @Override
    public void motionLogged(DeviceType deviceType, DateTime timestamp) {
        if (deviceType == DeviceType.Cup) {
            lastCup.set(DateTime.now());
        } else {
            lastWrist.set(DateTime.now());
        }
    }

    @Override
    public void heartbeat(DeviceType deviceType, DateTime timestamp) {
        if (deviceType == DeviceType.Cup) {
            lastCup.set(DateTime.now());
        }
    }

    @Override
    public void connected(DeviceType deviceType, DateTime timestamp) {
    }

    @Override
    public void disconnected(DeviceType deviceType, DateTime timestamp, boolean expected) {
    }

}
