package uk.ac.horizon.workmyway.metawear;

import com.mbientlab.metawear.MetaWearBoard;

interface DeviceTask {

    void doTask(MetaWearBoard board);

}
