package uk.ac.horizon.workmyway.ui.settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceFragment;

import java.util.concurrent.atomic.AtomicReference;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.WorkMyWay;
import uk.ac.horizon.workmyway.metawear.DeviceConnectionService;
import uk.ac.horizon.workmyway.rewards.RewardService;
import uk.ac.horizon.workmyway.ui.common.ActionPreference;
import uk.ac.horizon.workmyway.ui.common.DeviceServiceExecutor;
import uk.ac.horizon.workmyway.util.Log;

public class SettingsFragment extends PreferenceFragment implements ServiceConnection {

    private static final String TAG = SettingsFragment.class.getSimpleName();

    private DeviceServiceExecutor executor;

    private AtomicReference<RewardService.LocalBinder> rewardService = new AtomicReference<>(null);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences_settings);

        ((ActionPreference) findPreference(getString(R.string.pk_reset_connections))).setAction(
                arg -> {
                    AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
                    b.setMessage("Are you sure you wish to continue?")
                            .setPositiveButton("Continue", (dialogInterface, i) -> {
                                if (executor != null) {
                                    executor.execute(
                                            DeviceConnectionService.LocalBinder::resetAndDisconnect);
                                }
                                dialogInterface.dismiss();
                                //getActivity().finishAffinity();
                            })
                            .setNegativeButton(
                                    "Cancel",
                                    (dialogInterface, i) -> dialogInterface.dismiss()).create().show();
                    return null;
                });

        ((ActionPreference) findPreference(getString(R.string.pk_reset_rewards))).setAction(
                arg -> {
                    Log.i(TAG, "Recalculating day comparison rewards");
                    RewardService.LocalBinder s = rewardService.get();
                    if (s != null) {
                        s.recalculate();
                    }
                    return null;
                });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof DeviceServiceExecutor) {
            executor = (DeviceServiceExecutor) activity;
        }
        activity.bindService(new Intent(
                activity, RewardService.class), this, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onDetach() {
        Activity activity = getActivity();
        if (activity != null) {
            activity.unbindService(this);
        }
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        WorkMyWay.getAnalytics().setCurrentScreen(
                getActivity(), this.getClass().getSimpleName(), this.getClass().getName());
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (iBinder instanceof RewardService.LocalBinder) {
            rewardService.set((RewardService.LocalBinder) iBinder);
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        rewardService.set(null);
    }
}
