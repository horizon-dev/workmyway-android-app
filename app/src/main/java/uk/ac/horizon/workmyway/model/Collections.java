package uk.ac.horizon.workmyway.model;

import java.util.SortedSet;

public class Collections {

    public interface Periods extends SortedSet<Period> {}

    public interface ModelPeriods extends SortedSet<Periods> {}

}
