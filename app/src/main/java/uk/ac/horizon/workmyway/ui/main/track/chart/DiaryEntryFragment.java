package uk.ac.horizon.workmyway.ui.main.track.chart;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;
import org.joda.time.MutableDateTime;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.WorkMyWay;
import uk.ac.horizon.workmyway.db.ASyncDatabase;
import uk.ac.horizon.workmyway.model.MutableModel;
import uk.ac.horizon.workmyway.util.Log;

public class DiaryEntryFragment extends DialogFragment {

    private static final String TAG = DiaryEntryFragment.class.getSimpleName();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        @SuppressLint("InflateParams")
        View v = inflater.inflate(R.layout.fragment__ui_main_track_chart__diary_entry, null);
        final Button fromTime = (Button) v.findViewById(R.id.from_time_entry);
        final Button toTime = (Button) v.findViewById(R.id.to_time_entry);
        final EditText details = (EditText) v.findViewById(R.id.details);
        final CheckBox activeBox = (CheckBox) v.findViewById(R.id.active_box);
        DateTime now = DateTime.now();
        final MutableDateTime fromDateTime = new MutableDateTime(now);
        final MutableDateTime toDateTime = new MutableDateTime(now);
        fromTime.setText(now.toString("HH:mm"), TextView.BufferType.NORMAL);
        toTime.setText(now.toString("HH:mm"), TextView.BufferType.NORMAL);
        fromTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateTime now = DateTime.now();
                TimePickerDialog d = new TimePickerDialog(getActivity(),
                        AlertDialog.THEME_HOLO_LIGHT,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                DateTime now = DateTime.now()
                                        .withHourOfDay(hourOfDay)
                                        .withMinuteOfHour(minute);
                                fromDateTime.setHourOfDay(hourOfDay);
                                fromDateTime.setMinuteOfHour(minute);
                                fromTime.setText(now.toString("HH:mm"), TextView.BufferType.NORMAL);
                            }
                        }, now.getHourOfDay(), now.getMinuteOfHour(), true);
                d.show();
            }
        });
        toTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateTime now = DateTime.now();
                TimePickerDialog d = new TimePickerDialog(getActivity(),
                        AlertDialog.THEME_HOLO_LIGHT,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                DateTime now = DateTime.now()
                                        .withHourOfDay(hourOfDay)
                                        .withMinuteOfHour(minute);
                                toDateTime.setHourOfDay(hourOfDay);
                                toDateTime.setMinuteOfHour(minute);
                                toTime.setText(now.toString("HH:mm"), TextView.BufferType.NORMAL);
                            }
                        }, now.getHourOfDay(), now.getMinuteOfHour(), true);
                d.show();
            }
        });
        builder.setView(v)
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            ASyncDatabase db = WorkMyWay.getDatabase();
                            final DateTime start = fromDateTime.toDateTime();
                            final Duration duration = new Duration(
                                    fromDateTime.toInstant(), toDateTime.toInstant());
                            final boolean isActive = activeBox.isChecked();
                            db.insertSelfReport(
                                    start,
                                    details.getText().toString(),
                                    duration,
                                    isActive);
                        } catch (Exception e) {
                            Log.e(TAG, e.getMessage(), e);
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        DiaryEntryFragment.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }

    @Override
    public void onResume() {
        super.onResume();
        WorkMyWay.getAnalytics().setCurrentScreen(
                getActivity(), this.getClass().getSimpleName(), this.getClass().getName());
    }
}
