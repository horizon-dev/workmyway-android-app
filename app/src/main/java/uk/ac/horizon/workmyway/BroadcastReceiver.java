package uk.ac.horizon.workmyway;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BroadcastReceiver extends android.content.BroadcastReceiver {

    private static final String TAG = BroadcastReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        if (action != null) {
            switch (action) {
                case Intent.ACTION_BOOT_COMPLETED:
                    Log.d(TAG, "ACTION_BOOT_COMPLETED broadcast message received");
                    context.getApplicationContext();
                    break;
                default:
                    break;
            }
        }
    }

}
