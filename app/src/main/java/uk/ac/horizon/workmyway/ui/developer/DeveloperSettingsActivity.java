package uk.ac.horizon.workmyway.ui.developer;

import android.app.ActionBar;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MenuItem;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.ui.common.DeviceServiceExecutorActivity;

public class DeveloperSettingsActivity extends DeviceServiceExecutorActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__ui_developer__developer_settings);
        ActionBar bar = getActionBar();
        if (bar != null) {
            bar.setSubtitle(R.string.activity__ui_developer__developer_settings__subtitle);
            bar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Resources res = getResources();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (resultCode != RESULT_OK) {
            super.onActivityResult(requestCode, resultCode, data);
            return;
        }
        if (requestCode == res.getInteger(R.integer.request_code_wrist_device_scan)) {
            final String mac = data.getStringExtra(DeviceScanActivity.RESULT_FIELD_NAME);
            preferences.edit().putString(
                    getString(R.string.pk_wrist_device_mac), mac).apply();
        } else if (requestCode == res.getInteger(R.integer.request_code_cup_device_scan)) {
            final String mac = data.getStringExtra(DeviceScanActivity.RESULT_FIELD_NAME);
            preferences.edit().putString(
                    getString(R.string.pk_cup_device_mac), mac).apply();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

}
