package uk.ac.horizon.workmyway.metawear.devicelisteners;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.joda.time.DateTime;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.metawear.DeviceListenerAdapter;
import uk.ac.horizon.workmyway.model.DeviceType;
import uk.ac.horizon.workmyway.model.ModelManager;
import uk.ac.horizon.workmyway.model.MutableModel;

public class Live extends DeviceListenerAdapter {

    private enum Mode {
        Step,
        Motion,
        Unknown
    }

    @SuppressWarnings("FieldCanBeLocal")
    private final SharedPreferences.OnSharedPreferenceChangeListener preferenceChangeListener =
            new SharedPreferences.OnSharedPreferenceChangeListener() {
                @Override
                public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
                    if (context.getString(R.string.pk_count_mode).equals(key)) {
                        currentMode = getModeByName(prefs.getString(
                                context.getString(R.string.pk_count_mode),
                                context.getString(R.string.count_mode_default)));
                    }
                }
            };

    private final Context context;

    private final ModelManager modelManager;

    private Mode currentMode;

    public Live(Context context, ModelManager modelManager) {
        this.modelManager = modelManager;
        this.context = context;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.registerOnSharedPreferenceChangeListener(preferenceChangeListener);
        currentMode = getModeByName(prefs.getString(
                context.getString(R.string.pk_count_mode),
                context.getString(R.string.count_mode_default)));
    }

    @Override
    public void stepLive(DeviceType deviceType, DateTime timestamp) {
        MutableModel model = modelManager.getModelForToday();
        if (deviceType == DeviceType.Wrist && currentMode.equals(Mode.Step)) {
            model.addCount(timestamp);
        }
    }

    @Override
    public void stepLogged(DeviceType deviceType, DateTime timestamp) {
        MutableModel model = modelManager.getModelForToday();
        if (deviceType == DeviceType.Wrist && currentMode.equals(Mode.Step)) {
            model.addCount(timestamp);
        }
    }

    @Override
    public void motionLive(DeviceType deviceType, DateTime timestamp) {
        MutableModel model = modelManager.getModelForToday();
        if (deviceType == DeviceType.Wrist && currentMode.equals(Mode.Motion)) {
            model.addCount(timestamp);
        }
    }

    @Override
    public void motionLogged(DeviceType deviceType, DateTime timestamp) {
        MutableModel model = modelManager.getModelForToday();
        if (deviceType == DeviceType.Wrist && currentMode.equals(Mode.Motion)) {
            model.addCount(timestamp);
        }
    }

    @Override
    public void heartbeat(DeviceType deviceType, DateTime timestamp) {
        MutableModel model = modelManager.getModelForToday();

        if (deviceType == DeviceType.Wrist) {
            model.addHeartbeat(timestamp);
        }
    }

    @Override
    public void connected(DeviceType deviceType, DateTime timestamp) {
        MutableModel model = modelManager.getModelForToday();
        if (deviceType == DeviceType.Wrist) {
            model.addConnected(timestamp);
        }
    }

    @Override
    public void disconnected(DeviceType deviceType, DateTime timestamp, boolean expected) {
        MutableModel model = modelManager.getModelForToday();
        if (deviceType == DeviceType.Wrist) {
            model.addDisconnected(timestamp);
        }
    }

    private Mode getModeByName(String name) {
        if (name.equals("motion")) {
            return Mode.Motion;
        } else if (name.equals("step")) {
            return Mode.Step;
        }
        return Mode.Unknown;
    }

}
