package uk.ac.horizon.workmyway.ui.main.history;

import android.content.res.Resources;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;

import org.joda.time.Duration;

import java.util.ArrayList;
import java.util.Map;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.WorkMyWay;
import uk.ac.horizon.workmyway.model.ModelStats;
import uk.ac.horizon.workmyway.model.Period;

public class DaySummaryFragment extends SummaryFragment {

    @Override
    protected int getLayoutId() {
        return R.layout.fragment__ui_main_history__day_summary;
    }

    @Override
    protected void setup(View view, ModelStats modelStats) {
        Map<Period.Type, Duration> daySummary = modelStats.getSummary();
        float active = daySummary.get(Period.Type.Active).getMillis();
        float healthy = daySummary.get(Period.Type.Healthy).getMillis();
        float prolonged = daySummary.get(Period.Type.Prolonged).getMillis();
        float total = active + healthy + prolonged;

        PieChart chart = (PieChart) view.findViewById(R.id.chart);
        ArrayList<PieEntry> entries = new ArrayList<>();
        Resources r = getResources();
        entries.add(new PieEntry((active / total) * 100f, r.getString(R.string.entry_active)));
        entries.add(new PieEntry((healthy / total) * 100f,
                r.getString(R.string.entry_healthy)));
        entries.add(new PieEntry((prolonged / total) * 100f,
                r.getString(R.string.entry_prolonged)));
        PieData data = new PieData();
        PieDataSet dataSet = new PieDataSet(entries, "");
        dataSet.setValueFormatter(new PercentFormatter());
        dataSet.setColors(new int[]{
                R.color.activity_active,
                R.color.activity_healthy,
                R.color.activity_prolonged
        }, getContext());
        data.setDataSet(dataSet);
        chart.setData(data);
        chart.setDrawEntryLabels(false);
        chart.setDescription(null);
        chart.setDrawHoleEnabled(false);
        chart.setTouchEnabled(false);
        chart.invalidate();

        TextView summary = (TextView) view.findViewById(R.id.text_view_summary);
        String format = getResources().getString(R.string.day_summary);
        summary.setText(
                Html.fromHtml(parseColors(String.format(format,
                        (int) (((active + healthy) / total) * 100f)))));
    }

    @Override
    public void onResume() {
        super.onResume();
        WorkMyWay.getAnalytics().setCurrentScreen(
                getActivity(), this.getClass().getSimpleName(), this.getClass().getName());
    }

}
