package uk.ac.horizon.workmyway.ui.registration;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import uk.ac.horizon.workmyway.R;

public class SignInFragment extends Fragment {

    interface OnFragmentInteractionListener {

        void signIn(String username, String password);

        void switchToRegistration();

    }

    private OnFragmentInteractionListener listener;

    private TextView errorView;

    private String errorText;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment__ui_registration__signin, container, false);
        errorView = (TextView) v.findViewById(R.id.error);
        if (errorText != null) {
            errorView.setText(errorText);
            errorView.invalidate();
        }
        final EditText username = (EditText) v.findViewById(R.id.participant_id);
        final EditText password = (EditText) v.findViewById(R.id.password);
        (v.findViewById(R.id.switch_to_registration)).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null) {
                            listener.switchToRegistration();
                        }
                    }
                }
        );
        v.findViewById(R.id.signin).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        errorView.setText("");
                        errorView.invalidate();
                        String un = username.getText().toString();
                        String pw1 = password.getText().toString();
                        if ("".equals(un)) {
                            errorView.setText(R.string.username_missing);
                            errorView.invalidate();
                            return;
                        }
                        if ("".equals(pw1)) {
                            errorView.setText(R.string.password_missing);
                            errorView.invalidate();
                            return;
                        }
                        if (listener != null) {
                            listener.signIn(un, pw1);
                        }
                    }
                });
        return v;
    }

    public void setOnFragmentInteractionListener(OnFragmentInteractionListener listener) {
        this.listener = listener;
    }

    public void setErrorText(final String errorText) {
        this.errorText = errorText;
        if (errorView != null) {
            errorView.setText(errorText);
            errorView.invalidate();
        }
    }

}
