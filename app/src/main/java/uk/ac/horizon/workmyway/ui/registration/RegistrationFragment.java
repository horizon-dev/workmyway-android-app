package uk.ac.horizon.workmyway.ui.registration;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import uk.ac.horizon.workmyway.R;

public class RegistrationFragment extends Fragment {

    interface OnFragmentInteractionListener {

        void register(String username, String password);

        void switchToSignIn();

    }

    private OnFragmentInteractionListener listener;

    private TextView errorView;

    private String errorText;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment__ui_registration__registration, container, false);
        errorView = (TextView) v.findViewById(R.id.error);
        if (errorText != null) {
            errorView.setText(errorText);
            errorView.invalidate();
        }
        final EditText username = (EditText) v.findViewById(R.id.participant_id);
        final EditText password1 = (EditText) v.findViewById(R.id.password);
        final EditText password2 = (EditText) v.findViewById(R.id.repeat_password);
        v.findViewById(R.id.switch_to_signin).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null) {
                            listener.switchToSignIn();
                        }
                    }
                }
        );
        v.findViewById(R.id.register).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        errorView.setText("");
                        errorView.invalidate();
                        String un = username.getText().toString();
                        String pw1 = password1.getText().toString();
                        String pw2 = password2.getText().toString();
                        if ("".equals(un)) {
                            errorView.setText(R.string.username_missing);
                            errorView.invalidate();
                            return;
                        }
                        if ("".equals(pw1)) {
                            errorView.setText(R.string.password_missing);
                            errorView.invalidate();
                            return;
                        }
                        if (!pw1.equals(pw2)) {
                            errorView.setText(R.string.password_mismatch);
                            errorView.invalidate();
                            return;
                        }
                        if (listener != null) {
                            listener.register(un, pw1);
                        }
                    }
                });
        return v;
    }

    public void setOnFragmentInteractionListener(OnFragmentInteractionListener listener) {
        this.listener = listener;
    }

    public void setErrorText(String errorText) {
        this.errorText = errorText;
        if (errorView != null) {
            errorView.setText(errorText);
            errorView.invalidate();
        }
    }

}
