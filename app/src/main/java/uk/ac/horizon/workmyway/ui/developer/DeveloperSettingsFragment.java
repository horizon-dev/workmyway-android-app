package uk.ac.horizon.workmyway.ui.developer;

import android.app.Activity;
import android.os.Bundle;
import android.preference.PreferenceFragment;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.metawear.DeviceConnectionService;
import uk.ac.horizon.workmyway.ui.common.ActionPreference;
import uk.ac.horizon.workmyway.ui.common.DeviceServiceExecutor;
import uk.ac.horizon.workmyway.util.VoidCallback;

public class DeveloperSettingsFragment extends PreferenceFragment {

    private DeviceServiceExecutor executor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences_developer_settings);
        ActionPreference ap =
                (ActionPreference) findPreference(getString(R.string.pk_reset_devices));
        ap.setAction(new VoidCallback<Void>() {
            @Override
            public Void doCallback(Void arg) {
                if (executor != null) {
                    executor.execute(new DeviceServiceExecutor.Task() {
                        @Override
                        public void doTask(DeviceConnectionService.LocalBinder binder) {
                            binder.resetAndDisconnect();
                        }
                    });
                }
                return null;
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof DeviceServiceExecutor) {
            executor = (DeviceServiceExecutor) activity;
        }
    }

}
