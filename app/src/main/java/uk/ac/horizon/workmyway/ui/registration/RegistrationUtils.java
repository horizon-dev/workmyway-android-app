package uk.ac.horizon.workmyway.ui.registration;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;

import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2Session;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionOptions;

import net.minidev.json.JSONObject;

import java.net.URL;
import java.util.HashMap;

import uk.ac.horizon.workmyway.R;

public class RegistrationUtils {

    public static boolean isRegistered(Context context) {
        SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(context);
        Resources r = context.getResources();
        return p.contains(r.getString(R.string.pk_registration_username))
                && p.contains(r.getString(R.string.pk_registration_password));
    }

    static void signIn(String username, String password, Context context)
            throws Exception {
        register(username, password, context);
    }

    @SuppressLint("HardwareIds")
    static void register(String username, String password, Context context)
            throws Exception {
        URL url = new URL(context.getString(R.string.rpc_server_url));
        JSONRPC2Session session = new JSONRPC2Session(url);
        JSONRPC2SessionOptions options = new JSONRPC2SessionOptions();
        options.enableCompression(true);
        options.setConnectTimeout(10000);
        options.setReadTimeout(60000);
        session.setOptions(options);
        JSONRPC2Request req = new JSONRPC2Request("sign_in", "1");
        HashMap<String, Object> params = new HashMap<>();
        TelephonyManager tm = (TelephonyManager)
                context.getSystemService(Context.TELEPHONY_SERVICE);
        params.put("imei", tm.getDeviceId());
        params.put("username", username);
        params.put("password", password);
        req.setNamedParams(params);
        JSONRPC2Response resp = session.send(req);
        if (resp.indicatesSuccess()) {
            JSONObject r = (JSONObject) resp.getResult();
            if (r.get("error") != null) {
                throw new Exception(String.valueOf(r.get("error")));
            }
        } else {
            throw new Exception(resp.getError());
        }
    }

}
