package uk.ac.horizon.workmyway.model.base;

import android.support.annotation.NonNull;

import org.joda.time.DateTime;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

import uk.ac.horizon.workmyway.model.Collections;

class Model extends AbstractModel {

    private final Collections.ModelPeriods periods;

    Model(
            @NonNull Config config,
            @NonNull Collection<DateTime> counts,
            @NonNull Map<DateTime, Boolean> trackingStatuses,
            @NonNull Collection<DateTime> heartbeats,
            @NonNull Map<DateTime, Boolean> connectionStatuses) {
        CollectionsImpl.Timeline epochs = new CollectionsImpl.TimelineImpl();
        for (DateTime count : counts) {
            getEpochForCount(config, epochs, count).addCount(count);
        }
        CollectionsImpl.Intervals trackedIntervals =
                convertTrackingStatuses(
                        config, new TreeMap<>(trackingStatuses), config.getDay().getEnd());
        CollectionsImpl.Intervals disconnections =
                computeDisconnections(
                        heartbeats, connectionStatuses, counts, config.getDay().getEnd());
        periods = build(config, epochs, trackedIntervals, disconnections);
    }

    @Override
    @NonNull
    public Collections.ModelPeriods getPeriods() {
        return periods;
    }

}
