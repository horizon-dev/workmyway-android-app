package uk.ac.horizon.workmyway.util;

public interface Callback<T, R> {

    R doCallback(T arg);

}
