package uk.ac.horizon.workmyway.model;

import android.support.annotation.NonNull;

public interface Model {

    @NonNull
    Collections.ModelPeriods getPeriods();

}
