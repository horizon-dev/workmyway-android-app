package uk.ac.horizon.workmyway.ui.main.history;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.ColorInt;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.model.ModelStats;
import uk.ac.horizon.workmyway.util.Log;

public abstract class SummaryFragment extends Fragment {

    private static final String TAG = SummaryFragment.class.getSimpleName();

    private final Object lock = new Object();

    private ModelStats modelStats;

    private View view;

    @CallSuper
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (lock) {
                    while (modelStats == null || view == null) {
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            Log.w(TAG, e.getMessage(), e);
                        }
                    }
                    ((Activity) context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                setup(view, modelStats);
                            } catch (IllegalStateException e) {
                                Log.w(TAG, e.getMessage(), e);
                            }
                        }
                    });
                }
            }
        }).start();
    }

    @Override
    public final View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        synchronized (lock) {
            view = inflater.inflate(getLayoutId(), container, false);
            lock.notify();
        }
        return view;
    }

    @CallSuper
    public void setModelStats(ModelStats modelStats) {
        synchronized (lock) {
            this.modelStats = modelStats;
            lock.notify();
        }
    }

    String parseColors(String s) {
        Resources r = getResources();
        @ColorInt int active = r.getColor(R.color.activity_active);
        @ColorInt int healthy = r.getColor(R.color.activity_healthy);
        @ColorInt int prolonged = r.getColor(R.color.activity_prolonged);
        return s.replace("color=\"activity_active\"",
                "color=\"#" + Integer.toHexString(active & 0xFFFFFF) + "\"")
                .replace("color=\"activity_healthy\"",
                        "color=\"#" + Integer.toHexString(healthy & 0xFFFFFF) + "\"")
                .replace("color=\"activity_prolonged\"",
                        "color=\"#" + Integer.toHexString(prolonged & 0xFFFFFF) + "\"");
    }

    protected abstract int getLayoutId();

    protected abstract void setup(View view, ModelStats modelStats);

}
