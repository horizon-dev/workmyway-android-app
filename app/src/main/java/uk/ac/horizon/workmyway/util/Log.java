package uk.ac.horizon.workmyway.util;

import org.joda.time.DateTime;

import java.io.PrintWriter;
import java.io.StringWriter;

import uk.ac.horizon.workmyway.db.ASyncDatabase;

@SuppressWarnings({"SameParameterValue", "unused"})
public final class Log {

    public enum Level {
        VERBOSE(0),
        DEBUG(1),
        INFO(2),
        WARN(3),
        ERROR(4);

        private final int level;

        Level(int level) {
            this.level = level;
        }

        public int getLevel() {
            return level;
        }
    }

    private static Level level = Level.VERBOSE;

    private static ASyncDatabase aSyncDatabase;

    private Log() { }

    public static void v(String tag, String msg) {
        if (aSyncDatabase != null && level.getLevel() <= Level.VERBOSE.getLevel()) {
            aSyncDatabase.insertLogMessage(
                    DateTime.now(), String.format("[VERBOSE][%s]%s", tag, msg));
        }
        android.util.Log.v(tag, msg);
    }

    public static void d(String tag, String msg) {
        if (aSyncDatabase != null && level.getLevel() <= Level.DEBUG.getLevel()) {
            aSyncDatabase.insertLogMessage(
                    DateTime.now(), String.format("[DEBUG][%s]%s", tag, msg));
        }
        android.util.Log.d(tag, msg);
    }

    public static void i(String tag, String msg) {
        if (aSyncDatabase != null && level.getLevel() <= Level.INFO.getLevel()) {
            aSyncDatabase.insertLogMessage(
                    DateTime.now(), String.format("[INFO][%s]%s", tag, msg));
        }
        android.util.Log.i(tag, msg);
    }

    public static void w(String tag, String msg, Throwable tr) {
        if (aSyncDatabase != null && level.getLevel() <= Level.WARN.getLevel()) {
            aSyncDatabase.insertLogMessage(
                    DateTime.now(),
                    String.format("[WARN][%s]%s<%s>", tag, msg, getStackTrace(tr)));
        }
        android.util.Log.w(tag, msg, tr);
    }

    public static void w(String tag, String msg) {
        if (aSyncDatabase != null && level.getLevel() <= Level.WARN.getLevel()) {
            aSyncDatabase.insertLogMessage(
                    DateTime.now(), String.format("[WARN][%s]%s", tag, msg));
        }
        android.util.Log.w(tag, msg);
    }


    public static void e(String tag, String msg) {
        if (aSyncDatabase != null && level.getLevel() <= Level.ERROR.getLevel()) {
            aSyncDatabase.insertLogMessage(
                    DateTime.now(), String.format("[ERROR][%s]%s", tag, msg));
        }
        android.util.Log.e(tag, msg);
    }

    public static void e(String tag, String msg, Throwable tr){
        if (aSyncDatabase != null && level.getLevel() <= Level.ERROR.getLevel()) {
            aSyncDatabase.insertLogMessage(
                    DateTime.now(),
                    String.format("[ERROR][%s]%s<%s>", tag, msg, getStackTrace(tr)));
        }
        android.util.Log.e(tag, msg, tr);
    }

    public static void setASyncDatabase(ASyncDatabase aSyncDatabase) {
        Log.aSyncDatabase = aSyncDatabase;
    }

    public static void setLevel(Level level) {
        Log.level = level;
    }

    private static String getStackTrace(Throwable t) {
        if (t == null) {
            return null;
        }
        StringWriter sw = new StringWriter();
        t.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }

}
