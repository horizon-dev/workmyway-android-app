package uk.ac.horizon.workmyway.model.base;

import android.support.annotation.NonNull;

import org.joda.time.DateTime;

import java.util.Collection;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import uk.ac.horizon.workmyway.model.Collections;

class MutableModel
        extends AbstractModel
        implements uk.ac.horizon.workmyway.model.MutableModel {

    private final CollectionsImpl.Timeline epochs = new CollectionsImpl.TimelineImpl();

    private final SortedMap<DateTime, Boolean> trackingStatuses = new TreeMap<>();

    private final SortedSet<DateTime> heartbeats = new TreeSet<>();

    private final SortedMap<DateTime, Boolean> connectionStatuses = new TreeMap<>();

    private final Config config;

    private final Collection<DateTime> counts;

    MutableModel(
            @NonNull Config config,
            @NonNull Collection<DateTime> counts,
            @NonNull Map<DateTime, Boolean> trackingStatuses,
            @NonNull Collection<DateTime> heartbeats,
            @NonNull Map<DateTime, Boolean> connectionStatuses) {
        this.config = config;
        this.counts = counts;
        for (DateTime count : counts) {
            getEpochForCount(config, epochs, count).addCount(count);
        }
        this.trackingStatuses.putAll(trackingStatuses);
        this.heartbeats.addAll(heartbeats);
        this.connectionStatuses.putAll(connectionStatuses);
    }

    @NonNull
    @Override
    public synchronized Collections.ModelPeriods getPeriods() {
        CollectionsImpl.Intervals trackedIntervals =
                convertTrackingStatuses(config, trackingStatuses, DateTime.now());
        CollectionsImpl.Intervals disconnections =
                computeDisconnections(heartbeats, connectionStatuses, counts, DateTime.now());
        return build(config, epochs, trackedIntervals, disconnections);
    }

    @Override
    public synchronized void addCount(@NonNull DateTime count) {
        getEpochForCount(config, epochs, count).addCount(count);
    }

    @Override
    public synchronized void addIntervalStart(@NonNull DateTime start) {
        trackingStatuses.put(start, true);
    }

    @Override
    public synchronized void addIntervalEnd(@NonNull DateTime end) {
        trackingStatuses.put(end, false);
    }

    @Override
    public synchronized void addDisconnected(@NonNull DateTime dateTime) {
        connectionStatuses.put(dateTime, false);
    }

    @Override
    public synchronized void addConnected(@NonNull DateTime dateTime) {
        connectionStatuses.put(dateTime, true);
    }

    @Override
    public synchronized void addHeartbeat(@NonNull DateTime dateTime) {
        heartbeats.add(dateTime);
    }

    @Override
    public synchronized boolean isCurrentlyTracking() {
        if (trackingStatuses.size() > 0) {
            return trackingStatuses.get(trackingStatuses.lastKey());
        }
        return false;
    }

}
