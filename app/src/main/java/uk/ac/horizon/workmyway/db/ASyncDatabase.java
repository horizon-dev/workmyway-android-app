package uk.ac.horizon.workmyway.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.Pair;

import com.mbientlab.metawear.module.AccelerometerBosch;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Duration;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import uk.ac.horizon.workmyway.model.DeviceType;
import uk.ac.horizon.workmyway.model.ModelDataStore;
import uk.ac.horizon.workmyway.util.Callback;
import uk.ac.horizon.workmyway.util.VoidCallback;

public final class ASyncDatabase implements ModelDataStore {

    private static abstract class Write
            implements VoidCallback<SQLiteDatabase> {

        @Override
        public Void doCallback(SQLiteDatabase db) {
            execute(db);
            return null;
        }

        abstract void execute(SQLiteDatabase db);

    }

    private static final String TAG = ASyncDatabase.class.getSimpleName();

    private final BlockingQueue<Write> queue = new LinkedBlockingQueue<>();

    private final PriorityMutex mutex = new PriorityMutex();

    private final SQLiteDatabase database;

    public ASyncDatabase(final SQLiteDatabase database) {
        this.database = database;
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit((Runnable) () -> {
            //noinspection InfiniteLoopStatement
            while (true) {
                try {
                    final Write write = queue.take();
                    mutex.lock(PriorityMutex.Priority.WRITE);
                    write.execute(database);
                } catch (Throwable e) {
                    Log.e(TAG, e.getMessage(), e);
                } finally {
                    mutex.unlock();
                }
            }
        });
    }

    public void insertMotionReading(final DateTime timestamp, final DeviceType device) {
        enqueue(new Write() {
            @Override
            void execute(SQLiteDatabase db) {
                class CT extends Contract.MotionReadings {}
                DateTime utc = timestamp.withZone(DateTimeZone.UTC);
                ContentValues v = new ContentValues();
                v.put(CT.COLUMN_NAME_DEVICE, Contract.DeviceTypeValue.toStorage(device));
                v.put(CT.COLUMN_NAME_TIMESTAMP, utc.getMillis());
                db.insert(CT.TABLE_NAME, null, v);
            }
        });
    }

    public void insertStepReading(final DateTime timestamp, final DeviceType device) {
        enqueue(new Write() {
            @Override
            void execute(SQLiteDatabase db) {
                class CT extends Contract.StepReadings {}
                DateTime utc = timestamp.withZone(DateTimeZone.UTC);
                ContentValues v = new ContentValues();
                v.put(CT.COLUMN_NAME_DEVICE, Contract.DeviceTypeValue.toStorage(device));
                v.put(CT.COLUMN_NAME_TIMESTAMP, utc.getMillis());
                db.insert(CT.TABLE_NAME, null, v);
            }
        });
    }

    public void insertTapReading(
            final DateTime timestamp, final AccelerometerBosch.Tap tap, final DeviceType device) {
        enqueue(new Write() {
            @Override
            void execute(SQLiteDatabase db) {
                class CT extends Contract.TapReadings {}
                DateTime utc = timestamp.withZone(DateTimeZone.UTC);
                ContentValues v = new ContentValues();
                v.put(CT.COLUMN_NAME_DEVICE, Contract.DeviceTypeValue.toStorage(device));
                v.put(CT.COLUMN_NAME_TIMESTAMP, utc.getMillis());
                v.put(CT.COLUMN_NAME_SIGN, String.valueOf(tap.sign));
                v.put(CT.COLUMN_NAME_TAP_TYPE, String.valueOf(tap.type));
                db.insert(CT.TABLE_NAME, null, v);
            }
        });
    }

    public void insertConnectionStatus(
            final DateTime timestamp,
            final boolean connected,
            final boolean expected,
            final DeviceType device) {
        enqueue(new Write() {
            @Override
            void execute(SQLiteDatabase db) {
                class CT extends Contract.ConnectionStatuses {}
                DateTime utc = timestamp.withZone(DateTimeZone.UTC);
                ContentValues v = new ContentValues();
                v.put(CT.COLUMN_NAME_DEVICE, Contract.DeviceTypeValue.toStorage(device));
                v.put(CT.COLUMN_NAME_TIMESTAMP, utc.getMillis());
                v.put(CT.COLUMN_NAME_CONNECTED, Contract.BooleanValue.toStorage(connected));
                v.put(CT.COLUMN_NAME_EXPECTED, Contract.BooleanValue.toStorage(expected));
                db.insert(CT.TABLE_NAME, null, v);
            }
        });
    }

    public void insertTrackingStatus(final DateTime timestamp, final boolean isTracking) {
        enqueue(new Write() {
            @Override
            void execute(SQLiteDatabase db) {
                class CT extends Contract.TrackingStatuses {}
                DateTime utc = timestamp.withZone(DateTimeZone.UTC);
                ContentValues v = new ContentValues();
                v.put(CT.COLUMN_NAME_TIMESTAMP, utc.getMillis());
                v.put(CT.COLUMN_NAME_STATUS, Contract.BooleanValue.toStorage(isTracking));
                db.insert(CT.TABLE_NAME, null, v);
            }
        });
    }

    public void insertLogMessage(final DateTime timestamp, final String message) {
        enqueue(new Write() {
            @Override
            void execute(SQLiteDatabase db) {
                class CT extends Contract.Logs {}
                ContentValues v = new ContentValues();
                DateTime utc = timestamp.withZone(DateTimeZone.UTC);
                v.put(CT.COLUMN_NAME_TIMESTAMP, utc.getMillis());
                v.put(CT.COLUMN_NAME_MESSAGE, message);
                db.insert(CT.TABLE_NAME, null, v);
            }
        });
    }

    public void insertSelfReport(
            final DateTime start,
            final String details,
            final Duration duration,
            final boolean active) {
        enqueue(new Write() {
            @Override
            void execute(SQLiteDatabase db) {
                class CT extends Contract.SelfReports {}
                ContentValues v = new ContentValues();
                DateTime timestamp = DateTime.now();
                DateTime utc = timestamp.withZone(DateTimeZone.UTC);
                v.put(CT.COLUMN_NAME_TIMESTAMP, utc.getMillis());
                v.put(CT.COLUMN_NAME_START, start.withZone(DateTimeZone.UTC).getMillis());
                v.put(CT.COLUMN_NAME_DETAILS, details);
                v.put(CT.COLUMN_NAME_DURATION, duration.getMillis());
                v.put(CT.COLUMN_NAME_ACTIVE, Contract.BooleanValue.toStorage(active));
                db.insert(CT.TABLE_NAME, null, v);
            }
        });
    }

    public void insertAlert(final String action) {
        enqueue(new Write() {
            @Override
            void execute(SQLiteDatabase db) {
                class CT extends Contract.Alerts {}
                ContentValues v = new ContentValues();
                DateTime timestamp = DateTime.now();
                DateTime utc = timestamp.withZone(DateTimeZone.UTC);
                v.put(CT.COLUMN_NAME_TIMESTAMP, utc.getMillis());
                v.put(CT.COLUMN_NAME_ACTION, action);
                db.insert(CT.TABLE_NAME, null, v);
            }
        });
    }

    public void insertConfigurationChange(final String key, final String value) {
        enqueue(new Write() {
            @Override
            void execute(SQLiteDatabase db) {
                class CT extends Contract.ConfigurationChanges {}
                ContentValues v = new ContentValues();
                DateTime timestamp = DateTime.now();
                DateTime utc = timestamp.withZone(DateTimeZone.UTC);
                v.put(CT.COLUMN_NAME_TIMESTAMP, utc.getMillis());
                v.put(CT.COLUMN_NAME_KEY, key);
                v.put(CT.COLUMN_NAME_VALUE, value);
                db.insert(CT.TABLE_NAME, null, v);
            }
        });
    }

    public void insertHeartbeat(final DateTime timestamp, final DeviceType device) {
        enqueue(new Write() {
            @Override
            void execute(SQLiteDatabase db) {
                class CT extends Contract.Heartbeats {}
                DateTime utc = timestamp.withZone(DateTimeZone.UTC);
                ContentValues v = new ContentValues();
                v.put(CT.COLUMN_NAME_DEVICE, Contract.DeviceTypeValue.toStorage(device));
                v.put(CT.COLUMN_NAME_TIMESTAMP, utc.getMillis());
                db.insert(CT.TABLE_NAME, null, v);
            }
        });
    }

    public void insertDayReward(final DateTime day, final JSONObject obj) {
        enqueue(new Write() {
            @Override
            void execute(SQLiteDatabase db) {
                class CT extends Contract.DayComparisonReward {}
                ContentValues v = new ContentValues();
                v.put(CT.COLUMN_NAME_DAY, Contract.DayValue.toStorage(day));
                v.put(CT.COLUMN_NAME_DATA, obj.toString());
                db.insert(CT.TABLE_NAME, null, v);
            }
        });
    }

    public void insertGoalReward(final DateTime day, final JSONObject obj, final boolean rewarded) {
        enqueue(new Write() {
            @Override
            void execute(SQLiteDatabase db) {
                class CT extends Contract.GoalReward {}
                ContentValues v = new ContentValues();
                v.put(CT.COLUMN_NAME_DAY, Contract.DayValue.toStorage(day));
                v.put(CT.COLUMN_NAME_DATA, obj.toString());
                v.put(CT.COLUMN_NAME_REWARDED, Contract.BooleanValue.toStorage(rewarded));
                db.insert(CT.TABLE_NAME, null, v);
            }
        });
    }

    public void deleteDayRewards() {
        enqueue(new Write() {
            @Override
            void execute(SQLiteDatabase db) {
                db.delete(Contract.DayComparisonReward.TABLE_NAME, null, null);
            }
        });
    }

    public <T> T getMotionReadings(long lastReadID, long limit, Callback<Cursor, T> processor) {
        mutex.lock(PriorityMutex.Priority.READ);
        try {
            try (Cursor c = database.query(
                    Contract.MotionReadings.TABLE_NAME,
                    null,
                    Contract.MotionReadings._ID + " > ?",
                    new String[]{String.valueOf(lastReadID)},
                    null,
                    null,
                    Contract.MotionReadings._ID + " ASC",
                    String.valueOf(limit))) {
                return processor.doCallback(c);
            }
        } finally {
            mutex.unlock();
        }
    }

    public <T> T getStepReadings(long lastReadID, long limit, Callback<Cursor, T> processor) {
        mutex.lock(PriorityMutex.Priority.READ);
        try {
            try (Cursor c = database.query(
                    Contract.StepReadings.TABLE_NAME,
                    null,
                    Contract.StepReadings._ID + " > ?",
                    new String[]{String.valueOf(lastReadID)},
                    null,
                    null,
                    Contract.StepReadings._ID + " ASC",
                    String.valueOf(limit))) {
                return processor.doCallback(c);
            }
        } finally {
            mutex.unlock();
        }
    }

    public <T> T getTrackingStatuses(long lastReadID, long limit, Callback<Cursor, T> processor) {
        mutex.lock(PriorityMutex.Priority.READ);
        try (Cursor c = database.query(
                Contract.TrackingStatuses.TABLE_NAME,
                null,
                Contract.TrackingStatuses._ID + " > ?",
                new String[]{String.valueOf(lastReadID)},
                null,
                null,
                Contract.TrackingStatuses._ID + " ASC",
                String.valueOf(limit))) {
            return processor.doCallback(c);
        } finally {
            mutex.unlock();
        }
    }

    public <T> T getTapReadings(long lastReadID, long limit, Callback<Cursor, T> processor) {
        mutex.lock(PriorityMutex.Priority.READ);
        try (Cursor c = database.query(
                Contract.TapReadings.TABLE_NAME,
                null,
                Contract.TapReadings._ID + " > ?",
                new String[]{String.valueOf(lastReadID)},
                null,
                null,
                Contract.TapReadings._ID + " ASC",
                String.valueOf(limit))) {
            return processor.doCallback(c);
        } finally {
            mutex.unlock();
        }
    }

    public <T> T getConnectionStatuses(long lastReadID, long limit, Callback<Cursor, T> processor) {
        mutex.lock(PriorityMutex.Priority.READ);
        try (Cursor c = database.query(
                Contract.ConnectionStatuses.TABLE_NAME,
                null,
                Contract.ConnectionStatuses._ID + " > ?",
                new String[]{String.valueOf(lastReadID)},
                null,
                null,
                Contract.ConnectionStatuses._ID + " ASC",
                String.valueOf(limit))) {
            return processor.doCallback(c);
        } finally {
            mutex.unlock();
        }
    }

    public <T> T getLogMessages(long lastReadID, long limit, Callback<Cursor, T> processor) {
        mutex.lock(PriorityMutex.Priority.READ);
        try (Cursor c = database.query(
                Contract.Logs.TABLE_NAME,
                null,
                Contract.Logs._ID + " > ?",
                new String[]{String.valueOf(lastReadID)},
                null,
                null,
                Contract.Logs._ID + " ASC",
                String.valueOf(limit))) {
            return processor.doCallback(c);
        } finally {
            mutex.unlock();
        }
    }

    public <T> T getSelfReports(long lastReadID, long limit, Callback<Cursor, T> processor) {
        mutex.lock(PriorityMutex.Priority.READ);
        try (Cursor c = database.query(
                Contract.SelfReports.TABLE_NAME,
                null,
                Contract.SelfReports._ID + " > ?",
                new String[]{String.valueOf(lastReadID)},
                null,
                null,
                Contract.SelfReports._ID + " ASC",
                String.valueOf(limit))) {
            return processor.doCallback(c);
        } finally {
            mutex.unlock();
        }
    }

    public <T> T getAlerts(long lastReadID, long limit, Callback<Cursor, T> processor) {
        mutex.lock(PriorityMutex.Priority.READ);
        try (Cursor c = database.query(
                Contract.Alerts.TABLE_NAME,
                null,
                Contract.Alerts._ID + " > ?",
                new String[]{String.valueOf(lastReadID)},
                null,
                null,
                Contract.Alerts._ID + " ASC",
                String.valueOf(limit))) {
            return processor.doCallback(c);
        } finally {
            mutex.unlock();
        }
    }

    public <T> T getConfigurationChanges(
            long lastReadID, long limit, Callback<Cursor, T> processor) {
        mutex.lock(PriorityMutex.Priority.READ);
        try (Cursor c = database.query(
                Contract.ConfigurationChanges.TABLE_NAME,
                null,
                Contract.ConfigurationChanges._ID + " > ?",
                new String[]{String.valueOf(lastReadID)},
                null,
                null,
                Contract.ConfigurationChanges._ID + " ASC",
                String.valueOf(limit))) {
            return processor.doCallback(c);
        } finally {
            mutex.unlock();
        }
    }

    public <T> T getHeartbeats(long lastReadID, long limit, Callback<Cursor, T> processor) {
        mutex.lock(PriorityMutex.Priority.READ);
        try (Cursor c = database.query(
                Contract.Heartbeats.TABLE_NAME,
                null,
                Contract.Heartbeats._ID + " > ?",
                new String[]{String.valueOf(lastReadID)},
                null,
                null,
                Contract.Heartbeats._ID + " ASC",
                String.valueOf(limit))) {
            return processor.doCallback(c);
        } finally {
            mutex.unlock();
        }
    }

    public <T> T getDayComparisonRewards(
            long lastReadID, long limit, Callback<Cursor, T> processor) {
        mutex.lock(PriorityMutex.Priority.READ);
        try (Cursor c = database.query(
                Contract.DayComparisonReward.TABLE_NAME,
                null,
                Contract.DayComparisonReward._ID + " > ?",
                new String[]{String.valueOf(lastReadID)},
                null,
                null,
                Contract.DayComparisonReward._ID + " ASC",
                String.valueOf(limit))) {
            return processor.doCallback(c);
        } finally {
            mutex.unlock();
        }
    }

    public <T> T getGoalRewards(long lastReadID, long limit, Callback<Cursor, T> processor) {
        mutex.lock(PriorityMutex.Priority.READ);
        try (Cursor c = database.query(
                Contract.GoalReward.TABLE_NAME,
                null,
                Contract.GoalReward._ID + " > ?",
                new String[]{String.valueOf(lastReadID)},
                null,
                null,
                Contract.GoalReward._ID + " ASC",
                String.valueOf(limit))) {
            return processor.doCallback(c);
        } finally {
            mutex.unlock();
        }
    }

    @Override
    public List<DateTime> getMotionReadings(DeviceType deviceType, DateTime start, DateTime end) {
        class CT extends Contract.MotionReadings {}
        mutex.lock(PriorityMutex.Priority.READ);
        try (Cursor c = database.query(
                CT.TABLE_NAME,
                null,
                CT.COLUMN_NAME_TIMESTAMP + " >= ? AND " +
                        CT.COLUMN_NAME_TIMESTAMP + " < ? AND " +
                        CT.COLUMN_NAME_DEVICE + " = ?",
                new String[]{
                        String.valueOf(start.withZone(DateTimeZone.UTC).getMillis()),
                        String.valueOf(end.withZone(DateTimeZone.UTC).getMillis()),
                        String.valueOf(Contract.DeviceTypeValue.toStorage(deviceType))
                },
                null,
                null,
                null)) {
            List<DateTime> results = new ArrayList<>();
            int timestampCol = c.getColumnIndex(CT.COLUMN_NAME_TIMESTAMP);
            while (c.moveToNext()) {
                results.add(new DateTime(c.getLong(timestampCol), DateTimeZone.UTC));
            }
            return results;
        } finally {
            mutex.unlock();
        }
    }

    @Override
    public List<DateTime> getStepReadings(DeviceType deviceType, DateTime start, DateTime end) {
        class CT extends Contract.StepReadings {}
        mutex.lock(PriorityMutex.Priority.READ);
        try (Cursor c = database.query(
                CT.TABLE_NAME,
                null,
                CT.COLUMN_NAME_TIMESTAMP + " >= ? AND " +
                        CT.COLUMN_NAME_TIMESTAMP + " < ? AND " +
                        CT.COLUMN_NAME_DEVICE + " = ?",
                new String[]{
                        String.valueOf(start.withZone(DateTimeZone.UTC).getMillis()),
                        String.valueOf(end.withZone(DateTimeZone.UTC).getMillis()),
                        String.valueOf(Contract.DeviceTypeValue.toStorage(deviceType))
                },
                null,
                null,
                null)) {
            List<DateTime> results = new ArrayList<>();
            int timestampCol = c.getColumnIndex(CT.COLUMN_NAME_TIMESTAMP);
            while (c.moveToNext()) {
                results.add(new DateTime(c.getLong(timestampCol), DateTimeZone.UTC));
            }
            return results;
        } finally {
            mutex.unlock();
        }
    }

    @Override
    public Map<DateTime, Boolean> getTrackingStatuses(DateTime start, DateTime end) {
        class CT extends Contract.TrackingStatuses {}
        mutex.lock(PriorityMutex.Priority.READ);
        try (Cursor c = database.query(
                CT.TABLE_NAME,
                null,
                CT.COLUMN_NAME_TIMESTAMP + " >= ? AND " +
                        CT.COLUMN_NAME_TIMESTAMP + " < ?",
                new String[]{
                        String.valueOf(start.withZone(DateTimeZone.UTC).getMillis()),
                        String.valueOf(end.withZone(DateTimeZone.UTC).getMillis())
                },
                null,
                null,
                CT.COLUMN_NAME_TIMESTAMP + " ASC",
                null)) {
            Map<DateTime, Boolean> results = new HashMap<>();
            int timestampCol = c.getColumnIndex(CT.COLUMN_NAME_TIMESTAMP);
            int statusCol = c.getColumnIndex(CT.COLUMN_NAME_STATUS);
            while (c.moveToNext()) {
                results.put(
                        new DateTime(c.getLong(timestampCol), DateTimeZone.UTC),
                        Contract.BooleanValue.fromStorage(c.getInt(statusCol)));
            }
            return results;
        } finally {
            mutex.unlock();
        }
    }

    @Override
    public Map<DateTime, Boolean> getConnectionStatuses(
            DeviceType deviceType, DateTime start, DateTime end) {
        class CT extends Contract.ConnectionStatuses {}
        mutex.lock(PriorityMutex.Priority.READ);
        try (Cursor c = database.query(
                CT.TABLE_NAME,
                null,
                CT.COLUMN_NAME_DEVICE + " = ? AND "
                        + CT.COLUMN_NAME_TIMESTAMP + " >= ? AND "
                        + CT.COLUMN_NAME_TIMESTAMP + " < ?",
                new String[]{
                        String.valueOf(Contract.DeviceTypeValue.toStorage(deviceType)),
                        String.valueOf(start.withZone(DateTimeZone.UTC).getMillis()),
                        String.valueOf(end.withZone(DateTimeZone.UTC).getMillis())
                },
                null,
                null,
                CT.COLUMN_NAME_TIMESTAMP + " ASC",
                null)) {
            Map<DateTime, Boolean> results = new HashMap<>();
            int timestampCol = c.getColumnIndex(CT.COLUMN_NAME_TIMESTAMP);
            int statusCol = c.getColumnIndex(CT.COLUMN_NAME_CONNECTED);
            while (c.moveToNext()) {
                results.put(
                        new DateTime(c.getLong(timestampCol), DateTimeZone.UTC),
                        Contract.BooleanValue.fromStorage(c.getInt(statusCol)));
            }
            return results;
        } finally {
            mutex.unlock();
        }
    }

    @Override
    public List<DateTime> getHeartbeats(
            @SuppressWarnings("SameParameterValue") DeviceType deviceType,
            DateTime start,
            DateTime end) {
        class CT extends Contract.Heartbeats {}
        mutex.lock(PriorityMutex.Priority.READ);
        try (Cursor c = database.query(
                CT.TABLE_NAME,
                null,
                CT.COLUMN_NAME_TIMESTAMP + " >= ? AND " +
                        CT.COLUMN_NAME_TIMESTAMP + " < ? AND " +
                        CT.COLUMN_NAME_DEVICE + " = ?",
                new String[]{
                        String.valueOf(start.withZone(DateTimeZone.UTC).getMillis()),
                        String.valueOf(end.withZone(DateTimeZone.UTC).getMillis()),
                        String.valueOf(Contract.DeviceTypeValue.toStorage(deviceType))
                },
                null,
                null,
                null)) {
            List<DateTime> results = new ArrayList<>();
            int timestampCol = c.getColumnIndex(CT.COLUMN_NAME_TIMESTAMP);
            while (c.moveToNext()) {
                results.add(new DateTime(c.getLong(timestampCol), DateTimeZone.UTC));
            }
            return results;
        } finally {
            mutex.unlock();
        }
    }


    @Nullable
    public JSONObject getDayReward(DateTime day) {
        int dayval = Contract.DayValue.toStorage(day);
        class CT extends Contract.DayComparisonReward {}
        mutex.lock(PriorityMutex.Priority.READ);
        try (Cursor c = database.query(
                CT.TABLE_NAME,
                null,
                CT.COLUMN_NAME_DAY + " = ?",
                new String[]{String.valueOf(dayval)},
                null,
                null,
                null,
                null)) {
            int dataCol = c.getColumnIndex(CT.COLUMN_NAME_DATA);
            if (c.moveToNext()) {
                try {
                    return new JSONObject(c.getString(dataCol));
                } catch (JSONException e) {
                    Log.e(TAG, e.getMessage(), e);
                    return null;
                }
            }
            return null;
        } finally {
            mutex.unlock();
        }
    }

    @NonNull
    public List<JSONObject> getDayRewards() {
        class CT extends Contract.DayComparisonReward {}
        mutex.lock(PriorityMutex.Priority.READ);
        try (Cursor c = database.query(
                CT.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null,
                null)) {
            int dataCol = c.getColumnIndex(CT.COLUMN_NAME_DATA);
            ArrayList<JSONObject> res = new ArrayList<>();
            if (c.moveToNext()) {
                try {
                    res.add(new JSONObject(c.getString(dataCol)));
                } catch (JSONException e) {
                    Log.e(TAG, e.getMessage(), e);
                }
            }
            return res;
        } finally {
            mutex.unlock();
        }
    }

    @Nullable
    public Pair<JSONObject, Boolean> getGoalReward(DateTime day) {
        int dayval = Contract.DayValue.toStorage(day);
        class CT extends Contract.GoalReward {}
        mutex.lock(PriorityMutex.Priority.READ);
        try (Cursor c = database.query(
                CT.TABLE_NAME,
                null,
                CT.COLUMN_NAME_DAY + " = ?",
                new String[]{String.valueOf(dayval)},
                null,
                null,
                null,
                null)) {
            int dataCol = c.getColumnIndex(CT.COLUMN_NAME_DATA);
            int rewardedCol = c.getColumnIndex(CT.COLUMN_NAME_REWARDED);
            if (c.moveToNext()) {
                try {
                    return new Pair<>(
                            new JSONObject(c.getString(dataCol)),
                            Contract.BooleanValue.fromStorage(c.getInt(rewardedCol)));
                } catch (JSONException e) {
                    Log.e(TAG, e.getMessage(), e);
                    return null;
                }
            }
            return null;
        } finally {
            mutex.unlock();
        }
    }

    @NonNull
    public List<JSONObject> getGoalRewards() {
        class CT extends Contract.GoalReward {}
        mutex.lock(PriorityMutex.Priority.READ);
        try (Cursor c = database.query(
                CT.TABLE_NAME,
                null,
                CT.COLUMN_NAME_REWARDED + " = ?",
                new String[]{String.valueOf(Contract.BooleanValue.toStorage(true))},
                null,
                null,
                null,
                null)) {
            int dataCol = c.getColumnIndex(CT.COLUMN_NAME_DATA);
            ArrayList<JSONObject> res = new ArrayList<>();
            if (c.moveToNext()) {
                try {
                    res.add(new JSONObject(c.getString(dataCol)));
                } catch (JSONException e) {
                    Log.e(TAG, e.getMessage(), e);
                }
            }
            return res;
        } finally {
            mutex.unlock();
        }
    }

    private void enqueue(Write write) {
        try {
            queue.put(write);
        } catch (InterruptedException e) {
            Log.w(TAG, e);
            enqueue(write);
        }
    }

}
