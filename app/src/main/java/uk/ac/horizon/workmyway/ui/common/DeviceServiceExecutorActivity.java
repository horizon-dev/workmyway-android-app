package uk.ac.horizon.workmyway.ui.common;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.util.Pair;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import uk.ac.horizon.workmyway.metawear.DeviceConnectionService;
import uk.ac.horizon.workmyway.util.Log;

public abstract class DeviceServiceExecutorActivity
        extends FragmentActivity
        implements
        DeviceServiceExecutor,
        ServiceConnection {

    private static final String TAG = DeviceServiceExecutorActivity.class.getSimpleName();

    private final Executor executor = Executors.newSingleThreadExecutor();

    private final AtomicBoolean isConnected = new AtomicBoolean(false);

    private Pair<ComponentName, DeviceConnectionService.LocalBinder> deviceService;

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (iBinder instanceof DeviceConnectionService.LocalBinder) {
            deviceService = new Pair<>(componentName,
                    (DeviceConnectionService.LocalBinder) iBinder);
            isConnected.set(true);
            synchronized (isConnected) {
                isConnected.notifyAll();
            }
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        if (deviceService != null && deviceService.first.equals(componentName)) {
            isConnected.set(false);
            synchronized (isConnected) {
                deviceService = null;
            }
        }
    }

    @Override
    public void execute(final Task task) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                synchronized (isConnected) {
                    while (!isConnected.get()) {
                        try {
                            isConnected.wait();
                        } catch (InterruptedException e) {
                            Log.w(TAG, e.getMessage(), e);
                        }
                    }
                    task.doTask(deviceService.second);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        bindService(new Intent(
                this, DeviceConnectionService.class), this, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService(this);
    }

}
