package uk.ac.horizon.workmyway.upload;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2Session;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionOptions;

import java.io.File;
import java.io.FilenameFilter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.WorkMyWay;
import uk.ac.horizon.workmyway.db.ASyncDatabase;
import uk.ac.horizon.workmyway.db.Contract;
import uk.ac.horizon.workmyway.util.Callback;
import uk.ac.horizon.workmyway.util.Enclosure;
import uk.ac.horizon.workmyway.util.Utils;

public class UploadService extends Service {

    private static final String TAG = UploadService.class.getSimpleName();

    private final Object waitLock = new Object();

    private final SharedPreferences.OnSharedPreferenceChangeListener credentialsListener =
            new SharedPreferences.OnSharedPreferenceChangeListener() {
                @Override
                public void onSharedPreferenceChanged(
                        SharedPreferences sharedPreferences, String key) {
                    if (key.equals(getString(R.string.pk_registration_username))
                            || key.equals(getString(R.string.pk_registration_password))
                            || key.equals(getString(R.string.pk_use_mobile_data))) {
                        synchronized (waitLock) {
                            waitLock.notifyAll();
                        }
                    }
                }
            };

    private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

    private ConnectivityManager connectivityManager;

    private SharedPreferences preferences;

    private JSONRPC2Session session;

    private RequestQueue uploadQueue;

    private String uploadUrlPattern;

    private String username;

    private String password;

    @Override
    public void onCreate() {
        super.onCreate();
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        uploadQueue = Volley.newRequestQueue(this);
        uploadUrlPattern = getString(R.string.upload_url);
        try {
            URL url = new URL(getString(R.string.rpc_server_url));
            session = new JSONRPC2Session(url);
            JSONRPC2SessionOptions options = new JSONRPC2SessionOptions();
            options.enableCompression(true);
            options.setConnectTimeout(10000);
            options.setReadTimeout(60000);
            session.setOptions(options);
        } catch (MalformedURLException e) {
            Log.e(TAG, e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public synchronized int onStartCommand(Intent intent, int flags, int startId) {
        connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        connectivityManager.registerNetworkCallback(
                new NetworkRequest.Builder().addCapability(
                        NetworkCapabilities.NET_CAPABILITY_INTERNET).build(),
                new ConnectivityManager.NetworkCallback() {
                    @Override
                    public void onAvailable(Network network) {
                        synchronized (waitLock) {
                            waitLock.notifyAll();
                        }
                    }
                });

        preferences.registerOnSharedPreferenceChangeListener(credentialsListener);
        executor.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                try {
                    waitForConnection();
                    waitForCredentials();
                    fetchAndUpload();
                } catch (Exception e) {
                    Log.e(TAG, "Error during upload", e);
                }
            }
        }, 0, 30, TimeUnit.SECONDS);
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        executor.shutdown();
        synchronized (waitLock) {
            waitLock.notifyAll();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not implemented");
    }

    private void waitForCredentials() {
        username = preferences.getString(getString(R.string.pk_registration_username), "");
        password = preferences.getString(getString(R.string.pk_registration_password), "");
        while (!executor.isShutdown()
                && (Utils.isNullOrEmpty(username) || Utils.isNullOrEmpty(password))) {
            synchronized (waitLock) {
                try {
                    waitLock.wait();
                } catch (InterruptedException e) {
                    Log.w(TAG, e.getMessage(), e);
                }
                username = preferences.getString(getString(R.string.pk_registration_username), "");
                password = preferences.getString(getString(R.string.pk_registration_password), "");
            }
        }
    }

    private void waitForConnection() {
        NetworkInfo network;
        while (!executor.isShutdown()
                && ((network = connectivityManager.getActiveNetworkInfo()) == null
                || !network.isConnected()
                || (!(preferences.getBoolean(getString(R.string.pk_use_mobile_data),
                        getResources().getBoolean(R.bool.use_mobile_data_default)))
                && network.getType() != ConnectivityManager.TYPE_WIFI))) {
            synchronized (waitLock) {
                try {
                    waitLock.wait();
                } catch (InterruptedException e) {
                    Log.w(TAG, e);
                }
            }
        }
    }

    private void upload(String method, ArrayList<HashMap<String, Object>> data)
            throws Exception {
        if (data.size() == 0) {
            return;
        }
        JSONRPC2Request req = new JSONRPC2Request(method, "1");
        HashMap<String, Object> params = new HashMap<>();
        params.put("username", username);
        params.put("password", password);
        params.put("lite", "0");
        params.put("data", data);
        req.setNamedParams(params);
        try {
            session.send(req);
        } catch (JSONRPC2SessionException e) {
            Log.e(TAG, e.getMessage(), e);
            throw new Exception(e);
        }

    }

    private void fetchAndUpload() {

        final String mrKey = getString(R.string.pk_last_uploaded_motion_reading);
        final String srKey = getString(R.string.pk_last_uploaded_step_reading);
        final String tsKey = getString(R.string.pk_last_uploaded_tracking_status);
        final String trKey = getString(R.string.pk_last_uploaded_tap_reading);
        final String csKey = getString(R.string.pk_last_uploaded_connection_status);
        final String lmKey = getString(R.string.pk_last_uploaded_log_message);
        final String srrKey = getString(R.string.pk_last_uploaded_self_report);
        final String aKey = getString(R.string.pk_last_uploaded_alert);
        final String ccKey = getString(R.string.pk_last_uploaded_configuration_change);
        final String hbKey = getString(R.string.pk_last_uploaded_heartbeat);
        final String dcrKey = getString(R.string.pk_last_uploaded_day_comparison_reward);
        final String grKey = getString(R.string.pk_last_uploaded_goal_reward);

        final ASyncDatabase database = WorkMyWay.getDatabase();

        final Enclosure<Long> idEnc = new Enclosure<>();

        final int limit = 500;

        {
            final long mrLastId = preferences.getLong(mrKey, 0);
            ArrayList<HashMap<String, Object>> motionReadings =
                    database.getMotionReadings(mrLastId, limit, new Callback<
                            Cursor, ArrayList<HashMap<String, Object>>>() {
                        @Override
                        public ArrayList<HashMap<String, Object>> doCallback(Cursor c) {
                            ArrayList<HashMap<String, Object>> results = new ArrayList<>();
                            long lastID = mrLastId;
                            class CT extends Contract.MotionReadings {}
                            int idCol = c.getColumnIndex(CT._ID);
                            int timestampCol = c.getColumnIndex(CT.COLUMN_NAME_TIMESTAMP);
                            int deviceCol = c.getColumnIndex(CT.COLUMN_NAME_DEVICE);
                            while (c.moveToNext()) {
                                lastID = c.getLong(idCol);
                                HashMap<String, Object> m = new HashMap<>();
                                m.put("id", lastID);
                                m.put("timestamp", c.getLong(timestampCol));
                                m.put("device_type", c.getInt(deviceCol));
                                results.add(m);
                            }
                            idEnc.set(lastID);
                            return results;
                        }
                    });

            try {
                upload("save_motion_readings", motionReadings);
                preferences.edit().putLong(mrKey, idEnc.get()).apply();
            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
            }
        }

        {
            final long srLastId = preferences.getLong(srKey, 0);
            ArrayList<HashMap<String, Object>> stepReadings =
                    database.getStepReadings(srLastId, limit, new Callback<
                            Cursor, ArrayList<HashMap<String, Object>>>() {
                        @Override
                        public ArrayList<HashMap<String, Object>> doCallback(Cursor c) {
                            ArrayList<HashMap<String, Object>> results = new ArrayList<>();
                            long lastID = srLastId;
                            class CT extends Contract.StepReadings {}
                            int idCol = c.getColumnIndex(CT._ID);
                            int timestampCol = c.getColumnIndex(CT.COLUMN_NAME_TIMESTAMP);
                            int deviceCol = c.getColumnIndex(CT.COLUMN_NAME_DEVICE);
                            while (c.moveToNext()) {
                                lastID = c.getLong(idCol);
                                HashMap<String, Object> m = new HashMap<>();
                                m.put("id", lastID);
                                m.put("timestamp", c.getLong(timestampCol));
                                m.put("device_type", c.getInt(deviceCol));
                                results.add(m);
                            }
                            idEnc.set(lastID);
                            return results;
                        }
                    });

            try {
                upload("save_step_readings", stepReadings);
                preferences.edit().putLong(srKey, idEnc.get()).apply();
            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
            }
        }

        {
            final long tsLastId = preferences.getLong(tsKey, 0);
            ArrayList<HashMap<String, Object>> trackingStatuses =
                    database.getTrackingStatuses(tsLastId, limit, new Callback<Cursor,
                            ArrayList<HashMap<String, Object>>>() {
                        @Override
                        public ArrayList<HashMap<String, Object>> doCallback(Cursor c) {
                            ArrayList<HashMap<String, Object>> results = new ArrayList<>();
                            long lastID = tsLastId;
                            class CT extends Contract.TrackingStatuses {}
                            int idCol = c.getColumnIndex(CT._ID);
                            int timestampCol = c.getColumnIndex(CT.COLUMN_NAME_TIMESTAMP);
                            int statusCol = c.getColumnIndex(CT.COLUMN_NAME_STATUS);
                            while (c.moveToNext()) {
                                lastID = c.getLong(idCol);
                                HashMap<String, Object> m = new HashMap<>();
                                m.put("id", lastID);
                                m.put("timestamp", c.getLong(timestampCol));
                                m.put("status", Contract.BooleanValue.fromStorage(c.getInt(statusCol)));
                                results.add(m);
                            }
                            idEnc.set(lastID);
                            return results;
                        }
                    });

            try {
                upload("save_tracking_statuses", trackingStatuses);
                preferences.edit().putLong(tsKey, idEnc.get()).apply();
            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
            }
        }

        {
            final long trLastId = preferences.getLong(trKey, 0);
            ArrayList<HashMap<String, Object>> tapReadings =
                    database.getTapReadings(trLastId, limit, new Callback<Cursor,
                            ArrayList<HashMap<String, Object>>>() {
                        @Override
                        public ArrayList<HashMap<String, Object>> doCallback(Cursor c) {
                            ArrayList<HashMap<String, Object>> results = new ArrayList<>();
                            long lastID = trLastId;
                            class CT extends Contract.TapReadings {}
                            int idCol = c.getColumnIndex(CT._ID);
                            int timestampCol = c.getColumnIndex(CT.COLUMN_NAME_TIMESTAMP);
                            int deviceCol = c.getColumnIndex(CT.COLUMN_NAME_DEVICE);
                            int signCol = c.getColumnIndex(CT.COLUMN_NAME_SIGN);
                            int typeCol = c.getColumnIndex(CT.COLUMN_NAME_TAP_TYPE);
                            while (c.moveToNext()) {
                                HashMap<String, Object> m = new HashMap<>();
                                lastID = c.getLong(idCol);
                                m.put("id", lastID);
                                m.put("timestamp", c.getLong(timestampCol));
                                m.put("device_type", c.getInt(deviceCol));
                                m.put("sign", c.getString(signCol));
                                m.put("tap_type", c.getString(typeCol));
                                results.add(m);
                            }
                            idEnc.set(lastID);
                            return results;
                        }
                    });

            try {
                upload("save_tap_readings", tapReadings);
                preferences.edit().putLong(trKey, idEnc.get()).apply();
            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
            }
        }

        {
            final long csLastId = preferences.getLong(csKey, 0);
            ArrayList<HashMap<String, Object>> connectionStatuses =
                    database.getConnectionStatuses(csLastId, limit, new Callback<Cursor,
                            ArrayList<HashMap<String, Object>>>() {
                        @Override
                        public ArrayList<HashMap<String, Object>> doCallback(Cursor c) {
                            ArrayList<HashMap<String, Object>> results = new ArrayList<>();
                            long lastID = csLastId;
                            class CT extends Contract.ConnectionStatuses {}
                            int idCol = c.getColumnIndex(CT._ID);
                            int timestampCol = c.getColumnIndex(CT.COLUMN_NAME_TIMESTAMP);
                            int deviceCol = c.getColumnIndex(CT.COLUMN_NAME_DEVICE);
                            int connectedCol = c.getColumnIndex(CT.COLUMN_NAME_CONNECTED);
                            int expectedCol = c.getColumnIndex(CT.COLUMN_NAME_EXPECTED);
                            while (c.moveToNext()) {
                                lastID = c.getLong(idCol);
                                HashMap<String, Object> m = new HashMap<>();
                                m.put("id", lastID);
                                m.put("timestamp", c.getLong(timestampCol));
                                m.put("device_type", c.getInt(deviceCol));
                                m.put("connected",
                                        Contract.BooleanValue.fromStorage(c.getInt(connectedCol)));
                                m.put("expected",
                                        Contract.BooleanValue.fromStorage(c.getInt(expectedCol)));
                                results.add(m);
                            }
                            idEnc.set(lastID);
                            return results;
                        }
                    });

            try {
                upload("save_connection_statuses", connectionStatuses);
                preferences.edit().putLong(csKey, idEnc.get()).apply();
            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
            }
        }

        {
            final long lmLastId = preferences.getLong(lmKey, 0);
            ArrayList<HashMap<String, Object>> logMessages =
                    database.getLogMessages(lmLastId, limit, new Callback<Cursor,
                            ArrayList<HashMap<String, Object>>>() {
                        @Override
                        public ArrayList<HashMap<String, Object>> doCallback(Cursor c) {
                            ArrayList<HashMap<String, Object>> results = new ArrayList<>();
                            long lastID = lmLastId;
                            class CT extends Contract.Logs {}
                            int idCol = c.getColumnIndex(CT._ID);
                            int timestampCol = c.getColumnIndex(CT.COLUMN_NAME_TIMESTAMP);
                            int messageCol = c.getColumnIndex(CT.COLUMN_NAME_MESSAGE);
                            while (c.moveToNext()) {
                                lastID = c.getLong(idCol);
                                HashMap<String, Object> m = new HashMap<>();
                                m.put("id", lastID);
                                m.put("timestamp", c.getLong(timestampCol));
                                m.put("message", c.getString(messageCol));
                                results.add(m);
                            }
                            idEnc.set(lastID);
                            return results;
                        }
                    });

            try {
                upload("save_log_messages", logMessages);
                preferences.edit().putLong(lmKey, idEnc.get()).apply();
            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
            }
        }

        {
            final long srrLastId = preferences.getLong(srrKey, 0);
            ArrayList<HashMap<String, Object>> selfReports =
                    database.getSelfReports(srrLastId, limit, new Callback<Cursor,
                            ArrayList<HashMap<String, Object>>>() {
                        @Override
                        public ArrayList<HashMap<String, Object>> doCallback(Cursor c) {
                            ArrayList<HashMap<String, Object>> results = new ArrayList<>();
                            long lastID = srrLastId;
                            class CT extends Contract.SelfReports {}
                            int idCol = c.getColumnIndex(CT._ID);
                            int timestampCol = c.getColumnIndex(CT.COLUMN_NAME_TIMESTAMP);
                            int startCol = c.getColumnIndex(CT.COLUMN_NAME_START);
                            int detailsCol = c.getColumnIndex(CT.COLUMN_NAME_DETAILS);
                            int durationCol = c.getColumnIndex(CT.COLUMN_NAME_DURATION);
                            int activeCol = c.getColumnIndex(CT.COLUMN_NAME_ACTIVE);
                            while (c.moveToNext()) {
                                lastID = c.getLong(idCol);
                                HashMap<String, Object> m = new HashMap<>();
                                m.put("id", lastID);
                                m.put("timestamp", c.getLong(timestampCol));
                                m.put("start", c.getLong(startCol));
                                m.put("details", c.getString(detailsCol));
                                m.put("duration", c.getLong(durationCol));
                                m.put("active",
                                        Contract.BooleanValue.fromStorage(c.getInt(activeCol)));
                                results.add(m);
                            }
                            idEnc.set(lastID);
                            return results;
                        }
                    });

            try {
                upload("save_self_reports", selfReports);
                preferences.edit().putLong(srrKey, idEnc.get()).apply();
            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
            }
        }

        {
            final long aLastId = preferences.getLong(aKey, 0);
            ArrayList<HashMap<String, Object>> alerts =
                    database.getAlerts(aLastId, limit, new Callback<Cursor,
                            ArrayList<HashMap<String, Object>>>() {
                        @Override
                        public ArrayList<HashMap<String, Object>> doCallback(Cursor c) {
                            ArrayList<HashMap<String, Object>> results = new ArrayList<>();
                            long lastID = aLastId;
                            class CT extends Contract.Alerts {}
                            int idCol = c.getColumnIndex(CT._ID);
                            int timestampCol = c.getColumnIndex(CT.COLUMN_NAME_TIMESTAMP);
                            int actionCol = c.getColumnIndex(CT.COLUMN_NAME_ACTION);
                            while (c.moveToNext()) {
                                lastID = c.getLong(idCol);
                                HashMap<String, Object> m = new HashMap<>();
                                m.put("id", lastID);
                                m.put("timestamp", c.getLong(timestampCol));
                                m.put("action", c.getString(actionCol));
                                results.add(m);
                            }
                            idEnc.set(lastID);
                            return results;
                        }
                    });

            try {
                upload("save_alerts", alerts);
                preferences.edit().putLong(aKey, idEnc.get()).apply();
            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
            }
        }

        {
            final long ccLastId = preferences.getLong(ccKey, 0);
            ArrayList<HashMap<String, Object>> configChanges =
                    database.getConfigurationChanges(ccLastId, limit, new Callback<Cursor,
                            ArrayList<HashMap<String, Object>>>() {
                        @Override
                        public ArrayList<HashMap<String, Object>> doCallback(Cursor c) {
                            ArrayList<HashMap<String, Object>> results = new ArrayList<>();
                            long lastID = ccLastId;
                            class CT extends Contract.ConfigurationChanges {}
                            int idCol = c.getColumnIndex(CT._ID);
                            int timestampCol = c.getColumnIndex(CT.COLUMN_NAME_TIMESTAMP);
                            int keyCol = c.getColumnIndex(CT.COLUMN_NAME_KEY);
                            int valCol = c.getColumnIndex(CT.COLUMN_NAME_VALUE);
                            while (c.moveToNext()) {
                                lastID = c.getLong(idCol);
                                HashMap<String, Object> m = new HashMap<>();
                                m.put("id", lastID);
                                m.put("timestamp", c.getLong(timestampCol));
                                m.put("key", c.getString(keyCol));
                                m.put("value", c.getString(valCol));
                                results.add(m);
                            }
                            idEnc.set(lastID);
                            return results;
                        }
                    });

            try {
                upload("save_configuration_changes", configChanges);
                preferences.edit().putLong(ccKey, idEnc.get()).apply();
            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
            }
        }

        {
            final long hbLastId = preferences.getLong(hbKey, 0);
            ArrayList<HashMap<String, Object>> heartbeats =
                    database.getHeartbeats(hbLastId, limit, new Callback<
                            Cursor, ArrayList<HashMap<String, Object>>>() {
                        @Override
                        public ArrayList<HashMap<String, Object>> doCallback(Cursor c) {
                            ArrayList<HashMap<String, Object>> results = new ArrayList<>();
                            long lastID = hbLastId;
                            class CT extends Contract.Heartbeats {}
                            int idCol = c.getColumnIndex(CT._ID);
                            int timestampCol = c.getColumnIndex(CT.COLUMN_NAME_TIMESTAMP);
                            int deviceCol = c.getColumnIndex(CT.COLUMN_NAME_DEVICE);
                            while (c.moveToNext()) {
                                lastID = c.getLong(idCol);
                                HashMap<String, Object> m = new HashMap<>();
                                m.put("id", lastID);
                                m.put("timestamp", c.getLong(timestampCol));
                                m.put("device_type", c.getInt(deviceCol));
                                results.add(m);
                            }
                            idEnc.set(lastID);
                            return results;
                        }
                    });

            try {
                upload("save_heartbeats", heartbeats);
                preferences.edit().putLong(hbKey, idEnc.get()).apply();
            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
            }
        }

        {
            final long dcrLastId = preferences.getLong(dcrKey, 0);
            ArrayList<HashMap<String, Object>> rewards =
                    database.getDayComparisonRewards(dcrLastId, limit, new Callback<
                            Cursor, ArrayList<HashMap<String, Object>>>() {
                        @Override
                        public ArrayList<HashMap<String, Object>> doCallback(Cursor c) {
                            ArrayList<HashMap<String, Object>> results = new ArrayList<>();
                            long lastID = dcrLastId;
                            class CT extends Contract.DayComparisonReward {}
                            int idCol = c.getColumnIndex(CT._ID);
                            int dayCol = c.getColumnIndex(CT.COLUMN_NAME_DAY);
                            int dataCol = c.getColumnIndex(CT.COLUMN_NAME_DATA);
                            while (c.moveToNext()) {
                                lastID = c.getLong(idCol);
                                HashMap<String, Object> m = new HashMap<>();
                                m.put("id", lastID);
                                m.put("day", Contract.DayValue.fromStorage(c.getInt(dayCol)).toString("dd/MM/yyyy"));
                                m.put("data", c.getString(dataCol));
                                results.add(m);
                            }
                            idEnc.set(lastID);
                            return results;
                        }
                    });

            try {
                upload("save_day_comparison_rewards", rewards);
                preferences.edit().putLong(dcrKey, idEnc.get()).apply();
            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
            }
        }

        {
            final long grLastId = preferences.getLong(grKey, 0);
            ArrayList<HashMap<String, Object>> rewards =
                    database.getGoalRewards(grLastId, limit, new Callback<
                            Cursor, ArrayList<HashMap<String, Object>>>() {
                        @Override
                        public ArrayList<HashMap<String, Object>> doCallback(Cursor c) {
                            ArrayList<HashMap<String, Object>> results = new ArrayList<>();
                            long lastID = grLastId;
                            class CT extends Contract.GoalReward {}
                            int idCol = c.getColumnIndex(CT._ID);
                            int dayCol = c.getColumnIndex(CT.COLUMN_NAME_DAY);
                            int dataCol = c.getColumnIndex(CT.COLUMN_NAME_DATA);
                            int rewardedCol = c.getColumnIndex(CT.COLUMN_NAME_REWARDED);
                            while (c.moveToNext()) {
                                lastID = c.getLong(idCol);
                                HashMap<String, Object> m = new HashMap<>();
                                m.put("id", lastID);
                                m.put("day", Contract.DayValue.fromStorage(c.getInt(dayCol)).toString("dd/MM/yyyy"));
                                m.put("data", c.getString(dataCol));
                                m.put("rewarded", Contract.BooleanValue.fromStorage(c.getInt(rewardedCol)));
                                results.add(m);
                            }
                            idEnc.set(lastID);
                            return results;
                        }
                    });

            try {
                upload("save_goal_rewards", rewards);
                preferences.edit().putLong(grKey, idEnc.get()).apply();
            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
            }
        }

        /*String uploadUrl = uploadUrlPattern
                .replace("{username}", username)
                .replace("{password}", password);
        for (File complete : getFilesDir().listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File file, String s) {
                return s.endsWith(".complete");
            }
        })) {
            final File upload = new File(
                    complete.getParentFile(),
                    complete.getName().replace("" + ".complete", ".upload"));
            if (!complete.renameTo(upload)) {
                Log.w(TAG, "Unable to rename file: " + complete.getName());
                continue;
            }

            uploadQueue.add(new UploadRequest(uploadUrl, upload, new Response.Listener<Void>() {
                @Override
                public void onResponse(Void response) {
                    if (!upload.delete()) {
                        Log.w(TAG, "Couldn't delete file: " + upload.getName());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "Upload failed: " + upload.getName(), error);
                }
            }));
        }*/

    }

}

