package uk.ac.horizon.workmyway.util;

import java.util.concurrent.atomic.AtomicInteger;

public final class NotificationID {

    private final static AtomicInteger i = new AtomicInteger(0);

    public static int getNextID() {
        return i.getAndIncrement();
    }

}
