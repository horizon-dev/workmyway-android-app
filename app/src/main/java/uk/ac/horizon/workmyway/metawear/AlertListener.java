package uk.ac.horizon.workmyway.metawear;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.annotation.ColorInt;

import org.joda.time.Instant;
import org.joda.time.Interval;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.alert.AlertLevel;

class AlertListener implements uk.ac.horizon.workmyway.alert.AlertListener {

    private final DeviceConnectionService service;

    private AlertLevel currentLevel;

    private Interval currentPauseInterval;

    AlertListener(DeviceConnectionService service) {
        this.service = service;
    }

    @Override
    public synchronized void onLevelChanged(AlertLevel level) {
        if (level.equals(currentLevel)) {
            return;
        }
        currentLevel = level;
        if (currentPauseInterval == null || !currentPauseInterval.contains(Instant.now())) {
            setLed(level);
        }
    }

    @Override
    public synchronized void onBeginPause(Interval interval) {
        currentPauseInterval = interval;
        service.submitCupTask(CommonTasks.ledOff());
    }

    @Override
    public synchronized void onEndPause(Interval interval) {
        if (interval.equals(currentPauseInterval)) {
            currentPauseInterval = null;
            setLed(currentLevel);
        }
    }

    private void setLed(final AlertLevel level) {
        DeviceTask task;
        if (level.equals(AlertLevel.None)) {
            task = CommonTasks.ledOff();
        } else {
            Resources res = service.getResources();
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(service);
            @ColorInt int color;
            String pattern;
            switch (level) {
                case BreakDue:
                    color = prefs.getInt(
                            res.getString(R.string.pk_second_alert_led_color),
                            res.getColor(R.color.alert_break_due));
                    pattern = prefs.getString(
                            res.getString(R.string.pk_second_alert_led_pattern),
                            res.getString(R.string.led_pattern_second_alert_default));
                    break;
                case BreakOverdue:
                    color = prefs.getInt(
                            res.getString(R.string.pk_third_alert_led_color),
                            res.getColor(R.color.alert_break_overdue));
                    pattern = prefs.getString(
                            res.getString(R.string.pk_third_alert_led_pattern),
                            res.getString(R.string.led_pattern_third_alert_default));
                    break;
                default:
                    color = prefs.getInt(
                            res.getString(R.string.pk_first_alert_led_color),
                            res.getColor(R.color.alert_break_approaching));
                    pattern = prefs.getString(
                            res.getString(R.string.pk_first_alert_led_pattern),
                            res.getString(R.string.led_pattern_first_alert_default));
            }
            int count = res.getInteger(R.integer.alert_led_count);
            switch (pattern) {
                case "breathe":
                    task = CommonTasks.pulseLed(color, count);
                    break;
                case "flash":
                    task = CommonTasks.blinkLed(color, count);
                    break;
                case "on":
                    task = CommonTasks.solidLed(color, count);
                    break;
                default:
                    task = CommonTasks.solidLed(Color.rgb(0, 255, 0), -1);
                    //task = CommonTasks.ledOff();
            }
        }
        service.queueCupTask(task);
    }

}
