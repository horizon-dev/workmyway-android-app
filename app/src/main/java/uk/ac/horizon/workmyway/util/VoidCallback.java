package uk.ac.horizon.workmyway.util;

public interface VoidCallback<T> extends Callback<T, Void> {}
