package uk.ac.horizon.workmyway.model.base;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;

import java.util.ArrayList;
import java.util.List;

import uk.ac.horizon.workmyway.R;
import uk.ac.horizon.workmyway.model.DeviceType;
import uk.ac.horizon.workmyway.model.ModelDataStore;

public class ModelManager
        implements
        uk.ac.horizon.workmyway.model.ModelManager,
        SharedPreferences.OnSharedPreferenceChangeListener {

    private enum Mode {
        Step,
        Motion,
        Unknown
    }

    private final Object lock = new Object();

    private final Context context;

    private final ModelDataStore store;

    private MutableModel dayModel = null;

    public ModelManager(Context context, ModelDataStore store) {
        this.context = context;
        this.store = store;
        PreferenceManager.getDefaultSharedPreferences(context)
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (context.getString(R.string.pk_count_mode).equals(key)
                || context.getString(R.string.pk_count_threshold).equals(key)
                || context.getString(R.string.pk_epoch_time).equals(key)
                || context.getString(R.string.pk_prolonged_sitting_duration).equals(key)
                || context.getString(R.string.pk_detection_x).equals(key)
                || context.getString(R.string.pk_detection_y).equals(key)
                || context.getString(R.string.pk_detection_c0).equals(key)
                || context.getString(R.string.pk_detection_c1).equals(key)
                || context.getString(R.string.pk_detection_z1).equals(key)
                || context.getString(R.string.pk_detection_p).equals(key)
                || context.getString(R.string.pk_detection_q).equals(key)
                || context.getString(R.string.pk_detection_inactive_epochs).equals(key)) {
            synchronized (lock) {
                dayModel = null;
            }
        }
    }

    @Override
    @NonNull
    public MutableModel getModelForToday() {
        synchronized (lock) {
            if (dayModel != null) {
                return dayModel;
            }
            DateTime start = DateTime.now().withMillisOfDay(0);
            DateTime end = start.plusDays(1);
            Interval day = new Interval(start, end);
            Config config = buildConfig(day);
            List<DateTime> counts;
            switch (getCurrentMode()) {
            case Motion:
                counts = store.getMotionReadings(DeviceType.Wrist, start, end);
                break;
            case Step:
                counts = store.getStepReadings(DeviceType.Wrist, start, end);
                break;
            default:
                counts = new ArrayList<>();
                break;
            }
            return (dayModel = new MutableModel(
                    config,
                    counts,
                    store.getTrackingStatuses(start, end),
                    store.getHeartbeats(DeviceType.Wrist, start, end),
                    store.getConnectionStatuses(DeviceType.Wrist, start, end)));
        }
    }

    @Override
    @NonNull
    public Model getModelFor(Interval day) {
        DateTime start = day.getStart();
        DateTime end = day.getEnd();
        Config config = buildConfig(day);
        List<DateTime> counts;
        switch (getCurrentMode()) {
        case Motion:
            counts = store.getMotionReadings(DeviceType.Wrist, start, end);
            break;
        case Step:
            counts = store.getStepReadings(DeviceType.Wrist, start, end);
            break;
        default:
            counts = new ArrayList<>();
            break;
        }
        return new Model(
                config,
                counts,
                store.getTrackingStatuses(start, end),
                store.getHeartbeats(DeviceType.Wrist, start, end),
                store.getConnectionStatuses(DeviceType.Wrist, start, end));
    }

    private Config buildConfig(Interval day) {
        SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(context);
        return new ConfigBuilder()
                .setDay(day)
                .setEpochDuration(Duration.standardSeconds(Long.parseLong(p.getString(
                        context.getString(R.string.pk_epoch_time),
                        context.getString(R.string.epoch_time_default)))))
                .setProlongedPeriodDuration(Duration.standardMinutes(
                        Long.parseLong(p.getString(
                                context.getString(R.string.pk_prolonged_sitting_duration),
                                context.getString(R.string.prolonged_sitting_duration_default)))))
                .setX(Integer.parseInt(p.getString(
                        context.getString(R.string.pk_detection_x),
                        context.getString(R.string.detection_x_default))))
                .setY(Integer.parseInt(p.getString(
                        context.getString(R.string.pk_detection_y),
                        context.getString(R.string.detection_y_default))))
                .setC0(Integer.parseInt(p.getString(
                        context.getString(R.string.pk_detection_c0),
                        context.getString(R.string.detection_c0_default))))
                .setC1(Integer.parseInt(p.getString(
                        context.getString(R.string.pk_detection_c1),
                        context.getString(R.string.detection_c1_default))))
                .setZ1(Integer.parseInt(p.getString(
                        context.getString(R.string.pk_detection_z1),
                        context.getString(R.string.detection_z1_default))))
                .setP(Integer.parseInt(p.getString(
                        context.getString(R.string.pk_detection_p),
                        context.getString(R.string.detection_p_default))))
                .setQ(Integer.parseInt(p.getString(
                        context.getString(R.string.pk_detection_q),
                        context.getString(R.string.detection_q_default))))
                .setEpochsForInactive(Integer.parseInt(p.getString(
                        context.getString(R.string.pk_detection_inactive_epochs),
                        context.getString(R.string.detection_inactive_epochs_default))))
                .build();
    }

    private Mode getCurrentMode() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String modeName = prefs.getString(
                context.getString(R.string.pk_count_mode),
                context.getString(R.string.count_mode_default));
        if (modeName.equals("motion")) {
            return Mode.Motion;
        } else if (modeName.equals("step")) {
            return Mode.Step;
        }
        return Mode.Unknown;
    }

}
