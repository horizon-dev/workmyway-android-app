package uk.ac.horizon.workmyway.db;

import android.util.Log;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.LockSupport;

class PriorityMutex {

    private static final String TAG = PriorityMutex.class.getSimpleName();

    enum Priority {

        WRITE(1000),
        READ(-1000);

        final int value;

        Priority(int value) {
            this.value = value;
        }

    }

    private final AtomicBoolean locked = new AtomicBoolean(false);

    private final Queue<Thread> waiters = new ConcurrentLinkedQueue<>();

    void lock(@SuppressWarnings("unused") Priority priority) {
        boolean wasInterrupted = false;
        final Thread current = Thread.currentThread();
        waiters.add(current);
        while (waiters.peek() != current || !locked.compareAndSet(false, true)) {
            LockSupport.park(this);
            if (Thread.interrupted()) {
                wasInterrupted = true;
            }
        }
        waiters.remove();
        if (wasInterrupted) {
            Log.w(TAG, "Thread " + current.getId() + " was interrupted");
            current.interrupt();
        }
    }

    void unlock() {
        locked.set(false);
        LockSupport.unpark(waiters.peek());
    }

}
