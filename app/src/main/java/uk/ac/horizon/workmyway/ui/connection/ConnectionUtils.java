package uk.ac.horizon.workmyway.ui.connection;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import uk.ac.horizon.workmyway.R;

public final class ConnectionUtils {

    public static boolean isConnected(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.contains(context.getString(R.string.pk_wrist_device_mac))
                && prefs.contains(context.getString(R.string.pk_cup_device_mac));
    }

}
